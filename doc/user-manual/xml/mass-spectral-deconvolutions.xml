<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE chapter [

<!ENTITY % entities SYSTEM "msxpertsuite-user-manuals.ent">
%entities;

<!ENTITY % minexp-entities SYSTEM "minexpert-entities.ent">
%minexp-entities;


<!ENTITY % sgml.features "IGNORE">
<!ENTITY % xml.features "INCLUDE">

<!ENTITY % dbcent PUBLIC "-//OASIS//ENTITIES DocBook Character Entities V4.5//EN" "/usr/share/xml/docbook/schema/dtd/4.5/dbcentx.mod">
%dbcent;
]>

<chapter xmlns="http://docbook.org/ns/docbook"
xmlns:xlink="http://www.w3.org/1999/xlink"
xml:id="chap_mass-spectral-deconvolutions" version="5.0">

<info>

	<title>Mass Spectral Deconvolutions</title>

	<keywordset>

		<keyword>charge state envelope-based deconvolution</keyword>

		<keyword>isotopic cluster-based deconvolutione</keyword>

	</keywordset>

</info>


<para>

	When analysing a mass spectrum, two major deconvolutions are performed
	to get back to the M<subscript>r</subscript>&nbsp;mass of the analyte while
	reading &mz;&nbsp;values: the charge-based deconvolution and the monoisotopic
	cluster-based deconvolution.  In the following sections, both deconvolutions
	are described.

</para>

<para>

	Before delving in the deconvolutions, it is necessary to present two menu
	options that are found in the plot widgets contained in the <guilabel>Mass
	spectra</guilabel> window: the menu items under the
	<guilabel>Deconvolutions</guilabel> menu (<xref
	linkend="fig_minexpert-deconvolutions-menu"/>).

</para>

<figure xml:id="fig_minexpert-deconvolutions-menu">

	<title>Mass spectrum plot widget-specific deconvolution menu</title>

	<mediaobject>

		<imageobject role="fo">
			<imagedata fileref="print-minexpert-deconvolutions-menu.png" scale="66"/>
		</imageobject>
		<imageobject role="html">
			<imagedata fileref="web-minexpert-deconvolutions-menu.png"/>
		</imageobject>

		<caption> 

			<para>

				The two menu items are needed to configure the mouse-based
				deconvolution of mass spectra.

			</para> 

		</caption>

	</mediaobject>

</figure>

<para>

  These two menus allow one to set parameters for the deconvolution (see text
  for details).

</para>

<sect1>

	<title>Deconvolution based on charge state</title>

	<para>

		In this kind of deconvolution, at the present time, the software
		assumes that the ionization agent is the proton and that the ionization is
		positive.

	</para>

	<para>

		The deconvolution is based on the determination of the distance between
		two  peaks &mdash;consecutive or not&mdash; of a given charge state
		envelope.  When the user &lmb;-click-drags the cursor from one peak to
		another, the program tries to calculate if the distance between two peaks
		matches one or more charge difference(s). If so, it computes the molecular
		(&mr;) mass of the analyte whose mass peak is located <emphasis>under the
		cursor</emphasis>. 

		<note>

			<title>The mouse drag position is significant</title>

			<para>

				Note that the &lmb;-click-dragging direction (left&lrarr;right or
				right&lrarr;left) has an impact on the value of the charge
				<emphasis>z</emphasis> that is obtained, since that charge value is
				computed for the peak located <emphasis>under</emphasis> the cursor at
				the moment of the deconvolution. Conversely, the mouse-dragging
				direction has no effect on the &mr; (&c12;-relative molecular mass) of
				the analyte obtained as a result of the deconvolution process.

			</para>

		</note>

		<xref
		linkend="fig_minexpert-charge-based-deconvolution-two-situations"/> shows
		that process for a protein of &mr;&approx;30599&nbsp;Da. In the top panel,
		the deconvolution has involved two consecutive peaks. This is the default
		setting. However, sometimes in low-amount sample mass spectra, two nicely
		configured consecutive mass peaks are not found, and it is necessary to
		search for peaks at more than one peak span. For that, use the
		<guimenuitem>Set the span between two mass peaks</guimenuitem> to the
		required value (see <xref
		linkend="fig_minexpert-deconvolutions-menu"/>). In the lower panel, that
		span value was set to&nbsp;2 and the deconvolution provided the same
		result (of course, since we went one more peak on the left side of the
		spectrum, that pointed peak corresponds to an ion bearing one more
		proton).

	</para>


	<figure xml:id="fig_minexpert-charge-based-deconvolution-two-situations">

		<title>Charge state-based mass deconvolution</title>

		<mediaobject>

			<imageobject role="fo">
				<imagedata fileref="print-minexpert-charge-based-deconvolution-two-situations.png" scale="65"/>
			</imageobject>
			<imageobject role="html">
				<imagedata fileref="web-minexpert-charge-based-deconvolution-two-situations.png" scale="75"/>
			</imageobject>

			<caption>

				<para>

					Deconvolution approach using two peaks belonging to the same charge
					state envelope. The top deconvolution involves two consecutive mass
					peaks (peak span value is 1). The bottom deconvolution involves two
					non-consecutive peaks (peak span value is 2). Note how going one more
					peak  on the left side of the mass spectrum increments the protein
					charge by one unit. Also, the &mr; does not change significantly. Of
					course, it should be identical in both cases, but that requires
					zooming-in, enlarging at maximum the spectrum region and carefully
					positioning the cursor both at the start and end peaks.

				</para>

			</caption>

		</mediaobject>

	</figure>

	<para>

		The status bar of the window documents the current inter-peak distance
		measurement operation that is performed by &lmb;-click-drag of the cursor
		from the start peak to the end peak. The start peak is marked with a green
		marker and the end peak is marked with a red marker. Start and end positions
		are documented in the form [left &mz;&mdash;right &mz;] (even if the mouse
		drag was from right to left, the values are sorted).  The &mz;&nbsp;delta
		value documents the distance between both start and end positions.  When the
		end position matches a theoretically expected distance corresponding to a
		charge difference of 1 or more (depending on the value of the peak span),
		then the charge <emphasis>z</emphasis> of the peak under the cursor is
		provided and the molecular mass (M<subscript>r</subscript>) is provided for
		the analyte whose peak is under the cursor (&mz; value documented).

	</para>

	<note>

		<para>

      Note that the charge calculation almost never produces an integer value
      with no fractional part (say, charge z=15.0) because it is almost
      impossible to drag the mouse cursor the exact &mz;&nbsp;range that would
      lead to such an integral charge value.  Almost always, the charge that is
      calculated looks like 14.995 or 15.001, for example. Why is it impossible
      to drag the mouse cursor exactly the interval that would produce an
      integral charge value? Simply because the mouse moves at discrete
      positions on the screen and these positions might be more or less far
      apart, depending on the mouse capabilities and on the current zoom factor
      over the mass spectrum region of interest.  It is advised to zoom-in as
      much as possible over the peaks at hand so as to minimize the difficulties
      above.  It may happen, however, that even zoomed-in peaks are not
      sufficiently distant to allow a charge calculation. In this case, reduce
      the stringency over the fractional part that is allowed in the charge (see
      menu item <guilabel>Set charge minimal fractional part</guilabel> at <xref
      linkend="fig_minexpert-deconvolutions-menu"/>).  By default, the
      stringency is set at 0.99, that is, any calculated value that has a
      fractional part either superior or equal to 0.99 or inferior or equal to
      0.01 would lead to a successful round-up/round-down to the nearest integer
      value. Outside of the [0.01-0.99] interval, no charge calculation is
      performed and thus no deconvolution is performed. When the stringency is
      too high, reducing it will allow the deconvolution to be carried-over. My
      own experience is that setting that value to&nbsp;0.995 is fine for most
      situations and provides very reliable results.

		</para>

	</note>

</sect1>

<sect1>

	<title>Deconvolution based on isotopic cluster peaks</title>

	<para>

		In this kind of deconvolution, the user &lmb;-click-drags the cursor between
		the first two peaks (when possible) of the isotopic cluster. The charge
		state of the ion is the inverse of the distance between the two consecutive
		peaks (that is, the &mz; delta value).  <xref
		linkend="fig_minexpert-isotopic-cluster-deconvolution"/> shows that
		deconvolution process at work.</para>

	<figure xml:id="fig_minexpert-isotopic-cluster-deconvolution">

		<title>Isotopic cluster-based mass deconvolution</title>

		<mediaobject>

			<imageobject role="fo">
				<imagedata fileref="print-minexpert-isotopic-cluster-deconvolution.png" scale="65"/>
			</imageobject>
			<imageobject role="html">
				<imagedata fileref="web-minexpert-isotopic-cluster-deconvolution.png" scale="99"/>
			</imageobject>

			<caption>

				<para>

					The user has performed a click-drag movement between the peak under
					the green marker and the peak under the red marker. The &mz;&nbsp;
					distance between the two markers is computed and the inverse is the
					charge of the analyte under this isotopic cluster. If the first and
					second left peaks of the cluster are not suitable for easy centroid
					location positioning of the mouse cursor, use peaks to the right and
					remove the corresponding number of bumps to the right from the
					calculated monoisotopic mass value.

				</para>

			</caption>

		</mediaobject>

	</figure>

	<note>

		<title>The mouse drag position is not significant</title>

		<para>

			Note that the &lmb;-click-dragging direction (left&lrarr;right or
			right&lrarr;left) has no impact on the value of monoisotopic mass computed
			because the software postulates that the lightest ion is the peak on the
			left.

		</para>

	</note>

</sect1>

<sect1>

	<title>Reading the resolving power based on mass spectral data</title>

	<para>

		When &lmb;-click-dragging the mouse cursor between two mass spectrum
		locations of interest, the program computes the apparent resolving
		power. This process is shown on <xref
		linkend="fig_minexpert-resolving-power-calculation"/>, where the
		resolving power is calculated by dragging the mouse cursor from one edge
		of a peak to the other at half maximum height (this is called
		<emphasis>full width at half maximum</emphasis> [FWHM] resolution).</para>


	<figure xml:id="fig_minexpert-resolving-power-calculation">

		<title>Calculation of the resolving power</title>

		<mediaobject>

			<imageobject role="fo">
				<imagedata fileref="print-minexpert-resolving-power-calculation.png"/>
			</imageobject>
			<imageobject role="html">
				<imagedata fileref="web-minexpert-resolving-power-calculation.png"/>
			</imageobject>

			<caption>

				<para>

					Click-dragging the mouse cursor will trigger the calculation of the
					resolving power of the instrument. That value is printed in the status
					bar.

				</para>

			</caption>

		</mediaobject>

	</figure>

</sect1>


			</chapter>



