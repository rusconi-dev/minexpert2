message("WIN10-MINGW64 environment https://www.msys2.org/")
message("Please run the configuration like this:")
message("cmake -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Debug ../development")

set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES "c:/msys64/mingw64/include")
set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES "c:/msys64/mingw64/include")

# This is used throughout all the build system files
set(TARGET mineXpert2)

# Now that we know what is the TARGET (in the toolchain files above,
# we can compute the lowercase TARGET (used for string replacements in 
# configure files and also for the resource compilation with windres.exe.
string(TOLOWER ${TARGET} TARGET_LOWERCASE)
message("TARGET_LOWERCASE: ${TARGET_LOWERCASE}")

set(HOME_DEVEL_DIR "c:/msys64/home/mydar/devel")

CONFIGURE_FILE(${CMAKE_SOURCE_DIR}/CMakeStuff/minexpert2-mingw64-win7+.iss.in
  ${CMAKE_SOURCE_DIR}/winInstaller/minexpert2-mingw64-win7+.iss @ONLY)

set(PwizLite_FOUND 1)
set(PwizLite_INCLUDE_DIRS "${HOME_DEVEL_DIR}/pwizlite/development/src")
set(PwizLite_LIBRARIES "${HOME_DEVEL_DIR}/pwizlite/build-area/mingw64/src/libpwizlite.dll") 
if(NOT TARGET PwizLite::PwizLite)

  add_library(PwizLite::PwizLite UNKNOWN IMPORTED)

  set_target_properties(PwizLite::PwizLite PROPERTIES
    IMPORTED_LOCATION             "${PwizLite_LIBRARIES}"
    INTERFACE_INCLUDE_DIRECTORIES "${PwizLite_INCLUDE_DIRS}"
    )
endif()


set(QCustomPlot_FOUND 1)
set(QCustomPlot_INCLUDE_DIRS "${HOME_DEVEL_DIR}/qcustomplot/development")
# Note the QCustomPlot_LIBRARIES (plural) because on Debian, the
# QCustomPlotConfig.cmake file has this variable name.
set(QCustomPlot_LIBRARIES "${HOME_DEVEL_DIR}/qcustomplot/build-area/mingw64/libqcustomplot.dll") 
# Per instructions of the lib author:
# https://www.qcustomplot.com/index.php/tutorials/settingup
message(STATUS "Setting definition -DQCUSTOMPLOT_USE_LIBRARY.")
if(NOT TARGET QCustomPlot::QCustomPlot)
  add_library(QCustomPlot::QCustomPlot UNKNOWN IMPORTED)
  set_target_properties(QCustomPlot::QCustomPlot PROPERTIES
    IMPORTED_LOCATION             "${QCustomPlot_LIBRARIES}"
    INTERFACE_INCLUDE_DIRECTORIES "${QCustomPlot_INCLUDE_DIRS}"
    INTERFACE_COMPILE_DEFINITIONS QCUSTOMPLOT_USE_LIBRARY)
endif()


set(IsoSpec++_FOUND 1)
set(IsoSpec++_INCLUDE_DIRS "${HOME_DEVEL_DIR}/isospec/development")
set(IsoSpec++_LIBRARIES "${HOME_DEVEL_DIR}/isospec/build-area/mingw64/IsoSpec++/libIsoSpec++.dll") 
if(NOT TARGET IsoSpec++::IsoSpec++)
  add_library(IsoSpec++::IsoSpec++ UNKNOWN IMPORTED)
  set_target_properties(IsoSpec++::IsoSpec++ PROPERTIES
    IMPORTED_LOCATION             "${IsoSpec++_LIBRARIES}"
    INTERFACE_INCLUDE_DIRECTORIES "${IsoSpec++_INCLUDE_DIRS}")
endif()


set(PappsoMSpp_FOUND 1)
set(PappsoMSpp_INCLUDE_DIRS "${HOME_DEVEL_DIR}/pappsomspp/development/src")
set(PappsoMSpp_LIBRARIES "${HOME_DEVEL_DIR}/pappsomspp/build-area/mingw64/src/libpappsomspp.dll") 
if(NOT TARGET PappsoMSpp::Core)
  add_library(PappsoMSpp::Core UNKNOWN IMPORTED)
  set_target_properties(PappsoMSpp::Core PROPERTIES
    IMPORTED_LOCATION ${PappsoMSpp_LIBRARIES}
    INTERFACE_INCLUDE_DIRECTORIES ${PappsoMSpp_INCLUDE_DIRS})

endif()


set(PappsoMSppWidget_FOUND 1)
set(PappsoMSppWidget_LIBRARIEs "${HOME_DEVEL_DIR}/pappsomspp/build-area/mingw64/src/pappsomspp/widget/libpappsomspp-widget.dll") 
if(NOT TARGET PappsoMSpp::Widget)
  add_library(PappsoMSpp::Widget UNKNOWN IMPORTED)
  set_target_properties(PappsoMSpp::Widget PROPERTIES
    IMPORTED_LOCATION ${PappsoMSppWidget_LIBRARIEs}
    INTERFACE_INCLUDE_DIRECTORIES ${PappsoMSpp_INCLUDE_DIRS})

endif()


## INSTALL directories
# This is the default on windows, but set it nonetheless.
set(CMAKE_INSTALL_PREFIX "C:/Program Files")
set(BIN_DIR ${CMAKE_INSTALL_PREFIX}/${TARGET})
# On Win, the doc dir is uppercase.
set(DOC_DIR ${CMAKE_INSTALL_PREFIX}/${TARGET}/doc)

## Add the platform-specific source (that is the icon resource)
#  after we have built it using windres.exe.
set(PLATFORM_SPECIFIC_SOURCES ${CMAKE_SOURCE_DIR}/${TARGET}.rc)

if(NOT CMAKE_RC_COMPILER)
  set(CMAKE_RC_COMPILER windres.exe)
endif()

execute_process(COMMAND ${CMAKE_RC_COMPILER} 
  -D GCC_WINDRES
  -I "${CMAKE_CURRENT_SOURCE_DIR}"
  -i "${CMAKE_SOURCE_DIR}/${TARGET_LOWERCASE}.rc"
  -o "${CMAKE_CURRENT_BINARY_DIR}/${TARGET}.obj"
  WORKING_DIRECTORY	${CMAKE_CURRENT_SOURCE_DIR})

set(PLATFORM_SPECIFIC_SOURCES "${CMAKE_CURRENT_BINARY_DIR}/${TARGET}.obj")


# On Win10 all the code is relocatable.
remove_definitions(-fPIC)


