# This file should be included if the command line reads like this:
# x86_64-w64-mingw32.shared-cmake -DCMAKE_BUILD_TYPE=Release -DMXE=1 ..

MESSAGE("MXE (M cross environment) https://mxe.cc/")
message("Please run the configuration like this:")
message("x86_64-w64-mingw32.shared-cmake -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Release ../../development")

set(HOME_DEVEL_DIR $ENV{HOME}/devel)


set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES ${HOME_DEVEL_DIR}/mxe/usr/x86_64-w64-mingw32.shared/include)
set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES ${HOME_DEVEL_DIR}/mxe/usr/x86_64-w64-mingw32.shared/include)

# This is used throughout all the build system files
set(TARGET mineXpert2)
set(TARGET_LOWERCASE minexpert2)


# Now that we have the TARGET variable contents, let's configure InnoSetup
CONFIGURE_FILE(${CMAKE_SOURCE_DIR}/CMakeStuff/${TARGET_LOWERCASE}-mxe.iss.in
  ${CMAKE_SOURCE_DIR}/winInstaller/${TARGET_LOWERCASE}-mxe.iss @ONLY)


# No more used as now this code belongs to the PappsoMSppWidget library.
set(CustomPwiz_FOUND 1)
set(CustomPwiz_INCLUDE_DIR "${HOME_DEVEL_DIR}/custompwiz/development/src")
set(CustomPwiz_LIBRARY "${HOME_DEVEL_DIR}/custompwiz/build-area/src/libcustompwiz.dll")
if(NOT TARGET CustomPwiz::CustomPwiz)
  add_library(CustomPwiz::CustomPwiz UNKNOWN IMPORTED)
  set_target_properties(CustomPwiz::CustomPwiz PROPERTIES
    IMPORTED_LOCATION             "${CustomPwiz_LIBRARY}"
    INTERFACE_INCLUDE_DIRECTORIES "${CustomPwiz_INCLUDE_DIR}")
endif()


set(QCustomPlot_FOUND 1)
set(QCustomPlot_INCLUDE_DIR "${HOME_DEVEL_DIR}/qcustomplot/development")
set(QCustomPlot_LIBRARY "${HOME_DEVEL_DIR}/qcustomplot/build-area/mxe/libqcustomplot.dll")
if(NOT TARGET QCustomPlot::QCustomPlot)
  add_library(QCustomPlot::QCustomPlot UNKNOWN IMPORTED)
  set_target_properties(QCustomPlot::QCustomPlot PROPERTIES
    IMPORTED_LOCATION             "${QCustomPlot_LIBRARY}"
    INTERFACE_INCLUDE_DIRECTORIES "${QCustomPlot_INCLUDE_DIR}")
endif()


set(IsoSpec++_FOUND 1)
set(IsoSpec++_INCLUDE_DIR "${HOME_DEVEL_DIR}/isospec/development")
set(IsoSpec++_LIBRARY "${HOME_DEVEL_DIR}/isospec/build-area/mxe/IsoSpec++/libIsoSpec++.dll")
if(NOT TARGET IsoSpec++::IsoSpec++)
  add_library(IsoSpec++::IsoSpec++ UNKNOWN IMPORTED)
  set_target_properties(IsoSpec++::IsoSpec++ PROPERTIES
    IMPORTED_LOCATION             "${IsoSpec++_LIBRARY}"
    INTERFACE_INCLUDE_DIRECTORIES "${IsoSpec++_INCLUDE_DIR}")
endif()


set(PappsoMSpp_FOUND 1)
set(PappsoMSpp_INCLUDE_DIR "${HOME_DEVEL_DIR}/pappsomspp/development/src")
set(PappsoMSpp_LIBRARY "${HOME_DEVEL_DIR}/pappsomspp/build-area/mxe/src/libpappsomspp.dll")
if(NOT TARGET PappsoMSpp::Core)
  add_library(PappsoMSpp::Core UNKNOWN IMPORTED)
  set_target_properties(PappsoMSpp::Core PROPERTIES
    IMPORTED_LOCATION             "${PappsoMSpp_LIBRARY}"
    INTERFACE_INCLUDE_DIRECTORIES "${PappsoMSpp_INCLUDE_DIR}")
endif()


set(PappsoMSppWidget_FOUND 1)
set(PappsoMSppWidget_INCLUDE_DIR "${HOME_DEVEL_DIR}/pappsomspp/development/src")
set(PappsoMSppWidget_LIBRARY
  "${HOME_DEVEL_DIR}/pappsomspp/build-area/mxe/src/pappsomspp/widget/libpappsomspp-widget.dll")
if(NOT TARGET PappsoMSpp::Widget)
  add_library(PappsoMSpp::Widget UNKNOWN IMPORTED)
  set_target_properties(PappsoMSpp::Widget PROPERTIES
    IMPORTED_LOCATION             "${PappsoMSppWidget_LIBRARY}"
    INTERFACE_INCLUDE_DIRECTORIES "${PappsoMSppWidget_INCLUDE_DIR}")
endif()


## INSTALL directories
# This is the default on windows, but set it nonetheless.
set(CMAKE_INSTALL_PREFIX "C:/Program Files")

set(BIN_DIR ${CMAKE_INSTALL_PREFIX}/${TARGET})
# On Win, the doc dir is uppercase.
set(DOC_DIR ${CMAKE_INSTALL_PREFIX}/${TARGET}/doc)

# On Win10 all the code is relocatable.
remove_definitions(-fPIC)

