/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QSettings>
#include <QMenuBar>
#include <QMenu>
#include <QDebug>


/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "TaskMonitorWnd.hpp"
#include "TaskMonitorCompositeWidget.hpp"
#include "Application.hpp"


namespace msxps
{
namespace minexpert
{


//! Construct an TaskMonitorWnd instance.
TaskMonitorWnd::TaskMonitorWnd(QWidget *parent,
                               const QString &title,
                               const QString &settingsTitle,
                               const QString &description)
  : QMainWindow(parent),
    m_title(title),
    m_settingsTitle(settingsTitle),
    m_description(description)
{
  if(parent == Q_NULLPTR)
    qFatal("Programming error.");

  m_ui.setupUi(this);

  if(!initialize())
    qFatal("Programming error.");
}


//! Destruct \c this TaskMonitorWnd instance.
TaskMonitorWnd::~TaskMonitorWnd()
{
  // qDebug();
  writeSettings();
}


//! Write the settings to as to restore the window geometry later.
void
TaskMonitorWnd::writeSettings()
{
  QSettings settings(static_cast<Application *>(QCoreApplication::instance())
                       ->getUserConfigSettingsFilePath(),
                     QSettings::IniFormat);
  settings.beginGroup("TaskMonitorWnd");

  settings.setValue("geometry", saveGeometry());
  settings.setValue("isVisible", isVisible());

  settings.endGroup();
}


//! Read the settings to as to restore the window geometry.
void
TaskMonitorWnd::readSettings()
{
  QSettings settings(static_cast<Application *>(QCoreApplication::instance())
                       ->getUserConfigSettingsFilePath(),
                     QSettings::IniFormat);
  settings.beginGroup("TaskMonitorWnd");

  restoreGeometry(settings.value("geometry").toByteArray());
  setVisible(settings.value("isVisible").toBool());

  settings.endGroup();
}


//! Handle the close event.
void
TaskMonitorWnd::closeEvent([[maybe_unused]] QCloseEvent *event)
{
  // qDebug();
  writeSettings();
  hide();
}


//! Initialize the window.
bool
TaskMonitorWnd::initialize()
{
  setWindowTitle(QString("mineXpert2 - %1").arg(m_title));

  readSettings();

  return true;
}


void
TaskMonitorWnd::show()
{
  QSettings settings(static_cast<Application *>(QCoreApplication::instance())
                       ->getUserConfigSettingsFilePath(),
                     QSettings::IniFormat);
  settings.beginGroup("TaskMonitorWnd");

  restoreGeometry(settings.value(m_settingsTitle + "_geometry").toByteArray());

  settings.endGroup();

  QMainWindow::show();
}


TaskMonitorCompositeWidget *
TaskMonitorWnd::addTaskMonitorWidget(const QColor &color)
{

  // Allocate a task monitor composite plot widget.

  TaskMonitorCompositeWidget *widget_p =
    new TaskMonitorCompositeWidget(this, color);
  widget_p->setMinimumSize(400, 150);

  // The new widget should be preprended so that the newly created ones are at
  // the top of the vertical layout box.

  m_ui.scrollAreaVerticalLayout->insertWidget(0, widget_p);

  return widget_p;
}


} // namespace minexpert

} // namespace msxps
