/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */

/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QMessageBox>
#include <QDebug>
#include <QSettings>


/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "ProcessingFlowViewerDlg.hpp"
#include "ProgramWindow.hpp"
#include "Application.hpp"


namespace msxps
{
namespace minexpert
{


ProcessingFlowViewerDlg::ProcessingFlowViewerDlg(BasePlotWnd *base_plot_wnd_p,
                                                 const QString &applicationName)
  : QDialog(dynamic_cast<QWidget *>(base_plot_wnd_p)),
    m_applicationName{applicationName}
{
  if(base_plot_wnd_p == nullptr)
    qFatal("Programming error.");

  // NO, we have one instance that gets hidden but not destroyed.
  // We want to destroy the dialog when it is closed.
  // setAttribute(Qt::WA_DeleteOnClose);

  m_ui.setupUi(this);

  // Set the window title amongst other operations.
  setupDialog();
}


void
ProcessingFlowViewerDlg::closeEvent([[maybe_unused]] QCloseEvent *event)
{
}


ProcessingFlowViewerDlg::~ProcessingFlowViewerDlg()
{
  writeSettings();
}


//! Save the settings to later restore the window in its same position.
void
ProcessingFlowViewerDlg::writeSettings()
{
  QSettings settings(static_cast<Application *>(QCoreApplication::instance())
                       ->getUserConfigSettingsFilePath(),
                     QSettings::IniFormat);
  settings.beginGroup("ProcessingFlowViewerDlg");

  settings.setValue("geometry", saveGeometry());

  settings.endGroup();
}


//! Read the settings to restore the window in its last position.
void
ProcessingFlowViewerDlg::readSettings()
{
  QSettings settings(static_cast<Application *>(QCoreApplication::instance())
                       ->getUserConfigSettingsFilePath(),
                     QSettings::IniFormat);
  settings.beginGroup("ProcessingFlowViewerDlg");

  restoreGeometry(settings.value("geometry").toByteArray());

  settings.endGroup();
}


bool
ProcessingFlowViewerDlg::setupDialog()
{
  setWindowTitle(QString("%1 - ProcessingFlow viewer").arg(m_applicationName));

  readSettings();


  return true;
}


void
ProcessingFlowViewerDlg::setProcessingFlowText(
  const QString &processing_flow_text)
{
  m_ui.plainTextEdit->clear();
  m_ui.plainTextEdit->insertPlainText(processing_flow_text);
}


} // namespace minexpert

} // namespace msxps
