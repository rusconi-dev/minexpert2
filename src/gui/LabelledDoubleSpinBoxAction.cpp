/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QWidgetAction>
#include <QDoubleSpinBox>
#include <QHBoxLayout>
#include <QLabel>

/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "LabelledDoubleSpinBoxAction.hpp"


namespace msxps
{
namespace minexpert
{


#include "LabelledDoubleSpinBoxAction.hpp"

LabelledDoubleSpinBoxAction::LabelledDoubleSpinBoxAction(const QString &label,
                                                         double value,
                                                         QObject *parent)
  : QWidgetAction(parent)
{
  m_label = label;

  QWidget *composite_widget_p = new QWidget(static_cast<QWidget *>(parent));

  QHBoxLayout *layout_p = new QHBoxLayout(composite_widget_p);

  // The label that describes the value to enter in the spin box
  QLabel *label_p = new QLabel(label);
  layout_p->addWidget(label_p);

  // The spin box where the user enter the value
  mpa_spinBox = new QDoubleSpinBox(composite_widget_p);

  // Set the spinbox value to the requested one.
  mpa_spinBox->setValue(value);

  // The spin box holds a percentage value.
  mpa_spinBox->setRange(0, 100);

  // For filters, a step of 5 seems reasonable.
  mpa_spinBox->setSingleStep(5);
  
  layout_p->addWidget(mpa_spinBox);

  composite_widget_p->setLayout(layout_p);

  setDefaultWidget(composite_widget_p);
}


LabelledDoubleSpinBoxAction::~LabelledDoubleSpinBoxAction()
{
}


QDoubleSpinBox *
LabelledDoubleSpinBoxAction::getSpinBox() const
{
  return mpa_spinBox;
}


QString
LabelledDoubleSpinBoxAction::getLabel() const
{
  return m_label;
}


double
LabelledDoubleSpinBoxAction::getValue() const
{
  return mpa_spinBox->value();
}

} // namespace minexpert

} // namespace msxps
