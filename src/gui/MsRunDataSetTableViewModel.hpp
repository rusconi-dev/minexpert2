/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */

#pragma once

/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QAbstractItemModel>
#include <QModelIndex>
#include <QVariant>


/////////////////////// pappsomspp includes
#include <pappsomspp/msrun/msrundatasettree.h>
#include <pappsomspp/msrun/msrundatasettreenode.h>


/////////////////////// Local includes


namespace msxps
{
namespace minexpert
{

enum class MassSpecDataViewColumns
{
  COLUMN_MS_LEVEL,
  COLUMN_SPECTRUM_INDEX,
  COLUMN_RT,
  COLUMN_DT,
  COLUMN_PRECURSOR_INDEX,
  COLUMN_PRECURSOR_MZ,
  COLUMN_PRECURSOR_Z,
  COLUMN_LAST,
};

extern std::map<int, QString> massSpecDataViewColumnsMap;


class MsRunDataSetTableViewItem;

class MsRunDataSetTableViewModel : public QAbstractItemModel
{
  Q_OBJECT

  private:
  pappso::MsRunDataSetTreeCstSPtr mcsp_msRunDataSetTree = nullptr;

  void setupModelData(pappso::MsRunDataSetTreeCstSPtr ms_run_data_set_tree_csp);


  public:
  explicit MsRunDataSetTableViewModel(
    pappso::MsRunDataSetTreeCstSPtr ms_run_data_set_tree_sp, QObject *parent);

  ~MsRunDataSetTableViewModel();

  QVariant data(const QModelIndex &index, int role) const override;

  Qt::ItemFlags flags(const QModelIndex &index) const override;

  QVariant headerData(int section,
                      Qt::Orientation orientation,
                      int role = Qt::DisplayRole) const override;

  QModelIndex index(int row,
                    int column,
                    const QModelIndex &parent = QModelIndex()) const override;

  QModelIndex parent(const QModelIndex &index) const override;

  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;

  MsRunDataSetTableViewItem *mp_rootItem;
};


} // namespace minexpert
} // namespace msxps
