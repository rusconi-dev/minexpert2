/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QDialog>
#include <QPlainTextEdit>


/////////////////////// pappsomspp includes
#include <pappsomspp/trace/trace.h>
#include <pappsomspp/trace/maptrace.h>
#include <pappsomspp/processing/combiners/mzintegrationparams.h>


/////////////////////// Local includes
#include "ui_MassPeakShaperDlg.h"
#include "../nongui/MassPeakShaper.hpp"
#include "../nongui/MassPeakShaperConfig.hpp"


namespace msxps
{
namespace minexpert
{


enum class TabWidgetPage
{
  INPUT_DATA = 0x000,
  LOG,
  RESULTS,
};


class ProgramWindow;

class MassPeakShaperDlg : public QDialog
{
  Q_OBJECT

  public:
  MassPeakShaperDlg(ProgramWindow *program_window_p, const QString &applicationName);
  virtual ~MassPeakShaperDlg();

  void setCentroidData(const QString &data);

  private:
  Ui::MassPeakShaperDlg m_ui;

	ProgramWindow *mp_programWindow = nullptr;

  QString m_applicationName;
  QString m_fileName;

	pappso::MapTrace m_mapTrace;

  pappso::MzIntegrationParams m_mzIntegrationParams;

  // Each (m/z,i) pair (the i is in fact a probability that can later be
  // converted to a relative intensity) in the text edit widget will be
  // converted into a PeakShaper instance. Each PeakShaper instance will craft
  // the m_pointCount DataPoints to create the trace corresponding to the
  // starting peak centroid.
  QList<MassPeakShaper *> m_peakShapers;

  MassPeakShaperConfig m_config;

  Q_INVOKABLE void writeSettings();
  Q_INVOKABLE void readSettings();

  void closeEvent(QCloseEvent *event);

  void setupDialog();

  QString sortInputDataPoints();
  pappso::DataPoint probeFirstInputDataPoint();

  QString craftMassSpectrumName();
  std::size_t fillInThePeakShapers();
  bool fetchValidateInputData();

  void message(const QString &message);

  private slots:
  void resolutionChanged(int value);
  void fwhmChanged(double value);
  void pointCountChanged(int value);

  void gaussianRadioButtonToggled(bool checked);
  void lorentzianRadioButtonToggled(bool checked);

  // void inputDataSelectionChanged();

  void run();
  void outputFileName();
  void displayMassSpectrum();
  void sortInputDataPointsTextEdit();

  void toFwhm();
};

} // namespace minexpert

} // namespace msxps

