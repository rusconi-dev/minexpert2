/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2021 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QWidget>
#include <QPainter>
#include <QPointer>
#include <QEvent>
#include <QMouseEvent>
#include <QDebug>


/////////////////////// Local includes
#include "PlotAreaOverlay.hpp"


namespace msxps
{
namespace minexpert
{


PlotAreaOverlay::PlotAreaOverlay(QWidget *parent) : QWidget(parent)
{
  //qDebug();

  setParent(parent);

  // For MS Windows
  // Qt::FramelessWindowHint

  setWindowFlags(Qt::Widget | Qt::FramelessWindowHint |
                 Qt::WindowStaysOnTopHint);

  // setAttribute(Qt::WA_NoSystemBackground);
  // setAttribute(Qt::WA_TranslucentBackground);
  setAttribute(Qt::WA_TransparentForMouseEvents);
}


PlotAreaOverlay::~PlotAreaOverlay()
{
}


void
PlotAreaOverlay::setVertRulerVisible(bool is_visible)
{
  m_isVertRulerVisible = is_visible;
}


bool
PlotAreaOverlay::isVertRulerVisible()
{
  return m_isVertRulerVisible;
}


void
PlotAreaOverlay::setLastHorSliderValue(int value)
{
  m_horSliderValue = value;
}


void
PlotAreaOverlay::paintEvent([[maybe_unused]] QPaintEvent *paint_event)
{
  if(m_isVertRulerVisible)
    {
      raise();
      show();

      //qDebug() << "Going to draw the line  at x = " << m_horSliderValue;

      QLineF line(m_horSliderValue,
                  0,
                  m_horSliderValue,
                  size().height());

      QPainter(this).drawLine(line);
    }
}


} // namespace minexpert

} // namespace msxps
