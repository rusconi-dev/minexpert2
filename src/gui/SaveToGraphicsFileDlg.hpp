/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// StdLib includes


/////////////////////// Qt includes


/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "ui_SaveToGraphicsFileDlg.h"
#include "BasePlotWnd.hpp"
#include "BasePlotCompositeWidget.hpp"


namespace msxps
{
namespace minexpert
{


class SaveToGraphicsFileDlg : public QDialog
{
  Q_OBJECT

  public:
  SaveToGraphicsFileDlg(QWidget *parent,
                        BasePlotCompositeWidget *plot_widget_p,
                        const QString &application_name,
                        const QString &sample_name,
												const QColor &color);
  ~SaveToGraphicsFileDlg();

  public slots:
  void okPushButton();
  void cancelPushButton();
  void formatItemChanged(int index);

  protected:
  Ui::SaveToGraphicsFileDlg m_ui;

  private:
  enum GraphicsFormat
  {
    GRAPHICS_FORMAT_PDF = 0,
    GRAPHICS_FORMAT_SVG,
    GRAPHICS_FORMAT_PNG,
    GRAPHICS_FORMAT_JPG,
    GRAPHICS_FORMAT_CLIPBOARD,
    GRAPHICS_FORMAT_LAST
  };

  BasePlotCompositeWidget *mp_plotWidget = nullptr;
  QString m_applicationName;
	QString m_sampleName;
  QString m_fileName;
	QColor m_color;
  QMap<int, QString> m_graphicsFormatMap;

  void initialize();

  void readSettings();
  void writeSettings();
};


} // namespace minexpert

} // namespace msxps
