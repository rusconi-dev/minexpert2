/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */

#include <QDebug>

#include "MsRunDataSetTableViewItem.hpp"


namespace msxps
{
namespace minexpert
{

MsRunDataSetTableViewItem::MsRunDataSetTableViewItem(
  const QList<QVariant> &data,
  pappso::QualifiedMassSpectrumCstSPtr qualified_mass_spectrum_csp,
  MsRunDataSetTableViewItem *parent_item_p)
  : m_itemData(data),
    mp_parentItem(parent_item_p),
    mcsp_qualifiedMassSpectrum(qualified_mass_spectrum_csp)
{
}


MsRunDataSetTableViewItem::~MsRunDataSetTableViewItem()
{
  qDeleteAll(m_children);
}


void
MsRunDataSetTableViewItem::appendChild(MsRunDataSetTableViewItem *item)
{
  m_children.append(item);
}


MsRunDataSetTableViewItem *
MsRunDataSetTableViewItem::child(int row)
{
  return m_children.value(row);
}


int
MsRunDataSetTableViewItem::childCount() const
{
  return m_children.count();
}


int
MsRunDataSetTableViewItem::columnCount() const
{
  return m_itemData.count();
}


QVariant
MsRunDataSetTableViewItem::data(int column) const
{
  return m_itemData.value(column);
}


MsRunDataSetTableViewItem *
MsRunDataSetTableViewItem::parentItem()
{
  return mp_parentItem;
}


int
MsRunDataSetTableViewItem::row() const
{
  if(mp_parentItem)
    return mp_parentItem->m_children.indexOf(
      const_cast<MsRunDataSetTableViewItem *>(this));

  return 0;
}


pappso::QualifiedMassSpectrumCstSPtr
MsRunDataSetTableViewItem::getQualifiedMassSpectrum() const
{
  return mcsp_qualifiedMassSpectrum;
}


} // namespace minexpert
} // namespace msxps
