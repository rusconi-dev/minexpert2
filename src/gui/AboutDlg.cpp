/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QFile>
#include <QFileInfo>
#include <QDebug>
#include <QUrl>
#include <QDesktopServices>
#include <QOperatingSystemVersion>


/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "config.h"
#include "../nongui/About_gpl_v30.hpp"
#include "AboutDlg.hpp"


namespace msxps
{
namespace minexpert
{


AboutDlg::AboutDlg(QWidget *parent, const QString &applicationName)
  : QDialog(parent), m_applicationName(applicationName)
{
  if(parent == nullptr)
    qFatal("Programming error.");

  m_ui.setupUi(this);

  QString text = QString("msXpertSuite - module %1, version %2.")
                   .arg(m_applicationName)
                   .arg(VERSION);

  m_ui.programVersionLabel->setText(text);

  setWindowTitle(QString("%1 - About").arg(m_applicationName));

  connect(m_ui.closePushButton, SIGNAL(clicked()), this, SLOT(accept()));

  setupGraphicsView();

  makeUserManualCommandLinkConnections();

  m_ui.copyrightLabel->setText(
    "msXpertSuite Copyright 2016-2021 by Filippo Rusconi");
  m_ui.gplTextEdit->setText(gpl);

  setApplicationFeaturesText();

  setHistoryText();
}


AboutDlg::~AboutDlg()
{
}


void
AboutDlg::setApplicationFeaturesText()
{
  m_ui.featuresTextEdit->setHtml(
    "<html><head><meta name=\"qrichtext\" content=\"1\" /><style "
    "type=\"text/css\">\n"
    " p, li { white-space: pre-wrap; }\n"
    " </style></head><body style=\" font-family:'Sans Serif'; "
    "font-size:12pt; font-weight:400; font-style:normal; "
    "text-decoration:none;\">\n"
    "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; "
    "margin-left:0px; margin-right:0px; -qt-block-indent:0; "
    "text-indent:0px;\"></p>\n"
    "<br>\n"
    "<br>\n"
    "<em>mineXpert2</em> provides the following main features:\n"
    "<br>\n"
    "<ul>\n"
    "<li>Load any number of MS data files in mzML|mzXML|txt formats;</li>\n"
    "<li>Display the TIC chromatogram, that is dynamically computed;</li>\n"
    "<li>Perform any kind of data integration, from TIC chromatogram to "
    "drift time spectum to mass spectrum, or to color maps, in any "
    "order;</li>\n"
    "<li>Extremely flexible data view handling allowing sophisticated data set "
    " comparison modes;</li>\n"
    "<li>Set the MS<sup>n</sup> data set to be integrated;"
    "<li>Configure an \"Analysis mode\" that sends the characteristics of "
    "pointed peaks to a given file to ease data mining;</li>\n"
    "<li>mineXpert is particularly well-suited for ion mobility mass "
    "spectrometry data analysis.</li>\n"
    "</ul>\n"
    "<br>\n"
    "</body></html>\n");
}


void
AboutDlg::makeUserManualCommandLinkConnections()
{
  connect(m_ui.localPdfCommandLinkButton, &QPushButton::clicked, []() {
#include <QDesktopServices>
#include <QUrl>
    QString url = "file:///";
    if(QOperatingSystemVersion::currentType() == QOperatingSystemVersion::MacOS)
      {
        // We ship the program as a macOS bundle, so we need to provide
        // the doc directory inside it.

        url = QCoreApplication::applicationDirPath();
        url += "/../doc/minexpert2-doc.pdf";

        // Use this expedient to remove the /../ path redirection.
        QFileInfo file_info(url);
        // Re-seed the url with the proper scheme.
        url = "file:///";
        url += file_info.canonicalFilePath();
      }
    else
      {
        // We may be executing under the AppImage bundle structure. In this
        // case, we need to provide the full directory path to the documentation
        // that is not more /usr/share/... as in system GNU/Linux. The root
        // directory of the AppImage bundle is stored in the APPDIR environment
        // variable.

        QString app_image_dir = qgetenv("APPDIR");

        if(app_image_dir.isEmpty())
          {
            url += DOC_DIR;
          }
        else
          {
            url += app_image_dir;
            url += DOC_DIR;
          }

        // Finally construct the remainder.
        qInfo() << "Opening url:" << url;
        url += "/minexpert2-doc.pdf";
      }

    qInfo() << "Opening url:" << url;

    QDesktopServices::openUrl(QUrl(url));
  });

  connect(m_ui.wwwPdfCommandLinkButton, &QPushButton::clicked, []() {
#include <QDesktopServices>
#include <QUrl>
    QDesktopServices::openUrl(
      QUrl("http://msxpertsuite.org/user-manuals/pdf/minexpert2-doc.pdf"));
  });

  connect(m_ui.localHtmlCommandLinkButton, &QPushButton::clicked, []() {
#include <QDesktopServices>
#include <QUrl>
    QString url = "file:///";
    if(QOperatingSystemVersion::currentType() == QOperatingSystemVersion::MacOS)
      {
        // We ship the program as a macOS bundle, so we need to provide
        // the doc directory inside it.
        url = QCoreApplication::applicationDirPath();
        url += "/../doc/html/index.html";

        // Use this expedient to remove the /../ path redirection.
        QFileInfo file_info(url);
        // Re-seed the url with the proper scheme.
        url = "file:///";
        url += file_info.canonicalFilePath();
      }
    else
      {
        // We may be executing under the AppImage bundle structure. In this
        // case, we need to provide the full directory path to the documentation
        // that is not more /usr/share/... as in system GNU/Linux. The root
        // directory of the AppImage bundle is stored in the APPDIR environment
        // variable.

        QString app_image_dir = qgetenv("APPDIR");
        qInfo() << "AppImage directory:" << app_image_dir;

        if(app_image_dir.isEmpty())
          {
            url += DOC_DIR;
          }
        else
          {
            url += app_image_dir;
            url += DOC_DIR;
          }

        // Finally construct the remainder.
        qInfo() << "Opening url:" << url;
        url += "/html/index.html";
      }

    qInfo() << "Opening url:" << url;

    QDesktopServices::openUrl(QUrl(url));
  });

  connect(m_ui.wwwHtmlCommandLinkButton, &QPushButton::clicked, []() {
#include <QDesktopServices>
#include <QUrl>
    QDesktopServices::openUrl(
      QUrl("http://msxpertsuite.org/user-manuals/html/minexpert2-user-manual/"
           "index.html"));
  });
}


void
AboutDlg::setHistoryText()
{
  // qDebug();

  // We load the history file now. It is part of the source code in the form
  // of doc/history.html.

  QFile historyFile(":/doc/history.html");

  if(!historyFile.open(QFile::ReadOnly | QFile::Text))
    {
      qFatal("Programming error.");
    }

  QString history = historyFile.readAll();
  m_ui.historyTextEdit->setHtml(history);
}

void
AboutDlg::setupGraphicsView()
{
  mp_graphicsScene = new QGraphicsScene();

  QPixmap pixmap(":/images/splashscreen.png");

  QGraphicsPixmapItem *pixmapItem = mp_graphicsScene->addPixmap(pixmap);
  if(!pixmapItem)
    return;
  m_ui.graphicsView->setScene(mp_graphicsScene);
}


void
AboutDlg::showHowToCiteTab()
{
  m_ui.tabWidget->setCurrentIndex(1);
}


} // namespace minexpert

} // namespace msxps
