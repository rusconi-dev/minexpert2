/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes
#include <math.h>

/////////////////////// Qt includes
#include <QWidgetAction>
#include <QDoubleSpinBox>
#include <QHBoxLayout>
#include <QLabel>

/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "LabelledNumberLineEditAction.hpp"


namespace msxps
{
namespace minexpert
{


#include "LabelledNumberLineEditAction.hpp"

LabelledNumberLineEditAction::LabelledNumberLineEditAction(const QString &label,
                                                           double value,
                                                           double minimum,
                                                           double maximum,
                                                           double decimals,
                                                           bool is_double,
                                                           QObject *parent)
  : QWidgetAction(parent)
{
  m_label     = label;
  m_minimum   = minimum;
  m_maximum   = maximum;
  m_decimals = decimals;
  m_isDouble  = is_double;

  QWidget *composite_widget_p = new QWidget(static_cast<QWidget *>(parent));

  QHBoxLayout *layout_p = new QHBoxLayout(composite_widget_p);

  // The label that describes the value to enter in the spin box
  QLabel *label_p = new QLabel(label);
  layout_p->addWidget(label_p);

  // The spin box where the user enter the value
  mpa_lineEdit = new QLineEdit(composite_widget_p);

  if(value < minimum || value > m_maximum)
    qFatal("The value is not contained in [minimum--maximum]");

  // Set the spinbox value to the requested one.
  if(m_isDouble)
    {
      mpa_validator = new QDoubleValidator(m_minimum, m_maximum, m_decimals, this);
      mpa_lineEdit->setText(QString::number(value, 'f', m_decimals));
    }
  else
    {
      mpa_validator = new QIntValidator(m_minimum, m_maximum, this);
      mpa_lineEdit->setText(QString::number(static_cast<int>(value)));
    }

  layout_p->addWidget(mpa_lineEdit);

  composite_widget_p->setLayout(layout_p);

  setDefaultWidget(composite_widget_p);
}


LabelledNumberLineEditAction::~LabelledNumberLineEditAction()
{
}


QLineEdit *
LabelledNumberLineEditAction::getWidget() const
{
  return mpa_lineEdit;
}


QString
LabelledNumberLineEditAction::getLabel() const
{
  return m_label;
}


QVariant
LabelledNumberLineEditAction::getValue() const
{
  bool ok = false;

  if(m_isDouble)
  {
    double value = mpa_lineEdit->text().toDouble(&ok);

    if(!ok)
      return mpa_lineEdit->text();
    else
      return value;
  }
  else
  {
    // int
    int value = mpa_lineEdit->text().toInt(&ok);

    if(!ok)
      return mpa_lineEdit->text();
    else
      return value;
  }

  return QVariant();
}


} // namespace minexpert

} // namespace msxps
