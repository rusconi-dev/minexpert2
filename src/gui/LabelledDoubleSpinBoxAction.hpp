/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */

#pragma once

/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QWidgetAction>
#include <QDoubleSpinBox>


/////////////////////// pappsomspp includes


/////////////////////// Local includes


namespace msxps
{
namespace minexpert
{


class LabelledDoubleSpinBoxAction : public QWidgetAction
{
  Q_OBJECT

  public:
  LabelledDoubleSpinBoxAction(const QString &label,
                              double value = 0,
                              QObject *parent = nullptr);

  virtual ~LabelledDoubleSpinBoxAction();

  QDoubleSpinBox *getSpinBox() const;
  QString getLabel() const;
  double getValue() const;

  private:
  QString m_label;
  QDoubleSpinBox *mpa_spinBox = nullptr;
};


} // namespace minexpert

} // namespace msxps
