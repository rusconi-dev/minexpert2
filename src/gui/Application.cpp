/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QTimer>
#include <QIcon>
#include <QDebug>
#include <QDir>
#include <QStandardPaths>
#include <QLocale>


/////////////////////// pappsomspp includes
#include <pappsomspp/msfile/msfilereader.h>
#include <pappsomspp/msfile/msfileaccessor.h>
#include <pappsomspp/msrun/msrunid.h>
#include <pappsomspp/msrun/msrunreader.h>
#include <pappsomspp/utils.h>
#include <pappsomspp/processing/combiners/tracepluscombiner.h>
#include <pappsomspp/processing/combiners/massspectrumpluscombiner.h>
#include <pappsomspp/trace/trace.h>
#include <pappsomspp/trace/maptrace.h>


/////////////////////// Local includes
#include "Application.hpp"
#include <pappsomspp/processing/combiners/mzintegrationparams.h>
#include "gui/MsRunDataSetTableView.hpp"
#include "gui/MsRunDataSetTableViewItem.hpp"
#include "gui/MsRunDataSetTableViewModel.hpp"
#include "gui/MsRunDataSetTableViewProxyModel.hpp"


namespace msxps
{
namespace minexpert
{

void
myMessageOutputFormat(QtMsgType type,
                      const QMessageLogContext &context,
                      const QString &msg)
{
  QByteArray localMsg  = msg.toLocal8Bit();
  const char *file     = context.file ? context.file : "";
  const char *function = context.function ? context.function : "";

  // The file char arrray contains the full path to the file name.
  // We want to only have the basename of the file.

  QString file_path(file);
  QFileInfo file_info(file_path);
  QString file_name                     = file_info.fileName();
  const QByteArray file_name_char_array = file_name.toUtf8();
  char file_name_c_array[200];
  file_name_c_array[qMin(99, file_name_char_array.size())] = '\0';
  std::copy(file_name_char_array.constBegin(),
            file_name_char_array.constBegin() +
              qMin(99, file_name_char_array.size()),
            file_name_c_array);

  switch(type)
    {
      case QtDebugMsg:
        fprintf(stderr,
                "\t\t\tDebug: %s:%u\n\t\t%s\n%s\n",
                file,
                context.line,
                function,
                localMsg.constData());
        break;
      case QtInfoMsg:
        fprintf(stderr,
                "\t\t\tInfo: %s:%u\n\t\t%s\n%s\n",
                file,
                context.line,
                function,
                localMsg.constData());
        break;
      case QtWarningMsg:
        fprintf(stderr,
                "\t\t\tWarning %s:%u\n\t\t%s\n%s\n",
                file,
                context.line,
                function,
                localMsg.constData());
        break;
      case QtCriticalMsg:
        fprintf(stderr,
                "\t\t\tCritical %s:%u\n\t\t%s\n%s\n",
                file,
                context.line,
                function,
                localMsg.constData());
        break;
      case QtFatalMsg:
        fprintf(stderr,
                "\t\t\tFatal: %s:%u\n\t\t%s\n%s\n",
                file,
                context.line,
                function,
                localMsg.constData());
        break;
    }
}


Application::Application(int &argc, char **argv, const QString &moduleName)
  : QApplication{argc, argv}, m_moduleName{moduleName}
{
  // We are dealing with massive amounts of numerical data and the best locale
  // for that is "C".

  QLocale::setDefault(QLocale::C);

  QPixmap splash_pixmap(":/images/splashscreen.png");

  mpa_splash = new QSplashScreen(splash_pixmap, Qt::WindowStaysOnTopHint);
  mpa_splash->show();

  QTimer::singleShot(2000, this, SLOT(destroySplash()));

  // We now have to perform a number of initialization steps.

  QCoreApplication::setOrganizationName(m_moduleName);
  QCoreApplication::setOrganizationDomain("msxpertsuite.org");
  QCoreApplication::setApplicationName(m_moduleName);

  // The default window icon.
  QString pixmap_file_name = ":/images/icons/32x32/minexpert2.png";
  QPixmap icon_pixmap(pixmap_file_name);
  QIcon icon(icon_pixmap);
  setWindowIcon(icon);

  // Set the debugging message formatting pattern.
  // qSetMessagePattern(QString("Debug: %{file}:%{line}-%{function}():
  // %{message}"));
  qInstallMessageHandler(myMessageOutputFormat);

  // The configuration directory for all the settings of the program

  m_userConfigSettingsDirPath =
    QStandardPaths::writableLocation(QStandardPaths::ConfigLocation);

  if(m_userConfigSettingsDirPath.isEmpty())
    m_userConfigSettingsDirPath =
      QStandardPaths::writableLocation(QStandardPaths::HomeLocation);

  m_userConfigSettingsDirPath =
    m_userConfigSettingsDirPath + QDir::separator() + m_moduleName;

  m_userConfigSettingsFilePath =
    m_userConfigSettingsDirPath + QDir::separator() + "configSettings.ini";

  // qDebug() << "m_userConfigSettingsDirPath:" << m_userConfigSettingsDirPath;
  // qDebug() << "m_userConfigSettingsFilePath:" <<
  // m_userConfigSettingsFilePath;
}


Application::~Application()
{
}


void
Application::setDescription(QString &desc)
{
  m_description = desc;
}


QString
Application::description()
{
  return m_description;
}


void
Application::destroySplash()
{
  delete mpa_splash;
  mpa_splash = 0;
}


QString
Application::getUserConfigSettingsDirPath()
{
  return m_userConfigSettingsDirPath;
}


QString
Application::getUserConfigSettingsFilePath()
{
  return m_userConfigSettingsFilePath;
}


} // namespace minexpert

} // namespace msxps
