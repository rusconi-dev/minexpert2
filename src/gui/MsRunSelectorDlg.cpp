/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */

/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QSettings>


/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "MsRunSelectorDlg.hpp"
#include "Application.hpp"


namespace msxps
{
namespace minexpert
{


MsRunSelectorDlg::MsRunSelectorDlg(
  QWidget *parent_p,
  std::vector<pappso::MsRunIdCstSPtr> &ms_run_ids,
  QString module_name)
  : QDialog(parent_p), m_moduleName(module_name)
{
  m_ui.setupUi(this);

  setWindowTitle(QString("%1 - MS run selector").arg(m_moduleName));

  populateMsRunList(ms_run_ids);

  connect(m_ui.msRunListWidget,
          &QListWidget::itemActivated,
          this,
          &MsRunSelectorDlg::msRunIdItemActivated);

  connect(m_ui.cancelPushButton,
          &QPushButton::clicked,
          this,
          &MsRunSelectorDlg::cancelPushButtonClicked);
}


MsRunSelectorDlg::~MsRunSelectorDlg()
{
}


void
MsRunSelectorDlg::readSettings()
{
  QSettings settings(static_cast<Application *>(QCoreApplication::instance())
                       ->getUserConfigSettingsFilePath(),
                     QSettings::IniFormat);
  settings.beginGroup("MsRunSelectorDlg");

  restoreGeometry(settings.value("geometry").toByteArray());

  settings.endGroup();
}


void
MsRunSelectorDlg::writeSettings()
{
  QSettings settings(static_cast<Application *>(QCoreApplication::instance())
                       ->getUserConfigSettingsFilePath(),
                     QSettings::IniFormat);
  settings.beginGroup("MsRunSelectorDlg");

  settings.setValue("geometry", saveGeometry());

  settings.endGroup();
}


void
MsRunSelectorDlg::populateMsRunList(
  std::vector<pappso::MsRunIdCstSPtr> &ms_run_ids)
{
  for(auto &&ms_run_id : ms_run_ids)
    m_ui.msRunListWidget->addItem(ms_run_id->getRunId());
}


void
MsRunSelectorDlg::msRunIdItemActivated(QListWidgetItem *item)
{
  done(m_ui.msRunListWidget->row(item));
}


void
MsRunSelectorDlg::cancelPushButtonClicked()
{

  // Let the caller know that the user does not want to load any ms run from
  // this file.

  done(-1);
}


// This slot is called automagically if the user hits the Escape key to close
// the window.
void
MsRunSelectorDlg::reject()
{

  // Let the caller know that the user does not want to load any ms run from
  // this file.

  done(-1);
}


} // namespace minexpert

} // namespace msxps
