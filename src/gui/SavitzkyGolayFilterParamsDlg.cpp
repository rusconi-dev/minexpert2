/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QObject>
#include <QDebug>
#include <QWidget>
#include <QMessageBox>
#include <QSettings>
#include <QLineEdit>
#include <QCloseEvent>


/////////////////////// Local includes
#include "SavitzkyGolayFilterParamsDlg.hpp"
#include "Application.hpp"


namespace msxps
{
namespace minexpert
{


SavitzkyGolayFilterParamsDlg::SavitzkyGolayFilterParamsDlg(QWidget *parent)
  : QDialog(parent)
{
  if(parent == nullptr)
    qFatal("Programming error.");

  m_ui.setupUi(this);

  setupWidget();
}


SavitzkyGolayFilterParamsDlg::SavitzkyGolayFilterParamsDlg(
  QWidget *parent,
  const pappso::SavGolParams &mz_integration_params,
  const QColor &color)
  : QDialog(parent), m_savgolParams(mz_integration_params), m_color(color)
{
  if(parent == nullptr)
    qFatal("Programming error.");

  // qDebug().noquote() << "The mz integration params at dialog construction:"
  //<< m_savgolParams.toString();

  m_ui.setupUi(this);

  setupWidget();
}


SavitzkyGolayFilterParamsDlg::~SavitzkyGolayFilterParamsDlg()
{
  // qDebug();
  writeSettings();
}


void
SavitzkyGolayFilterParamsDlg::closeEvent([[maybe_unused]] QCloseEvent *event)
{
  // qDebug();

  writeSettings();

  emit savitzkyGolayFilterParamsDlgShouldBeDestroyedSignal();
}


//! Write the settings to as to restore the window geometry later.
void
SavitzkyGolayFilterParamsDlg::writeSettings()
{
  QSettings settings(static_cast<Application *>(QCoreApplication::instance())
                       ->getUserConfigSettingsFilePath(),
                     QSettings::IniFormat);
  settings.beginGroup("SavitzkyGolayFilterParamsDlg");

  settings.setValue("geometry", saveGeometry());

  settings.endGroup();
}


//! Read the settings to as to restore the window geometry.
void
SavitzkyGolayFilterParamsDlg::readSettings()
{
  QSettings settings(static_cast<Application *>(QCoreApplication::instance())
                       ->getUserConfigSettingsFilePath(),
                     QSettings::IniFormat);
  settings.beginGroup("SavitzkyGolayFilterParamsDlg");

  restoreGeometry(settings.value("geometry").toByteArray());

  settings.endGroup();
}


void
SavitzkyGolayFilterParamsDlg::setupWidget()
{

  // Set the right color to the line "tag" to help the user determine for which
  // graph the parameters were initialized.

  QPalette pal = palette();
  pal.setColor(QPalette::Window, m_color);
  m_ui.coloredLine->setAutoFillBackground(true);
  m_ui.coloredLine->setPalette(pal);

  // Now update all the values in the different controls.

  m_ui.nlSpinBox->setValue(m_savgolParams.nL);
  m_ui.nrSpinBox->setValue(m_savgolParams.nR);
  m_ui.mSpinBox->setValue(m_savgolParams.m);
  m_ui.ldSpinBox->setValue(m_savgolParams.lD);

  connect(m_ui.applyPushButton,
          &QPushButton::clicked,
          this,
          &SavitzkyGolayFilterParamsDlg::applyPushButtonClicked);

  readSettings();
}


QString
SavitzkyGolayFilterParamsDlg::toString() const
{
  return m_savgolParams.toString();
}


void
SavitzkyGolayFilterParamsDlg::keyPressEvent(QKeyEvent *event)
{
  // If Ctrl return, apply and close.

  if(event->key() == Qt::Key_Return && event->modifiers() & Qt::ControlModifier)
    {
      applyPushButtonClicked();

      close();
    }
}

void
SavitzkyGolayFilterParamsDlg::show()
{
  readSettings();

  QDialog::show();
}


void
SavitzkyGolayFilterParamsDlg::applyPushButtonClicked()
{
  // qDebug();

  m_savgolParams.initialize(m_ui.nlSpinBox->value(),
                            m_ui.nrSpinBox->value(),
                            m_ui.mSpinBox->value(),
                            m_ui.ldSpinBox->value(),
                            false);

  qDebug().noquote() << "New Savitzky-Golay parameters:\n"
                     << m_savgolParams.toString();

  emit savitzkyGolayFilterParamsChangedSignal(m_savgolParams);
}


} // namespace minexpert

} // namespace msxps
