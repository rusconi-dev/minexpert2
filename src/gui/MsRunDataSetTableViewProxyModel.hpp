/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */

#pragma once


/////////////////////// StdLib includes
#include <limits>


/////////////////////// Qt includes
#include <QSortFilterProxyModel>


/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "MsRunDataSetTableViewModel.hpp"
#include "../nongui/ProcessingFlow.hpp"


namespace msxps
{
namespace minexpert
{

class MsRunDataSetTableViewProxyModel : public QSortFilterProxyModel
{
  Q_OBJECT

  public:
  MsRunDataSetTableViewProxyModel(QObject *parent = nullptr);
  virtual ~MsRunDataSetTableViewProxyModel();

  bool convertFilteringStrings(std::size_t ms_level,
                               const QString &indices,
                               const QString &rt_values,
                               const QString &dt_values,
                               const QString &precursor_index_values,
                               const QString &precursor_mz_values,
                               const QString &precursor_z_values);

  void setPrecision(pappso::PrecisionPtr precision_p);

  protected:
  std::pair<std::size_t, std::size_t> m_indicesPair;
  std::size_t m_msLevel = 0;
  std::pair<double, double> m_rtValuesPair;
  std::pair<double, double> m_dtValuesPair;
  std::pair<std::size_t, std::size_t> m_precursorIndexPair;
  std::pair<double, double> m_precursorMzValuesPair;
  std::pair<std::size_t, std::size_t> m_precursorChargePair;

  // The precision/tolerance with which the various precursor mz values are to
  // be matched.
  pappso::PrecisionPtr mp_precision = nullptr;

  bool isValidFilterPair(const std::pair<std::size_t, std::size_t> &pair) const;
  bool isValidFilterPair(const std::pair<double, double> &pair) const;

  bool filterAcceptsRow(int sourceRow,
                        const QModelIndex &source_parent) const override;
  bool lessThan(const QModelIndex &left,
                const QModelIndex &right) const override;

  std::pair<std::size_t, std::size_t>
  parseSizeTString(const QString &text, size_t accepted_lower_value);
  std::pair<double, double> parseDoubleString(const QString &text,
                                              double accepted_lower_value);
};

} // namespace minexpert
} // namespace msxps
