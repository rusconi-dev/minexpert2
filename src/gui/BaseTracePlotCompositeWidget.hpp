
/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QWidget>

/////////////////////// QCustomPlot


/////////////////////// pappsomspp includes
#include <pappsomspp/widget/plotwidget/basetraceplotwidget.h>
#include <pappsomspp/trace/trace.h>


/////////////////////// Local includes
#include "ui_BasePlotCompositeWidget.h"
#include "BasePlotCompositeWidget.hpp"
#include "../nongui/MsRunDataSet.hpp"
#include "../nongui/ProcessingFlow.hpp"
#include "SavitzkyGolayFilterParamsDlg.hpp"


namespace msxps
{
namespace minexpert
{

class BaseTracePlotWnd;
class MsFragmentationSpecDlg;


// The BaseTracePlotCompositeWidget class is aimed at receiving QCPGraph_s, not
// QCPColorMap_s. This class implements all the basic features related to the
// presence of QCPGraph_s, like adding a Trace plot, for example, which would
// not work if the plot were of type QCPColorMap...
class BaseTracePlotCompositeWidget : public BasePlotCompositeWidget
{
  Q_OBJECT

  friend class BasePlotWnd;
  friend class BaseTracePlotWnd;

  public:
  explicit BaseTracePlotCompositeWidget(QWidget *parent,
                                        const QString &x_axis_label,
                                        const QString &y_axis_label);

  virtual ~BaseTracePlotCompositeWidget();

  virtual QCPGraph *addTrace(const pappso::Trace &trace,
                             QCPAbstractPlottable *parent_plottable_p,
                             MsRunDataSetCstSPtr ms_run_data_set_csp,
                             const QString &sample_name,
                             const ProcessingFlow &processing_flow,
                             const QColor &color);

  virtual void
  moveMouseCursorToNextGraphPoint(const pappso::BasePlotContext &context);

  void showSavitzkyGolayFilterParamsDlg();

  void savitzkyGolayFilterParamsChanged(pappso::SavGolParams sav_gol_params);

  void runFilter(const QString &filter_name);
  void runSavitzkyGolayFilter();

  bool savePeakListToClipboard();


  public slots:

  virtual void
  plotWidgetKeyPressEvent(const pappso::BasePlotContext &context) override;

  virtual void
  plotWidgetKeyReleaseEvent(const pappso::BasePlotContext &context) override;

  virtual void setNoiseSampleTrace();
  virtual void resetNoiseSampleTrace();

  signals:

  void plotWidgetMouseReleaseEventSignal(QMouseEvent *event);

  protected:
  // These are the members that are specific to the BaseTrace plot composite
  // widget.
  bool m_isSinglePointIntegrationMode = false;
  QCPRange m_integrationRange;
  SavitzkyGolayFilterParamsDlg *mp_savitzkyGolayFilterParamsDlg = nullptr;

  virtual void createMainMenu() override;
  pappso::Trace lowIntensitySignalRemoval(pappso::Trace &trace, double value);
};


} // namespace minexpert

} // namespace msxps
