/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */

#pragma once

/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QDialog>


/////////////////////// pappsomspp includes
#include <pappsomspp/msrun/msrunid.h>

/////////////////////// Local includes
#include "ui_MsRunSelectorDlg.h"


namespace msxps
{
namespace minexpert
{


class MsRunSelectorDlg : public QDialog
{
  Q_OBJECT

  public:
  explicit MsRunSelectorDlg(QWidget *parent_p,
                            std::vector<pappso::MsRunIdCstSPtr> &ms_run_ids,
                            // Not a reference because we may use this in a
                            // thread.
                            QString module_name);
  virtual ~MsRunSelectorDlg();

  protected:
  Ui::MsRunSelectorDlg m_ui;

  private:
  //! The name of the module, at the moment massXpert or mineXpert.
  QString m_moduleName;

  void writeSettings();
  void readSettings();

  void populateMsRunList(std::vector<pappso::MsRunIdCstSPtr> &ms_run_ids);

  void msRunIdItemActivated(QListWidgetItem *item);

  void cancelPushButtonClicked();
  void reject();
};


} // namespace minexpert

} // namespace msxps
