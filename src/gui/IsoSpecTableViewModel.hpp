/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// Qt includes
#include <QDebug>
#include <QAbstractTableModel>


/////////////////////// Local includes
#include "../nongui/IsoSpecEntity.hpp"
#include "IsoSpecDlg.hpp"
#include "IsoSpecTableView.hpp"

namespace msxps
{
namespace minexpert
{

enum class IsoSpecTableViewColumns
{
  // #include <libisospec++/isoSpec++.h>
  //
  // extern const int elem_table_ID[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
  // extern const int elem_table_atomicNo[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
  // extern const double
  // elem_table_probability[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
  // extern const double elem_table_mass[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
  // extern const int elem_table_massNo[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
  // extern const int
  // elem_table_extraNeutrons[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
  // extern const char* elem_table_element[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
  // extern const char* elem_table_symbol[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
  // extern const bool
  // elem_table_Radioactive[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
  // extern const double
  // elem_table_log_probability[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];

  ID,
  ELEMENT,
  SYMBOL,
  ATOMIC_NO,
  MASS,
  MASS_NO,
  EXTRA_NEUTRONS,
  PROBABILITY,
  LOG_PROBABILITY,
  RADIOACTIVE,
  TOTAL_COLUMNS
};


class IsoSpecTableViewModel : public QAbstractTableModel
{
  Q_OBJECT

  private:
  IsoSpecDlg *mp_parentDlg = nullptr;
  QList<IsoSpecEntity *> *mp_isoSpecEntityList;
  IsoSpecTableView *mp_tableView;

  public:
  IsoSpecTableViewModel(QList<IsoSpecEntity *> *isoSpecEntityList,
                        QObject *object);
  virtual ~IsoSpecTableViewModel();

  const QList<IsoSpecEntity *> *getIsoSpecEntityList() const;
  const IsoSpecDlg *parentDlg() const;

  void setTableView(IsoSpecTableView *);
  IsoSpecTableView *tableView();

  int rowCount(const QModelIndex &parent = QModelIndex()) const;
  int columnCount(const QModelIndex &parent = QModelIndex()) const;

  QVariant headerData(int, Qt::Orientation, int role = Qt::DisplayRole) const;

  QVariant data(const QModelIndex &parent = QModelIndex(),
                int role                  = Qt::DisplayRole) const;

  int addIsoSpecEntities(const QList<IsoSpecEntity *> &entityList);
  int removeIsoSpecEntities(int, int);
};

} // namespace minexpert

} // namespace msxps

