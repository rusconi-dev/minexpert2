/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * Inspiration from XTPcpp by Olivier Langella (olivier.langella@u-psud.fr)
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */

#pragma once

/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QThread>


/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "../nongui/MassDataIntegrator.hpp"

/// All the integrators that are based on the use of Ms Run Data Set Tree Node
// Visitors.

#include "MsRunDataSetTreeMassDataIntegratorToRt.hpp"

// All the integrators that are based on the use of Qualified Mass Spectrum
// Vector Visitors.

#include "QualifiedMassSpectrumVectorMassDataIntegratorToRt.hpp"
#include "QualifiedMassSpectrumVectorMassDataIntegratorToDt.hpp"
#include "QualifiedMassSpectrumVectorMassDataIntegratorToMz.hpp"
#include "QualifiedMassSpectrumVectorMassDataIntegratorToTicInt.hpp"
#include "QualifiedMassSpectrumVectorMassDataIntegratorToDtRtMz.hpp"
#include "QualifiedMassSpectrumVectorMassDataIntegratorToRtDt.hpp"


namespace msxps
{
namespace minexpert
{

class MassDataIntegratorTask : public QObject
{
  Q_OBJECT

  public:
  MassDataIntegratorTask();
  virtual ~MassDataIntegratorTask();

  protected:
  void closeEvent(QCloseEvent *event);

  public slots:

  void seedInitialTicChromatogramAndMsRunDataSetStatistics(
    MsRunDataSetTreeMassDataIntegratorToRt *mass_data_integrator_p);

  void
  integrateToRt(MsRunDataSetTreeMassDataIntegratorToRt *mass_data_integrator_p);

  void integrateToRt(
    QualifiedMassSpectrumVectorMassDataIntegratorToRt *mass_data_integrator_p);
  void integrateToMz(
    QualifiedMassSpectrumVectorMassDataIntegratorToMz *mass_data_integrator_p);

  void integrateToDt(
    QualifiedMassSpectrumVectorMassDataIntegratorToDt *mass_data_integrator_p);

  void
  integrateToTicIntensity(QualifiedMassSpectrumVectorMassDataIntegratorToTicInt
                            *mass_data_integrator_p);

  void integrateToDtRtMz(QualifiedMassSpectrumVectorMassDataIntegratorToDtRtMz
                           *mass_data_integrator_p);

  void integrateToDtRt(QualifiedMassSpectrumVectorMassDataIntegratorToRtDt
                         *mass_data_integrator_p);

  signals:
  void
  finishedIntegratingDataSignal(MassDataIntegrator *mass_data_integrator_p);

  void finishedIntegratingDataSignal(
    QualifiedMassSpectrumVectorMassDataIntegrator *mass_data_integrator_p);

  private:
};

} // namespace minexpert

} // namespace msxps
