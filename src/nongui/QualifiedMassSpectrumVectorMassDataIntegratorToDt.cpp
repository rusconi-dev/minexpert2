/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes
#include <vector>
#include <cmath>


/////////////////////// OPENMP include
#include <omp.h>

/////////////////////// Qt includes
#include <QDebug>
#include <QThread>


/////////////////////// pappsomspp includes
#include <pappsomspp/processing/combiners/tracepluscombiner.h>


/////////////////////// Local includes
#include "QualifiedMassSpectrumVectorMassDataIntegratorToDt.hpp"
#include "ProcessingStep.hpp"


int qualifiedMassSpectrumVectorMassDataIntegratorToDtMetaTypeId =
  qRegisterMetaType<
    msxps::minexpert::QualifiedMassSpectrumVectorMassDataIntegratorToDt>(
    "msxps::minexpert::QualifiedMassSpectrumVectorMassDataIntegratorToDt");

int qualifiedMassSpectrumVectorMassDataIntegratorToDtSPtrMetaTypeId =
  qRegisterMetaType<
    msxps::minexpert::QualifiedMassSpectrumVectorMassDataIntegratorToDtSPtr>(
    "msxps::minexpert::QualifiedMassSpectrumVectorMassDataIntegratorToDtSPtr");


namespace msxps
{
namespace minexpert
{

// Needed for qRegisterMetaType
QualifiedMassSpectrumVectorMassDataIntegratorToDt::
  QualifiedMassSpectrumVectorMassDataIntegratorToDt()
{
}


QualifiedMassSpectrumVectorMassDataIntegratorToDt::
  QualifiedMassSpectrumVectorMassDataIntegratorToDt(
    MsRunDataSetCstSPtr ms_run_data_set_csp,
    const ProcessingFlow &processing_flow,
    QualifiedMassSpectraVectorSPtr &qualified_mass_spectra_to_integrate_sp)
  : QualifiedMassSpectrumVectorMassDataIntegrator(
      ms_run_data_set_csp,
      processing_flow,
      qualified_mass_spectra_to_integrate_sp)
{
  m_processingFlow.setMsRunDataSetCstSPtr(mcsp_msRunDataSet);
}


// Needed for qRegisterMetaType
QualifiedMassSpectrumVectorMassDataIntegratorToDt::
  QualifiedMassSpectrumVectorMassDataIntegratorToDt(
    const QualifiedMassSpectrumVectorMassDataIntegratorToDt &other)
  : QualifiedMassSpectrumVectorMassDataIntegrator(other)
{
}


QualifiedMassSpectrumVectorMassDataIntegratorToDt::
  ~QualifiedMassSpectrumVectorMassDataIntegratorToDt()
{
}


void
QualifiedMassSpectrumVectorMassDataIntegratorToDt::integrate()
{
  qDebug();

  std::chrono::system_clock::time_point chrono_start_time =
    std::chrono::system_clock::now();

  // If there are no data, nothing to do.
  std::size_t mass_spectra_count =
    mcsp_qualifiedMassSpectraToIntegrateVector->size();

  if(!mass_spectra_count)
    {
      qDebug() << "The qualified mass spectrum vector is empty, nothing to do.";

      emit cancelOperationSignal();
    }

  qDebug() << "The number of mass spectra to handle:" << mass_spectra_count;

  // We need to clear the map trace!
  m_mapTrace.clear();

  emit setTaskDescriptionTextSignal("Integrating to a drift spectrum");

  // We need a processing flow to work, and, in particular, a processing step
  // from which to find the specifics of the calculation.

  if(!m_processingFlow.size())
    qFatal("The processing flow cannot be empty. Program aborted.");

  // When constructing this integrator, the processing flow passed as parameter
  // was used to gather innermost ranges data. When the steps that match
  // the innermost criterion, these are stored in m_innermostSteps2D. When the
  // steps are non-2D steps, then only the m_xxRange values are updated.

  // If the m_innermostSteps2D vector of ProcessingSteps is non-empty, then,
  // that means that we may have some MZ-related 2D processing steps to account
  // at combination time, because that is precisely the only moment that we can
  // do that work. To be able to provide selection polygon an mass
  // spectrum-specific dt|rt data, we have the SelectionPolygonSpec class. There
  // can be as many such class instances as required to document the integration
  // boundaries rt/mz and dt/mz for example. This is why we need to fill in the
  // vector of SelectionPolygonSpec and then set that to the combiner.

  // Note, however, that the hassle described above only is acceptable when the
  // selection polygon is not square. If the selection polygon is square, then
  // the ranges suffice, and they have been filled-in properly upon construction
  // of this integrator.

  qDebug() << "Now filling-in the selection_polygon_specs for any 2D step that "
              "has a source type involving the MZ data kind.";

  std::vector<pappso::SelectionPolygonSpec> selection_polygon_specs;

  std::size_t selection_polygon_spec_count =
    fillInSelectionPolygonSpecs(selection_polygon_specs);

  qDebug() << selection_polygon_spec_count
           << "selection polygon specs were retained for the configuration of "
              "the filter.";

  // At this point, allocate a visitor that is specific for the calculation of
  // the mass spectrum. Since we are not binning, we can use a visitor that
  // integrates to a pappso::Trace, not a pappso::MassSpectrum.

  // But we want to parallelize the computation. Se we will allocate a vector of
  // visitors (as many as possible for the available processor threads), each
  // visitor having a subset of the initial data to integrate.

  // Determine the best number of threads to use and how many spectra to provide
  // each of the threads.

  // In the pair below, first is the ideal number of threads and second is the
  // number of mass spectra per thread.
  std::pair<std::size_t, std::size_t> best_parallel_params =
    bestParallelIntegrationParams(mass_spectra_count);

  // Handy alias.
  using MassSpecVector = std::vector<pappso::QualifiedMassSpectrumCstSPtr>;
  using VectorIterator = MassSpecVector::const_iterator;

  // Prepare a set of iterators that will distribute all the mass spectra
  // uniformly.
  std::vector<std::pair<VectorIterator, VectorIterator>> iterators =
    calculateIteratorPairs(best_parallel_params.first,
                           best_parallel_params.second);

  // Set aside a vector of std::map<double, double> to store retention time
  // values and their TIC intensity

  // Handy aliases.
  using Pair             = std::pair<double, double>;
  using DtTicMap         = std::map<double, double>;
  using DtTicMapSPtr     = std::shared_ptr<DtTicMap>;
  using DtTicMapIterator = DtTicMap::iterator;

  std::vector<DtTicMapSPtr> vector_of_dt_spec_map;
  for(std::size_t iter = 0; iter < iterators.size(); ++iter)
    vector_of_dt_spec_map.push_back(std::make_shared<DtTicMap>());

  // Now that we have all the parallel material, we can start the parallel work.
  emit setProgressBarMaxValueSignal(mass_spectra_count);

  qDebug() << "Starting the iteration in the visitors.";

  omp_set_num_threads(iterators.size());
#pragma omp parallel for ordered
  for(std::size_t iter = 0; iter < iterators.size(); ++iter)
    {
      // qDebug() << "thread index:" << iter;

      // Get the iterator range that we need to deal in this thread.
      VectorIterator current_iterator = iterators.at(iter).first;
      VectorIterator end_iterator     = iterators.at(iter).second;

      // Get the map for this thread.

      DtTicMapSPtr current_dt_spec_map_sp = vector_of_dt_spec_map.at(iter);

      while(current_iterator != end_iterator)
        {
          // We want to be able to intercept any cancellation of the
          // operation.

          if(m_isOperationCancelled)
            break;

          // Make the computation.

          // For each mass spectrum all we need is compute its TIC value, that
          // is, the sum of all its intensities (the y value of the DataPoint
          // instances it contains).

          pappso::QualifiedMassSpectrumCstSPtr qualified_mass_spectrum_csp =
            *current_iterator;

          // Ensure that the qualified mass spectrum contains a mass spectrum
          // that effectively contains the binary (mz,i) data.

          qualified_mass_spectrum_csp =
            checkQualifiedMassSpectrum(qualified_mass_spectrum_csp);

          if(qualified_mass_spectrum_csp == nullptr)
            qFatal(
              "Programming error. Not possible that shared pointer to "
              "qualified mass spectrum be nullptr.");

          // Great, at this point we actually have the mass spectrum!

          // We now need to ensure that the processing flow allows for this mass
          // spectrum to be combined.

          // Immediately check if the qualified mass spectrum is of a MS level
          // that matches the greatest MS level found in the member processing
          // flow instance.

          if(!checkMsLevel(qualified_mass_spectrum_csp))
            {
              qDebug() << "The MsLevel check returned false";
              ++current_iterator;
              continue;
            }
          if(!checkMsFragmentation(qualified_mass_spectrum_csp))
            {
              qDebug() << "The MsFragmentation check returned false";
              ++current_iterator;
              continue;
            }

          // Make easy check about RT and DT.

          if(!checkRtRange(qualified_mass_spectrum_csp))
            {
              qDebug() << "checkRtRange failed check.";

              ++current_iterator;
              continue;
            }
          if(!checkDtRange(qualified_mass_spectrum_csp))
            {
              qDebug() << "checkDtRange failed check.";

              ++current_iterator;
              continue;
            }

          double mass_spectrum_sum_y = 0;

          // Handle the mobility data, either drift time or 1/K0 depending
          // on the file type. Do that now because we'll need the dt value when
          // performing the sumY calculation if a filtering with selection in
          // polygon is required.

          double dt = -1;

          // Get the format of the file, because that will condition the
          // way we provide mobility values.

          pappso::MzFormat file_format =
            qualified_mass_spectrum_csp->getMassSpectrumId()
              .getMsRunIdCstSPtr()
              ->getMzFormat();

          if(file_format == pappso::MzFormat::brukerTims)
            {
              QVariant dt_variant_value =
                qualified_mass_spectrum_csp->getParameterValue(
                  pappso::QualifiedMassSpectrumParameter::OneOverK0);

              if(!dt_variant_value.isValid())
                {
                  qFatal(
                    "The data are not Bruker timsTOF data. Inconsistency "
                    "found.");
                }

              bool ok = false;
              dt      = dt_variant_value.toDouble(&ok);
              if(!ok)
                {
                  qFatal("Failed to convert QVariant 1/K0 value to double.");
                }
            }
          else
            {
              dt = qualified_mass_spectrum_csp->getDtInMilliSeconds();
            }

          // At this point we need to account for any limitations in the m/z
          // dimension of the data. These limitations might be as easy as having
          // a single m/z range limit or as complex as having one or more 2D
          // steps involving the m/z dimension in the m_innermostSteps2D, if the
          // selection polygons are not rectangle.

          if(!selection_polygon_specs.size())
            {
              if(!std::isnan(m_mzRange.first) && !std::isnan(m_mzRange.second))
                {
                  mass_spectrum_sum_y =
                    qualified_mass_spectrum_csp->getMassSpectrumSPtr()->sumY(
                      m_mzRange.first, m_mzRange.second);
                }
              else
                {
                  mass_spectrum_sum_y =
                    qualified_mass_spectrum_csp->getMassSpectrumSPtr()->sumY();
                }
            }
          else
            {
              // It may be possible that the 2D steps limiting the m/z range
              // have been followed by 1D steps further limiting the m/Z range.
              // The point is, the 1D steps are not stored in
              // selection_polygon_specs. However, by necessity, if there is at
              // least one selection_polygon_spec, then the m_mzRange holds
              // values. Futher, if there had been a 1D step limiting even more
              // the m/z range, then that step would have been accounted for in
              // m_mzRange. So we can account that limit right away.

              if(std::isnan(m_mzRange.first) || std::isnan(m_mzRange.second))
                qFatal("Programming error.");

              pappso::Trace filtered_trace(
                *qualified_mass_spectrum_csp->getMassSpectrumSPtr());

              pappso::FilterResampleKeepXRange the_keep_filter(
                m_mzRange.first, m_mzRange.second);

              filtered_trace = the_keep_filter.filter(filtered_trace);

              pappso::FilterResampleKeepPointInPolygon the_polygon_filter(
                selection_polygon_specs);

              filtered_trace = the_polygon_filter.filter(
                filtered_trace,
                dt,
                qualified_mass_spectrum_csp->getRtInMinutes());

              mass_spectrum_sum_y = filtered_trace.sumY();
            }

          if(!mass_spectrum_sum_y)
            {
              ++current_iterator;
              continue;
            }

          // Try to insert the pair into the map. Check if that was done or
          // not.
          std::pair<DtTicMapIterator, bool> res =
            current_dt_spec_map_sp->insert(Pair(dt, mass_spectrum_sum_y));

          if(!res.second)
            {
              // One other same rt value was seen already (like in ion mobility
              // mass spectrometry, for example). Only increment the y value.

              res.first->second += mass_spectrum_sum_y;
            }

          // qDebug() << "Processed one more spectrum.";

          emit incrementProgressBarCurrentValueAndSetStatusTextSignal(
            1, "Processing spectra");

          // Now we can go to the next mass spectrum.
          ++current_iterator;
        }
      // End of
      // while(current_iterator != end_iterator)

      // qDebug() << "At the end of the current iterator pair, current map:";
      // for(auto &&pair : *current_dt_spec_map_sp)
      // qDebug().noquote() << "(" << pair.first << "," << pair.second << ")";
    }
  // End of
  // #pragma omp parallel for ordered
  // for(std::size_t iter = 0; iter < iterators.size(); ++iter)

  // In the loop above, for each thread, a std::map<double, double> map was
  // filled in with the rt, TIC pairs.

  // If the task was cancelled, the monitor widget was locked. We need to
  // unlock it.
  emit unlockTaskMonitorCompositeWidgetSignal();
  emit setupProgressBarSignal(0, vector_of_dt_spec_map.size() - 1);

  // We might be here because the user cancelled the operation in the for loop
  // above (visiting all the visitors). In this case m_isOperationCancelled is
  // true. We want to set it back to false, so that the following loop is gone
  // through. The user can ask that the operation be cancelled once more. But
  // we want that at least the performed work be used to show the trace.

  m_isOperationCancelled = false;
  int iter               = 0;

  for(auto &&map_sp : vector_of_dt_spec_map)
    {
      // qDebug();

      ++iter;

      if(m_isOperationCancelled)
        break;

      // qDebug() << "In the consolidating loop, iter:" << iter
      //<< "with a map of this size : " << map_sp->size();

      for(auto &&pair : *map_sp)
        {

          emit setStatusTextSignal(
            QString("Consolidating drift spectra from thread %1").arg(iter));

          emit setProgressBarCurrentValueSignal(iter);

          double rt  = pair.first;
          double tic = pair.second;

          std::pair<DtTicMapIterator, bool> res =
            m_mapTrace.insert(Pair(rt, tic));

          if(!res.second)
            {
              // One other same rt value was seen already (like in ion mobility
              // mass spectrometry, for example). Only increment the y value.

              res.first->second += tic;
            }
        }
    }

  // Write the data to file so that we can check. It works !!!
  // pappso::Utils::appendToFile(m_mapTrace.toString(), "/tmp/prova.txt");

  std::chrono::system_clock::time_point chrono_end_time =
    std::chrono::system_clock::now();

  QString chrono_string = pappso::Utils::chronoIntervalDebugString(
    "Integration to drift spectrum took:", chrono_start_time, chrono_end_time);

  emit logTextToConsoleSignal(chrono_string);

  // qDebug().noquote() << chrono_string;
}


} // namespace minexpert

} // namespace msxps
