/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes
#include <cmath>


/////////////////////// Qt includes
#include <QDebug>


/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "../nongui/globals.hpp"
#include "MassPeakShaperConfig.hpp"


namespace msxps
{
namespace minexpert
{


//! Width of a Gaussian/Lorentzian curve.
/*!
 * Describe how many times the whole gaussian/lorentzian curve should span
 * around the centroid mz ratio. 4 means that the peak will span
 : [ mzRatio - (2 * FWHM) --> mzRatio + (2 * FWHM) ]
 */
int FWHM_PEAK_SPAN_FACTOR = 4;


MassPeakShaperConfig::MassPeakShaperConfig()
  : m_resolution(0),
    m_fwhm(0),
    m_ionizationFormula(QString()),
    m_charge(1),
    m_pointCount(0),
    m_mzStep(0),
    m_normFactor(1),
    m_massPeakShapeType(MassPeakShapeType::GAUSSIAN)
{
}


MassPeakShaperConfig::MassPeakShaperConfig(int resolution,
                                           double fwhm,
                                           const QString &ionization_formula,
                                           int charge,
                                           int data_point_count,
                                           double mz_step,
                                           double norm_factor,
                                           MassPeakShapeType peak_shape_type)
  : m_resolution(resolution),
    m_fwhm(fwhm),
    m_ionizationFormula(ionization_formula),
    m_charge(charge),
    m_pointCount(data_point_count),
    m_mzStep(mz_step),
    m_normFactor(norm_factor),
    m_massPeakShapeType(peak_shape_type)
{
}


MassPeakShaperConfig::MassPeakShaperConfig(const MassPeakShaperConfig &other)
  : m_resolution(other.m_resolution),
    m_fwhm(other.m_fwhm),
    m_ionizationFormula(other.m_ionizationFormula),
    m_charge(other.m_charge),
    m_pointCount(other.m_pointCount),
    m_mzStep(other.m_mzStep),
    m_normFactor(other.m_normFactor),
    m_massPeakShapeType(other.m_massPeakShapeType)
{
}


MassPeakShaperConfig::~MassPeakShaperConfig()
{
}


void
MassPeakShaperConfig::operator=(const MassPeakShaperConfig &other)
{
  m_resolution        = other.m_resolution;
  m_fwhm              = other.m_fwhm;
  m_ionizationFormula = other.m_ionizationFormula;
  m_charge            = other.m_charge;
  m_pointCount        = other.m_pointCount;
  m_mzStep            = other.m_mzStep;
  m_normFactor        = other.m_normFactor;
  m_massPeakShapeType = other.m_massPeakShapeType;
}


void
MassPeakShaperConfig::setConfig(int resolution,
                                double fwhm,
                                const QString &ionization_formula,
                                int charge,
                                int pointCount,
                                double mzStep,
                                double normFactor,
                                MassPeakShapeType peakShapeType)
{
  m_resolution        = resolution;
  m_fwhm              = fwhm;
  m_ionizationFormula = ionization_formula;
  m_charge            = charge;
  m_pointCount        = pointCount;
  m_mzStep            = mzStep;
  m_normFactor        = normFactor;
  m_massPeakShapeType = peakShapeType;
}


void
MassPeakShaperConfig::setConfig(const MassPeakShaperConfig &other)
{
  m_resolution        = other.m_resolution;
  m_fwhm              = other.m_fwhm;
  m_ionizationFormula = other.m_ionizationFormula;
  m_charge            = other.m_charge;
  m_pointCount        = other.m_pointCount;
  m_mzStep            = other.m_mzStep;
  m_normFactor        = other.m_normFactor;
  m_massPeakShapeType = other.m_massPeakShapeType;
}


void
MassPeakShaperConfig::setResolution(int value)
{
  m_resolution = value;

  if(!m_resolution)
    {
      // We do not change m_fwhm that might have been set to a proper value.
    }
  else
    {
      m_fwhm = 0;
    }
}


int
MassPeakShaperConfig::resolution(double mz)
{
  if(m_resolution)
    return m_resolution;

  if(!m_fwhm)
    qFatal("Programming error.");

  m_resolution = mz / m_fwhm;

  return m_resolution;
}


void
MassPeakShaperConfig::setFwhm(double value)
{
  m_fwhm = value;

  if(!m_fwhm)
    {
      // We do not change m_resolution that might have been set to a proper
      // value.
    }
  else
    {
      m_resolution = 0;
    }
}


double
MassPeakShaperConfig::fwhm(double mz)
{
  if(m_fwhm)
    return m_fwhm;

  if(!m_resolution)
    qFatal("Programming error.");

  m_fwhm = mz / m_resolution;

  return m_fwhm;
}


QString
MassPeakShaperConfig::fwhmToString(double mz)
{
  return QString("%1").arg(fwhm(mz), 0, 'f', 6);
}


QString
MassPeakShaperConfig::resolutionToString() const
{
  QString string;
  string.setNum(m_resolution);

  return string;
}

// For the lorentzian, that is half of the fwhm.
double
MassPeakShaperConfig::halfFwhm(double mz)
{
  return (fwhm(mz) / 2);
}


// For the lorentzian, that is half of the fwhm.
QString
MassPeakShaperConfig::halfFwhmToString(double mz)
{
  QString string;
  string.setNum(fwhm(mz) / 2);

  return string;
}

void
MassPeakShaperConfig::setIonizationFormula(const QString &ionization_formula)
{
  m_ionizationFormula = ionization_formula;
}


QString
MassPeakShaperConfig::getIonizationFormula() const
{
  return m_ionizationFormula;
}


void
MassPeakShaperConfig::setCharge(int value)
{
  m_charge = value;
}


int
MassPeakShaperConfig::getCharge() const
{
  return m_charge;
}


QString
MassPeakShaperConfig::chargeToString() const
{
  QString string;
  string.setNum(m_charge);

  return string;
}


void
MassPeakShaperConfig::setPointCount(int value)
{
  m_pointCount = value;
}


int
MassPeakShaperConfig::getPointCount() const
{
  return m_pointCount;
}


QString
MassPeakShaperConfig::pointCountToString() const
{
  QString string;
  string.setNum(m_pointCount);

  return string;
}


void
MassPeakShaperConfig::setNormFactor(double value)
{
  m_normFactor = value;
}


double
MassPeakShaperConfig::normFactor()
{
  return m_normFactor;
}


QString
MassPeakShaperConfig::normFactorToString()
{
  QString string;
  string.setNum(m_normFactor);

  return string;
}

void
MassPeakShaperConfig::setMassPeakShapeType(MassPeakShapeType value)
{
  m_massPeakShapeType = value;
}


MassPeakShapeType
MassPeakShaperConfig::getMassPeakShapeType()
{
  return m_massPeakShapeType;
}


double
MassPeakShaperConfig::c(double mz)
{
  // c in the Gaussian curve is related to the fwhm value:
  double c = fwhm(mz) / (2 * sqrt(2 * log(2)));

  // qDebug() << < "c:" << c;

  return c;
}

QString
MassPeakShaperConfig::cToString(double mz)
{

  double cValue = c(mz);

  QString string;
  string.setNum(cValue);

  return string;
}


double
MassPeakShaperConfig::a(double mz)
{
  //  double pi = 3.1415926535897932384626433832795029;

  double a = (1 / (c(mz) * sqrt(2 * PI)));

  // qDebug() << "a:" << a;

  return a;
}


QString
MassPeakShaperConfig::aToString(double mz)
{
  double aValue = a(mz);

  QString string;
  string.setNum(aValue);

  return string;
}


double
MassPeakShaperConfig::gamma(double mz)
{
  double gamma = fwhm(mz) / 2;

  // qDebug() << "gamma:" << gamma;

  return gamma;
}

QString
MassPeakShaperConfig::gammaToString(double mz)
{
  double gammaValue = gamma(mz);

  QString string;
  string.setNum(gammaValue);

  return string;
}


void
MassPeakShaperConfig::setMzStep(double value)
{
  m_mzStep = value;
}


double
MassPeakShaperConfig::mzStep(double mz)
{

  // But what is the mz step ?
  //
  // We want the shape to be able to go down to baseline. Thus we want that the
  // shape to have a "basis" (or, better, a "ground") corresponding to twice the
  // FWHM on the left of the centroid and to twice the FWHM on the right (that
  // makes in total FWHM_PEAK_SPAN_FACTOR * FWHM).

  if(m_mzStep)
    return m_mzStep;

  if(!m_pointCount)
    qFatal("Programming error.");

  m_mzStep = (FWHM_PEAK_SPAN_FACTOR * fwhm(mz)) / m_pointCount;

  return m_mzStep;
}


QString
MassPeakShaperConfig::mzStepToString(double mz)
{
  QString string;
  string.setNum(mzStep(mz));

  return string;
}


QString
MassPeakShaperConfig::toString(double mz)
{
  QString string;

  if(m_massPeakShapeType == MassPeakShapeType::GAUSSIAN)
    {

      string = QString(
                 "Gaussian peak shaping:\n"
                 "Configuration for m/z value: %1\n"
                 "Resolution: %2\n"
                 "FWHM: %3\n"
                 "Number of points to shape the peak: %4\n"
                 "c: %5\n"
                 "c^2: %6\n"
                 "mz step: %7\n\n")
                 .arg(mz, 0, 'f', 5)
                 .arg(resolution(mz))
                 .arg(fwhm(mz), 0, 'f', 5)
                 .arg(m_pointCount)
                 .arg(c(mz), 0, 'f', 5)
                 .arg(c(mz) * c(mz), 0, 'f', 5)
                 .arg(mzStep(mz), 0, 'f', 5);
    }

  if(m_massPeakShapeType == MassPeakShapeType::LORENTZIAN)
    {
      string = QString(
                 "Lorentzian peak shaping:\n"
                 "Configuration for m/z value: %1\n"
                 "Resolution: %2\n"
                 "FWHM: %3\n"
                 "Number of points to shape the peak: %4\n"
                 "gamma: %5\n"
                 "gamma^2: %6\n"
                 "mz step: %7\n\n")
                 .arg(mz, 0, 'f', 5)
                 .arg(resolution(mz))
                 .arg(fwhm(mz), 0, 'f', 5)
                 .arg(m_pointCount)
                 .arg(gamma(mz), 0, 'f', 5)
                 .arg(gamma(mz) * gamma(mz), 0, 'f', 5)
                 .arg(mzStep(mz), 0, 'f', 5);
    }

  return string;
}

} // namespace minexpert

} // namespace msxps
