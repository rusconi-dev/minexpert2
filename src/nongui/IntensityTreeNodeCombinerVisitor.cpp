/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QDebug>
#include <QThread>


/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "IntensityTreeNodeCombinerVisitor.hpp"


namespace msxps
{
namespace minexpert
{


IntensityTreeNodeCombinerVisitor::IntensityTreeNodeCombinerVisitor(
  MsRunDataSetCstSPtr ms_run_data_set_csp,
  const ProcessingFlow &processing_flow)
  : BaseMsRunDataSetTreeNodeVisitor(ms_run_data_set_csp, processing_flow)
{
  //  qDebug() << "Processing flow has" << m_processingFlow.size() << "steps";
}


IntensityTreeNodeCombinerVisitor::IntensityTreeNodeCombinerVisitor(
  const IntensityTreeNodeCombinerVisitor &other)
  : BaseMsRunDataSetTreeNodeVisitor(other), m_ticIntensity(other.m_ticIntensity)
{
}


IntensityTreeNodeCombinerVisitor &
IntensityTreeNodeCombinerVisitor::operator=(
  const IntensityTreeNodeCombinerVisitor &other)
{
  if(this == &other)
    return *this;

  BaseMsRunDataSetTreeNodeVisitor::operator=(other);

  mcsp_msRunDataSet = other.mcsp_msRunDataSet;
  m_processingFlow  = other.m_processingFlow;
  m_ticIntensity    = other.m_ticIntensity;

  return *this;
}


IntensityTreeNodeCombinerVisitor::~IntensityTreeNodeCombinerVisitor()
{
  // qDebug();
}


// void
// IntensityTreeNodeCombinerVisitor::nodesToProcess(std::size_t
// nodes_to_process)
//{
//// Do nothing here because this nodes_to_process count is relative to the
//// thread in which this visitor is operating and thus does not correspond to
//// the total number of nodes that need processing. That total count of nodes
//// to be processed is set in the main thread once for all.
//}


void
IntensityTreeNodeCombinerVisitor::setProcessingFlow(
  const ProcessingFlow &processing_flow)
{
  m_processingFlow = processing_flow;
}


double
IntensityTreeNodeCombinerVisitor::getTicIntensity() const
{
  return m_ticIntensity;
}


bool
IntensityTreeNodeCombinerVisitor::visit(
  const pappso::MsRunDataSetTreeNode &node)
{

  // We visit a node, such that we can compute the TIC intensity.

  // qDebug() << "Visiting node:" << node.toString();

  if(!m_isProgressFeedbackSilenced)
    {
      // Whatever the status of this node (to be or not processed) let the user
      // know that we have gone through one more.

      // The "%c" format refers to the current value in the task monitor widget
      // that will craft the new text according to the current value after
      // having incremented it in the call below. This is necessary because this
      // visitor might be in a thread and the  we need to account for all the
      // thread when giving feedback to the user.

      emit incrementProgressBarCurrentValueAndSetStatusTextSignal(
        1, "Processed %c nodes");
    }

  // At this point we need to go deep in the processing flow's steps to check if
  // the node matches the whole set of the steps' specs.

  for(auto &&step : m_processingFlow)
    {
      if(!checkProcessingStep(*step, node))
        return false;
    }

  // At this point we know that the current node matches all the flow's steps'
  // specs. Return true, the node visitor user will know that he must use the
  // mass spectrum referenced by the current node.

  pappso::QualifiedMassSpectrumCstSPtr qualified_mass_spectrum_csp =
    node.getQualifiedMassSpectrum();

  if(qualified_mass_spectrum_csp == nullptr)
    qFatal("Cannot be nullptr");
  if(qualified_mass_spectrum_csp.get() == nullptr)
    qFatal("Cannot be nullptr");

  if(qualified_mass_spectrum_csp->getMassSpectrumSPtr() == nullptr)
    {
      // qDebug() << "Need to access the mass data right from the file.";

      std::size_t mass_spectrum_index =
        mcsp_msRunDataSet->getMsRunDataSetTreeCstSPtr()->massSpectrumIndex(
          &node);

      pappso::MsRunReaderCstSPtr ms_run_reader_csp =
        mcsp_msRunDataSet->getMsRunReaderCstSPtr();

      qualified_mass_spectrum_csp =
        std::make_shared<pappso::QualifiedMassSpectrum>(
          ms_run_reader_csp->qualifiedMassSpectrum(mass_spectrum_index, true));
    }

  if(qualified_mass_spectrum_csp == nullptr)
    {
      qFatal("Failed to read the mass spectral data from the file.");
    }

  // qDebug() << "The qualified mass spectrum:"
  //<< qualified_mass_spectrum_csp->toString()
  //<< "rt in minutes:" << qualified_mass_spectrum_csp->getRtInMinutes()
  //<< "dt in milliseconds:"
  //<< qualified_mass_spectrum_csp->getDtInMilliSeconds();

  // qDebug() << "The current qualified mass spectrum has size:"
  //<< qualified_mass_spectrum_csp->size();

  // A drift spectrum is nothing but a TIC value obtained by summing all the TIC
  // values for any mass spectrum that has a given dt value.

  if(m_limitMzRangeStart != std::numeric_limits<double>::max() &&
     m_limitMzRangeEnd != std::numeric_limits<double>::max())
    {
      m_ticIntensity +=
        qualified_mass_spectrum_csp->getMassSpectrumSPtr()->sumY(
          m_limitMzRangeStart, m_limitMzRangeEnd);
    }
  else
    {
      m_ticIntensity +=
        qualified_mass_spectrum_csp->getMassSpectrumSPtr()->sumY();
    }

  // qDebug() << "This visitor:" << this
  //<< "has TIC intensity" << m_ticIntensity;

  return true;
}


} // namespace minexpert

} // namespace msxps
