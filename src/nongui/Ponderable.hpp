/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */

#pragma once


/////////////////////// Qt includes


/////////////////////// Local includes
#include "globals.hpp"


namespace msxps
{
namespace minexpert
{


//! The Ponderable class provides a ponderable.
/*! Ponderable stores a monoisotopic and an average molecular
  mass. This class is used to derive chemical entities that have
  monoisotopic and average masses.

  For example, an isotope is the simplest chemical entity found on
  earth. It has a monoisotopic mass but no average mass. It thus
  cannot be considered a ponderable.

  Instead, an atom is the simplest ponderable found in massXpert,
  as it has both a monoisotopic mass and an average mass.

  Atom thus derives from Ponderable, while Isotope does
  not.
  */
class Ponderable
{
  protected:
  //! Monoisotopic mass.
  double m_mono;

  //! Average mass.
  double m_avg;

  public:
  Ponderable(double = 0, double = 0);
  Ponderable(const Ponderable &);
  virtual ~Ponderable();

  virtual Ponderable *clone() const;
  virtual void clone(Ponderable *) const;
  virtual void mold(const Ponderable &);
  virtual Ponderable &operator=(const Ponderable &);

  void setMasses(double, double);
  void setMass(double, MassType);
  void incrementMass(double, MassType);
  void masses(double * = 0, double * = 0) const;
  double mass(MassType) const;

  void clearMasses();

  void setMono(double);
  bool setMono(const QString &);
  void incrementMono(double);
  void decrementMono(double);
  double mono() const;
  double &rmono();
  QString monoString(int decimalPlaces) const;

  void setAvg(double);
  bool setAvg(const QString &);
  void incrementAvg(double);
  void decrementAvg(double);
  double avg() const;
  double &ravg();
  QString avgString(int decimalPlaces) const;

  virtual bool operator==(const Ponderable &) const;
  virtual bool operator!=(const Ponderable &) const;

  virtual bool calculateMasses();
  virtual bool accountMasses(double * = 0, double * = 0, int = 1) const;
};

} // namespace minexpert

} // namespace msxps


