/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */



/////////////////////// Local includes
#include "AtomCount.hpp"


namespace msxps
{
namespace minexpert
{

//! Constructs an AtomCount instance.
/*!

  Initializes the count to 0.

*/
AtomCount::AtomCount()
{
  m_count = 0;
}


//! Constructs a copy of \p other.
/*!

  \param other AtomCount to be used as a mold.

*/
AtomCount::AtomCount(const AtomCount &other)
  : Atom(other), m_count(other.m_count)
{
}


//! Creates a new atom count initialized with \c this.
/*!

  The initialization of the new atom count involves duplicating all
  the data of \c this, including all the isotopes of the isotope list.

  \return The newly created AtomCount, which should be deleted when no
  longer in use.

*/
AtomCount *
AtomCount::clone() const
{
  AtomCount *other = new AtomCount(*this);

  return other;
}


//! Modifies *\p other to be identical to \c this.
/*!

  \param other AtomCount.

*/
void
AtomCount::clone(AtomCount *other) const
{
  Q_ASSERT(other);

  if(other == this)
    return;

  other->m_count = m_count;

  Atom::clone(other);
}


//! Modifies \c this  to be identical to \p other.
/*!
  \param other AtomCount to be used as a mold.

*/
void
AtomCount::mold(const AtomCount &other)
{
  if(&other == this)
    return;

  m_count = other.m_count;

  Atom::mold(other);
}


//! Assigns \p other to \c this AtomCount.
/*!

  \param other AtomCount used as the mold to set values to \c this
  instance.

  \return a reference to \c this AtomCount.

*/
AtomCount &
AtomCount::operator=(const AtomCount &other)
{
  if(&other != this)
    mold(other);

  return *this;
}


//! Sets the count.
/*!

  \param count new count.

*/
void
AtomCount::setCount(int count)
{
  m_count = count;
}


//! Increment \c this count by \p times.
/*!

  \param times count to increment \c this count.

  \return the new count of this AtomCount instance.

*/
int
AtomCount::account(int times)
{
  m_count += times;
  return m_count;
}


//! Returns the count.
/*!

  \return the count.

*/
int
AtomCount::count() const
{
  return m_count;
}

//! Increments the masses in the arguments.
/*! This function performs the same that the base class function does
  unless the computation involves the atom count member data. The
  masses for \p this atom-count are not found in \p this instance,
  actually. First a reference atom is searched in \p reflist using \p
  this atom-count symbol as search criterion. The monoisotopic and
  average masses of the found atom are used for the computation. The
  arguments \p mono and \p avg are updated by incrementation of their
  values using the masses(monoisotopic and average) compounded by the
  \p times argument and by the count.

  For example, if \p times is 2 ; \p *mono is 100 and \p *avg is 101 ;
  \p this atom's monoisotopic mass is 200 and average mass is 202 ;
  the atom occurence count is 3, then the computation leads to \p
  *mono = 100 + 2 * 3 * 200 and \p *avg = 101 + 2 * 3 * 202.

  \param refList list of reference atoms.

  \param mono monoisotopic mass to update. Defaults to 0, in which
  case this mass is not updated.

  \param avg average mass to update. Defaults to 0, in which case this
  mass is not updated.

  \param times times that the increment should be performed. Defaults
  to 1.

  \return true if successful, false otherwise(that is, if no
  reference atom could be found in \p refList).

  \sa Atom::accountMasses(const QList<Atom *> &refList, double
  *mono, double *avg, int times).
  */
bool
AtomCount::accountMasses(const QList<Atom *> &refList,
                         double *mono,
                         double *avg,
                         int times) const
{
  int idx = -1;

  idx = symbolIndex(refList);

  if(idx == -1)
    return false;

  if(mono)
    *mono += refList.at(idx)->mono() * m_count * times;

  if(avg)
    *avg += refList.at(idx)->avg() * m_count * times;

  return true;
}

} // namespace minexpert

} // namespace msxps
