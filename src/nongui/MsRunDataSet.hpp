/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// StdLib includes
#include <map>


/////////////////////// Qt includes
#include <QMetaType>


/////////////////////// pappsomspp includes
#include <pappsomspp/types.h>
#include <pappsomspp/msrun/msrunreader.h>
#include <pappsomspp/msrun/msrunid.h>
#include <pappsomspp/massspectrum/qualifiedmassspectrum.h>
#include <pappsomspp/msrun/msrundatasettree.h>
#include <pappsomspp/msrun/msrundatasettreenode.h>
#include <pappsomspp/processing/combiners/mzintegrationparams.h>


/////////////////////// Local includes
#include "globals.hpp"
#include "MsRunDataSetStats.hpp"

namespace msxps
{
namespace minexpert
{


class MsRunDataSet;

typedef std::shared_ptr<MsRunDataSet> MsRunDataSetSPtr;
typedef std::shared_ptr<const MsRunDataSet> MsRunDataSetCstSPtr;


class MsRunDataSetStats;
//class pappso::MzIntegrationParams;

class MsRunDataSet : public QObject
{
  Q_OBJECT

  friend class MassSpecDataFileLoader;
  friend class MassDataIntegrator;
  friend class MsRunDataSetTreeMassDataIntegratorToRt;
  friend class MsRunDataSetTreeMassDataIntegratorToMz;
  friend class MsRunDataSetTreeMassDataIntegratorToDt;
  friend class MsRunDataSetTreeMassDataIntegratorToTicInt;
  friend class MsRunDataSetTreeMassDataIntegratorToDtRtMz;

  public:
  MsRunDataSet();

  explicit MsRunDataSet(
    QObject *parent,
    const pappso::MsRunReaderSPtr ms_run_reader_sp,
    // By default we use the streamed method to load the data.
    bool isStreamed = true);

  MsRunDataSet(const MsRunDataSet &other);
  ~MsRunDataSet();

  void setFileFormat(pappso::MzFormat file_format);
  pappso::MzFormat getFileFormat() const;

  pappso::MsRunIdCstSPtr getMsRunId() const;
  pappso::MsRunReaderSPtr getMsRunReaderSPtr();
  pappso::MsRunReaderCstSPtr getMsRunReaderCstSPtr() const;

  bool isStreamed() const;

  bool isIonMobilityExperiment() const;

  const MsRunDataSetStats &
  incrementStatistics(const pappso::MassSpectrumCstSPtr massSpectrum);
  const MsRunDataSetStats &consolidateStatistics();
  void resetStatistics();
  QString statisticsToString() const;

  void setMsRunDataSetStats(const MsRunDataSetStats &ms_run_data_set_stats);
  const MsRunDataSetStats &getMsRunDataSetStats() const;

  pappso::MsRunDataSetTreeCstSPtr getMsRunDataSetTreeCstSPtr() const;

  using MultiMap =
    std::multimap<pappso::pappso_double, pappso::QualifiedMassSpectrumCstSPtr>;

  using Map =
    std::map<pappso::pappso_double, pappso::QualifiedMassSpectrumCstSPtr>;

  pappso::QualifiedMassSpectrumCstSPtr massSpectrumAtIndex(std::size_t index) const;

  int size() const;

  void setParent(QObject *parent);

  void
  mapMassSpectrum(pappso::QualifiedMassSpectrumCstSPtr mass_spectrum_csptr);

  pappso::MzIntegrationParams craftInitialMzIntegrationParams() const;

  using VectorConstIterator =
    std::vector<pappso::MsRunDataSetTreeNode *>::const_iterator;

  std::pair<VectorConstIterator, VectorConstIterator>
  treeNodeIteratorRangeForRtRange(double start, double end) const;


#if 0 
		
		std::size_t addQualifiedMassSpectraInsideDtRtRange(
    double start,
    double end,
    std::size_t ms_level,
    std::vector<pappso::QualifiedMassSpectrumCstSPtr> &qual_mass_spectra,
    pappso::DataKind data_kind) const;

  std::size_t removeQualifiedMassSpectraOutsideDtRtRange(
    double start,
    double end,
    std::vector<pappso::QualifiedMassSpectrumCstSPtr> &qual_mass_spectra,
    pappso::DataKind data_kind) const;

#endif

  protected:
  private:
  // The ms run reader is required when the data are have been loaded in
  // streamed mode and thus the mass spectral data are not in memory, it is
  // necessary to access them from the file.
  pappso::MsRunReaderSPtr msp_msRunReader = nullptr;

  pappso::MzFormat m_fileFormat = pappso::MzFormat::unknown;

  //! Tells if the data are to be or were loaded in streamed mode or not.
  bool m_isStreamed = false;

  pappso::MsRunDataSetTreeSPtr msp_msRunDataSetTree = nullptr;

  //! Statistical data of the mass spectral data set.
  mutable MsRunDataSetStats m_msRunDataSetStats;

  // Maps relating rt/dt values with pappso::MsRunDataSetTreeNode pointers.

  using MapDoubleMsRunDataSetTreeNode =
    std::map<double, std::vector<pappso::MsRunDataSetTreeNode *>>;

  MapDoubleMsRunDataSetTreeNode m_rtMsRunDataSetTreeNodeMap;

  MapDoubleMsRunDataSetTreeNode m_dtMsRunDataSetTreeNodeMap;
};


} // namespace minexpert
} // namespace msxps


Q_DECLARE_METATYPE(msxps::minexpert::MsRunDataSet)
extern int msRunDataSetMetaTypeId;

Q_DECLARE_METATYPE(msxps::minexpert::MsRunDataSet *)
extern int msRunDataSetPtrMetaTypeId;

Q_DECLARE_METATYPE(msxps::minexpert::MsRunDataSetSPtr)
extern int msRunDataSetSPtrMetaTypeId;

Q_DECLARE_METATYPE(msxps::minexpert::MsRunDataSetCstSPtr)
extern int msRunDataSetCstSPtrMetaTypeId;
