/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// StdLib includes
#include <map>
#include <vector>
#include <memory>
#include <bitset>
#include <unordered_map>


/////////////////////// Qt includes
#include <QStringList>
#include <QString>
#include <QDateTime>


/////////////////////// pappsomspp includes


/////////////////////// Local includes


namespace msxps
{
namespace minexpert
{

typedef std::pair<QString, QString> ProcessingTypeStringPair;

class ProcessingType
{
  public:
  ProcessingType(std::bitset<32> bit_set);
  ProcessingType(const QString &brief_desc);
  ProcessingType(const ProcessingType &other);

  virtual ~ProcessingType();

  const QString &getBriefDesc() const;

  /*/// & = == operators */
  ProcessingType &operator=(const ProcessingType &other);
  bool operator==(const ProcessingType &other);
  std::bitset<32> operator&(const ProcessingType &mask);
  /* & = == operators */ ///

  std::bitset<32> bitSet() const;

  /*/// static bit set -- brief/detailed desc relating functions */
  static std::bitset<32> bitSet(QString brief_desc);
  static std::bitset<32> bitSet(const ProcessingType &processing_type);
  static QString briefDesc(std::bitset<32> bit_set);
  static QString detailedDesc(std::bitset<32> bit_set);
  /* static bit set -- brief/detailed desc relating functions */ ///

  /*/// static comparison and bit matching functions */
  static bool compare(const ProcessingType &a, const ProcessingType &b);

  static bool bitMatches(const ProcessingType &processing_type,
                         std::bitset<32> mask);
  static bool bitMatches(const ProcessingType &processing_type,
                         const QString &mask);
  static bool bitMatches(const ProcessingType &processing_type,
                         const ProcessingType &mask);
  /* static comparison and bit matching */ ///

  /*/// Begin non static comparison and bit matching */
  bool compare(const ProcessingType &other);

  bool bitMatches(std::bitset<32> mask) const;
  bool bitMatches(const ProcessingType &mask) const;
  bool bitMatches(const QString &mask) const;
  /* non static comparison and bit matching */ ///

  QString toBriefDesc() const;
  QString toBitString() const;

  private:
  static bool mapFilled;
  QString m_briefDesc = "[NOT_SET]";

  // This is a vector of ProcessingTypeDescription items that describe all the
  // processing types available in the program.

  /// Begin static private data and functions
  static std::vector<ProcessingTypeStringPair> processingTypes;

  static std::unordered_map<std::bitset<32>, ProcessingTypeStringPair>
    processingTypeRegistry;

  static bool isRegistered(QString brief_desc);
  static bool isRegistered(std::bitset<32> bit_set);
  /// End static private data and functions

  void registerProcessingTypes();
  void
  registerProcessingCompositeTypes(std::vector<QString> brief_desc_list,
                                   ProcessingTypeStringPair dest_desc_pair);
};


} // namespace minexpert

} // namespace msxps
