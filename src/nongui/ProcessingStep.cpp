/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes
#include <map>


/////////////////////// Qt includes
#include <QDebug>

/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "ProcessingStep.hpp"


namespace msxps
{
namespace minexpert
{


ProcessingStep::ProcessingStep()
  : m_dateAndTime(QDateTime::currentDateTimeUtc())
{
}


ProcessingStep::ProcessingStep(
  const pappso::SelectionPolygon &selection_polygon)
  : m_dateAndTime(QDateTime::currentDateTimeUtc()),
    m_selectionPolygon(selection_polygon)
{
}


ProcessingStep::ProcessingStep(const MsFragmentationSpec &ms_fragmentation_spec)
  : m_dateAndTime(QDateTime::currentDateTimeUtc())
{
  mpa_msFragmentationSpec = new MsFragmentationSpec(ms_fragmentation_spec);
}


ProcessingStep::ProcessingStep(const MsFragmentationSpec &ms_fragmentation_spec,
                               const pappso::SelectionPolygon selection_polygon)
  : m_dateAndTime(QDateTime::currentDateTimeUtc()),
    m_selectionPolygon(selection_polygon)
{
  mpa_msFragmentationSpec = new MsFragmentationSpec(ms_fragmentation_spec);
}


ProcessingStep::ProcessingStep(const ProcessingStep &other)
  : m_dateAndTime(other.m_dateAndTime),
    m_xSrcType(other.m_xSrcType),
    m_ySrcType(other.m_ySrcType),
    m_destType(other.m_destType),
    m_selectionPolygon(other.m_selectionPolygon),
    m_xAxisDataKind(other.m_xAxisDataKind),
    m_yAxisDataKind(other.m_yAxisDataKind)
{
  if(other.mpa_mzIntegrationParams != nullptr)
    mpa_mzIntegrationParams =
      new pappso::MzIntegrationParams(*other.mpa_mzIntegrationParams);

  if(other.mpa_msFragmentationSpec != nullptr)
    mpa_msFragmentationSpec =
      new MsFragmentationSpec(*other.mpa_msFragmentationSpec);
}


ProcessingStep::~ProcessingStep()
{
  if(mpa_mzIntegrationParams != nullptr)
    delete mpa_mzIntegrationParams;

  if(mpa_msFragmentationSpec != nullptr)
    delete mpa_msFragmentationSpec;
}


ProcessingStep &
ProcessingStep::operator=(const ProcessingStep &other)
{
  if(this == &other)
    return *this;

  m_dateAndTime = other.m_dateAndTime;

  m_xSrcType = other.m_xSrcType;
  m_ySrcType = other.m_ySrcType;

  m_destType = other.m_destType;

  m_selectionPolygon = other.m_selectionPolygon;

  m_xAxisDataKind = other.m_xAxisDataKind;
  m_yAxisDataKind = other.m_yAxisDataKind;

  if(other.mpa_mzIntegrationParams != nullptr)
    mpa_mzIntegrationParams =
      new pappso::MzIntegrationParams(*other.mpa_mzIntegrationParams);

  if(other.mpa_msFragmentationSpec != nullptr)
    mpa_msFragmentationSpec =
      new MsFragmentationSpec(*other.mpa_msFragmentationSpec);

  return *this;
}


void
ProcessingStep::setSrcProcessingType(pappso::Axis axis,
                                     const ProcessingType &processing_type)
{
  if(axis == pappso::Axis::x)
    m_xSrcType = processing_type;
  else if(axis == pappso::Axis::y)
    m_ySrcType = processing_type;
  else
    qFatal("Programming error.");
}


void
ProcessingStep::setSrcProcessingType(pappso::Axis axis,
                                     const QString &processing_type)
{
  return setSrcProcessingType(axis, ProcessingType(processing_type));
}


ProcessingType
ProcessingStep::getSrcProcessingType(pappso::Axis axis) const
{
  if(axis == pappso::Axis::x)
    return m_xSrcType;
  else if(axis == pappso::Axis::y)
    return m_ySrcType;

  return ProcessingType("NOT_SET");
}


void
ProcessingStep::setDestProcessingType(const ProcessingType &processing_type)
{
  m_destType = processing_type;
}

void
ProcessingStep::setDestProcessingType(const QString &processing_type)
{
  setDestProcessingType(ProcessingType(processing_type));
}

ProcessingType
ProcessingStep::getDestProcessingType() const
{
  return m_destType;
}


void
ProcessingStep::setSelectionPolygon(
  const pappso::SelectionPolygon &selection_polygon)
{
  m_selectionPolygon = selection_polygon;
}


const pappso::SelectionPolygon &
ProcessingStep::getSelectionPolygon() const
{
  return m_selectionPolygon;
}


void
ProcessingStep::resetSelectionPolygon()
{
  m_selectionPolygon.resetPoints();
}


bool
ProcessingStep::hasValidSelectionPolygon(bool &is_2D) const
{
  if(m_selectionPolygon.is1D())
    {
      is_2D = false;
      return true;
    }

  if(m_selectionPolygon.is2D())
    {
      is_2D = true;
      return true;
    }

  return false;
}


void
ProcessingStep::setDataKind(pappso::Axis axis, pappso::DataKind data_kind)
{
  if(static_cast<int>(axis) & static_cast<int>(pappso::Axis::x))
    m_xAxisDataKind = data_kind;
  else if(static_cast<int>(axis) & static_cast<int>(pappso::Axis::y))
    m_yAxisDataKind = data_kind;
  else
    qFatal("The axis provided is not correct.");
}


pappso::DataKind
ProcessingStep::getDataKind(pappso::Axis axis) const
{
  if(static_cast<int>(axis) & static_cast<int>(pappso::Axis::x))
    return m_xAxisDataKind;
  else if(static_cast<int>(axis) & static_cast<int>(pappso::Axis::y))
    return m_yAxisDataKind;
  else
    qFatal("The axis provided is not correct.");
}


bool
ProcessingStep::getRangeForAxis(pappso::Axis axis,
                                double &range_start,
                                double &range_end) const
{
  return m_selectionPolygon.range(axis, range_start, range_end);
}


bool
ProcessingStep::getRangeForAxisX(double &range_start, double &range_end) const
{
  return getRangeForAxis(pappso::Axis::x, range_start, range_end);
}


bool
ProcessingStep::getRangeForAxisY(double &range_start, double &range_end) const
{
  return getRangeForAxis(pappso::Axis::y, range_start, range_end);
}


void
ProcessingStep::setMzIntegrationParams(
  const pappso::MzIntegrationParams &mz_integration_params)
{
  mpa_mzIntegrationParams =
    new pappso::MzIntegrationParams(mz_integration_params);
}


const pappso::MzIntegrationParams *
ProcessingStep::getMzIntegrationParamsPtr() const
{
  return mpa_mzIntegrationParams;
}


void
ProcessingStep::setMsFragmentationSpec(
  const MsFragmentationSpec &fragmentation_spec)
{
  if(mpa_msFragmentationSpec != nullptr)
    delete mpa_msFragmentationSpec;

  mpa_msFragmentationSpec = new MsFragmentationSpec(fragmentation_spec);
}


MsFragmentationSpec
ProcessingStep::getMsFragmentationSpec() const
{
  if(mpa_msFragmentationSpec != nullptr)
    return *mpa_msFragmentationSpec;

  return MsFragmentationSpec();
}


MsFragmentationSpec *
ProcessingStep::getMsFragmentationSpecPtr()
{
  return mpa_msFragmentationSpec;
}


size_t
ProcessingStep::getMsLevel() const
{
  if(mpa_msFragmentationSpec != nullptr && mpa_msFragmentationSpec->isValid())
    return mpa_msFragmentationSpec->getMsLevel();

  return 0;
}


bool
ProcessingStep::hasValidFragmentationSpec() const
{
  if(mpa_msFragmentationSpec != nullptr)
    return mpa_msFragmentationSpec->isValid();
  else
    return false;
}


bool
ProcessingStep::isValid() const
{
  // A valid processing step has necessarily valid processing types and a valid
  // selection polygon and valid axis data kinds.

  if(ProcessingType::compare(m_xSrcType, ProcessingType("NOT_SET")))
    return false;

  // We do not check the m_ySrcType because if the starting point is not
  // bidimensional, that value is rightfully NOT_SET.

  // The destination type cannot be unset because that would not mean anything
  // from an integration stand point.
  if(ProcessingType::compare(m_destType, ProcessingType("NOT_SET")))
    return false;

  if(!m_selectionPolygon.is1D() && !m_selectionPolygon.is2D())
    return false;

  if(m_xAxisDataKind == pappso::DataKind::unset ||
     m_yAxisDataKind == pappso::DataKind::unset)
    return false;

  return true;
}


bool
ProcessingStep::srcMatches(std::bitset<32> bit_set) const
{
  return (m_xSrcType.bitMatches(bit_set) || m_ySrcType.bitMatches(bit_set));
}


bool
ProcessingStep::srcMatches(const ProcessingType &processing_type) const
{
  return srcMatches(processing_type.bitSet());
}


bool
ProcessingStep::srcMatches(const QString &brief_desc) const
{
  return srcMatches(ProcessingType(brief_desc));
}


bool
ProcessingStep::destMatches(std::bitset<32> bit_set) const
{
  return (m_destType.bitMatches(bit_set));
}


bool
ProcessingStep::destMatches(const ProcessingType &processing_type) const
{
  return destMatches(processing_type.bitSet());
}


bool
ProcessingStep::destMatches(const QString &brief_desc) const
{
  return destMatches(ProcessingType(brief_desc));
}


QString
ProcessingStep::toString(int offset, const QString &spacer) const
{
  QString lead;

  for(int iter = 0; iter < offset; ++iter)
    lead += spacer;

  QString text = lead;
  text += "Processing step at: ";
  text += m_dateAndTime.toString("yyyyMMdd-HH:mm:ss");
  text += "\n";

  text += "X axis Source Processing type: ";
  text += m_xSrcType.toBriefDesc();
  text += "\n";

  text += "Y axis Source Processing type: ";
  text += m_ySrcType.toBriefDesc();
  text += "\n";

  text += "Destination Processing type: ";
  text += m_destType.toBriefDesc();
  text += "\n";

  text += "X axis data kind:";
  text += QString::number(static_cast<int>(m_xAxisDataKind), 10);
  text += "\n";

  text += "Y axis data kind:";
  text += QString::number(static_cast<int>(m_yAxisDataKind), 10);
  text += "\n";

  text += "Selection polygon:";
  text += m_selectionPolygon.toString();
  text += "\n";

  if(m_selectionPolygon.is1D())
    text += "Polygon is mono-dimensional\n";
  else
    {
      text += "Polygon is bi-dimensional: ";

      if(m_selectionPolygon.isRectangle())
        text += "rectangle\n";
      else
        text += "oblique (skewed)\n";
    }

  if(mpa_mzIntegrationParams != nullptr)
    {
      // text += lead;
      // text +=  spacer;
      text += mpa_mzIntegrationParams->toString(offset + 2, spacer);
      // text += "\n";
    }

  if(mpa_msFragmentationSpec != nullptr)
    {
      // text += lead;
      // text +=  spacer;
      text += mpa_msFragmentationSpec->toString(offset + 2, spacer);
      // text += "\n";
    }

  return text;
}


} // namespace minexpert

} // namespace msxps
