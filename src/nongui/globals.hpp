/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

////////////////////////////// Qt includes
#include <QString>
#include <QList>
#include <QTextStream>


/*
   Bitwise stuff

   It is sometimes worth using an enum to name the bits:

   enum ThingFlags = {
   ThingMask = 0x0000,
   ThingFlag0 = 1 << 0,
   ThingFlag1 = 1 << 1,
   ThingError = 1 << 8,
   }

   Then use the names later on. I.e. write

   thingstate |= ThingFlag1;
   thingstate &= ~ThingFlag0;
   if (thing & ThingError) {...}

   to set, clear and test. This way you hide the magic numbers from the rest of
   your code.
   */


namespace msxps
{
namespace minexpert
{


//! Type of mass.
/*!
 * Can be monoisotopic or average or both.
 */
enum MassType
{
  MASS_NONE = 0x0000,                //!< No mass
  MASS_MONO = 1 << 0,                //!< Monoisotopic mass
  MASS_AVG  = 1 << 1,                //!< Average mass
  MASS_BOTH = (MASS_MONO | MASS_AVG) //!< Both masses
};


extern const double PI;

extern int ATOM_DEC_PLACES;
extern int OLIGOMER_DEC_PLACES;
extern int POLYMER_DEC_PLACES;
extern int PH_PKA_DEC_PLACES;

extern QRegularExpression gXyFormatMassDataRegExp;
extern QRegularExpression gEndOfLineRegExp;

void doubleVectorStatistics(std::vector<double> &vector,
                            double *sum,
                            double *average,
                            double *variance,
                            double *stdDeviation,
                            double *nonZeroSmallest,
                            double *smallest,
                            double *greatest,
                            double *median);

bool almostEqual(double value1, double value2, int decimalPlaces = 10);

QTextStream &qStdOut();

QString &unspacifyString(QString &text);

QString binaryRepresentation(int value);

QString elideText(const QString &text,
                  int charsLeft            = 4,
                  int charsRight           = 4,
                  const QString &delimitor = "...");

QString stanzify(const QString &text, int width);

QString stanzifyParagraphs(const QString &text, int width);

int zeroDecimals(double value);

double ppm(double value, double ppm);

double addPpm(double value, double ppm);

double removePpm(double value, double ppm);

double res(double value, double res);

double addRes(double value, double res);

double removeRes(double value, double res);

QString pointerAsString(void *pointer);


} // namespace minexpert

} // namespace msxps
