/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */

#pragma once

/////////////////////// StdLib includes


/////////////////////// Qt includes


/////////////////////// pappsomspp includes
#include <pappsomspp/massspectrum/qualifiedmassspectrum.h>
#include <pappsomspp/msrun/msrundatasettreenode.h>
#include <pappsomspp/msrun/msrundatasettreevisitor.h>
#include <pappsomspp/processing/combiners/tracepluscombiner.h>


/////////////////////// Local includes
#include "globals.hpp"
#include "ProcessingFlow.hpp"
#include "MsRunDataSet.hpp"
#include "BaseMsRunDataSetTreeNodeVisitor.hpp"
#include <pappsomspp/processing/combiners/mzintegrationparams.h>


namespace msxps
{
namespace minexpert
{

class RtDtMzColorMapsTreeNodeCombinerVisitor;

typedef std::shared_ptr<RtDtMzColorMapsTreeNodeCombinerVisitor>
  RtDtMzColorMapsTreeNodeCombinerVisitorSPtr;


class RtDtMzColorMapsTreeNodeCombinerVisitor
  : public BaseMsRunDataSetTreeNodeVisitor
{

  public:
  RtDtMzColorMapsTreeNodeCombinerVisitor(
    MsRunDataSetCstSPtr ms_run_data_set_csp,
    const ProcessingFlow &processing_flow,
    pappso::DataKind data_kind);

  RtDtMzColorMapsTreeNodeCombinerVisitor(
    const RtDtMzColorMapsTreeNodeCombinerVisitor &other);

  virtual ~RtDtMzColorMapsTreeNodeCombinerVisitor();

  RtDtMzColorMapsTreeNodeCombinerVisitor &
  operator=(const RtDtMzColorMapsTreeNodeCombinerVisitor &other);

  // Overrides the base class.
  bool visit(const pappso::MsRunDataSetTreeNode &node) override;

  void setProcessingFlow(const ProcessingFlow &processing_flow);

  const std::map<double, pappso::MapTrace> &getDoubleMapTraceMap() const;

  pappso::DataKind getDataKind() const;

  virtual bool
  checkProcessingStep(const ProcessingStep &step,
                      const pappso::MsRunDataSetTreeNode &node) override;

  virtual bool checkProcessingSpec(
    const std::pair<ProcessingType, ProcessingSpec *> &spec_type_pair,
    const pappso::MsRunDataSetTreeNode &node) override;

  virtual bool checkProcessingTypeByRt(
    const std::pair<ProcessingType, ProcessingSpec *> &spec_type_pair,
    const pappso::MsRunDataSetTreeNode &node) override;

  virtual bool checkProcessingTypeByMz(
    const std::pair<ProcessingType, ProcessingSpec *> &spec_type_pair,
    const pappso::MsRunDataSetTreeNode &node) override;

  virtual bool checkProcessingTypeByDt(
    const std::pair<ProcessingType, ProcessingSpec *> &spec_type_pair,
    const pappso::MsRunDataSetTreeNode &node) override;

  protected:
  // Are we handling dt or rt data in relation to mz?
  pappso::DataKind m_dataKind;

  pappso::TracePlusCombiner m_combiner;

  // This is a map that associates a MapTrace object that is the receptable of
  // the mass combination for all the spectra that were acquired at a given
  // rt/dt retention time value.  This happens when doing ion mobility mass
  // spectrometry, where for a given scan start time (that is, a rt value),
  // there might be 200 spectra (the mobility spectra, like in the Waters Synapt
  // acquisitions).

  std::map<double, pappso::MapTrace> m_doubleMapTraceMap;
};


} // namespace minexpert

} // namespace msxps
