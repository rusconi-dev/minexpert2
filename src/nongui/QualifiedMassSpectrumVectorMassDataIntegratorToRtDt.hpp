/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// StdLib includes
#include <vector>


/////////////////////// Qt includes


/////////////////////// pappsomspp includes
#include <pappsomspp/trace/maptrace.h>


/////////////////////// Local includes
#include "ProcessingStep.hpp"
#include "ProcessingFlow.hpp"
#include "QualifiedMassSpectrumVectorMassDataIntegrator.hpp"


namespace msxps
{
namespace minexpert
{

class QualifiedMassSpectrumVectorMassDataIntegratorToRtDt;

typedef std::shared_ptr<QualifiedMassSpectrumVectorMassDataIntegratorToRtDt>
  QualifiedMassSpectrumVectorMassDataIntegratorToRtDtSPtr;

typedef std::shared_ptr<
  const QualifiedMassSpectrumVectorMassDataIntegratorToRtDt>
  QualifiedMassSpectrumVectorMassDataIntegratorToRtDtCstSPtr;

class QualifiedMassSpectrumVectorMassDataIntegratorToRtDt
  : public QualifiedMassSpectrumVectorMassDataIntegrator
{
  Q_OBJECT

  public:
  QualifiedMassSpectrumVectorMassDataIntegratorToRtDt();

  QualifiedMassSpectrumVectorMassDataIntegratorToRtDt(
    MsRunDataSetCstSPtr ms_run_data_set_csp,
    const ProcessingFlow &processing_flow,
    QualifiedMassSpectraVectorSPtr &qualified_mass_spectra_to_integrate_sp);

  QualifiedMassSpectrumVectorMassDataIntegratorToRtDt(
    const QualifiedMassSpectrumVectorMassDataIntegratorToRtDt &other);

  virtual ~QualifiedMassSpectrumVectorMassDataIntegratorToRtDt();

  pappso::DataKind getLastIntegrationDataKind() const;

  std::size_t getColorMapKeyCellCount() const;
  std::size_t getColorMapMzCellCount() const;

  double getColorMapMinKey() const;
  double getColorMapMaxKey() const;

  double getColorMapMinMz() const;
  double getColorMapMaxMz() const;

  using Map = std::map<double, pappso::MapTrace>;
  const std::shared_ptr<Map> &getRtMapTraceMapSPtr() const;


  public slots:

  void integrate();


  signals:

  protected:
  // m_colorMapKeyCellCount: the number of rt values in the m_rtMapTraceMapSPtr
  // map.
  std::size_t m_colorMapKeyCellCount = 0;

  // m_colorMapMzCellCount: the maximum number of m/z data points
  std::size_t m_colorMapMzCellCount  = 0;

  // The m_colorMapMinKey is the smallest rt value
  double m_colorMapMinKey = std::numeric_limits<double>::max();
  // The m_colorMapMinKey is the greatest rt value
  double m_colorMapMaxKey = std::numeric_limits<double>::min();

  // m_colorMapMinMz: the smallest m/z value of all the data
  double m_colorMapMinMz = std::numeric_limits<double>::max();

  // m_colorMapMinMz: the greatest m/z value of all the data
  double m_colorMapMaxMz = std::numeric_limits<double>::min();

  // The map relating rt value with MapTrace (that in turn relate dt values
  // with TICint).
  std::shared_ptr<Map> m_rtMapTraceMapSPtr = nullptr;

  pappso::DataKind m_lastIntegrationDataKind;
};

} // namespace minexpert

} // namespace msxps


Q_DECLARE_METATYPE(
  msxps::minexpert::QualifiedMassSpectrumVectorMassDataIntegratorToRtDt)
extern int qualifiedMassSpectrumVectorMassDataIntegratorToRtDtMetaTypeId;

Q_DECLARE_METATYPE(
  msxps::minexpert::QualifiedMassSpectrumVectorMassDataIntegratorToRtDtSPtr)
extern int qualifiedMassSpectrumVectorMassDataIntegratorToRtDtSPtrMetaTypeId;

