/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes
#include <iostream>
#include <iomanip>
#include <cmath>


/////////////////////// OpenMP include
#include <omp.h>

/////////////////////// Qt includes
#include <QDebug>
#include <QThread>

/////////////////////// pappsomspp includes
#include <pappsomspp/processing/combiners/tracepluscombiner.h>


/////////////////////// Local includes
#include "MsRunDataSetTreeMassDataIntegratorToRt.hpp"
#include "ProcessingStep.hpp"
#include "ProcessingType.hpp"
#include "BaseMsRunDataSetTreeNodeVisitor.hpp"
#include "TicChromTreeNodeCombinerVisitor.hpp"
#include "MsRunStatisticsTreeNodeVisitor.hpp"
#include "MultiTreeNodeCombinerVisitor.hpp"


int msRunDataSetTreeMassDataIntegratorToRtMetaTypeId =
  qRegisterMetaType<msxps::minexpert::MsRunDataSetTreeMassDataIntegratorToRt>(
    "msxps::minexpert::MsRunDataSetTreeMassDataIntegratorToRt");

int msRunDataSetTreeMassDataIntegratorToRtSPtrMetaTypeId = qRegisterMetaType<
  msxps::minexpert::MsRunDataSetTreeMassDataIntegratorToRtSPtr>(
  "msxps::minexpert::MsRunDataSetTreeMassDataIntegratorToRtSPtr");


namespace msxps
{
namespace minexpert
{


MsRunDataSetTreeMassDataIntegratorToRt::MsRunDataSetTreeMassDataIntegratorToRt()
  : MassDataIntegrator()
{
}


MsRunDataSetTreeMassDataIntegratorToRt::MsRunDataSetTreeMassDataIntegratorToRt(
  MsRunDataSetCstSPtr ms_run_data_set_csp)
  : MassDataIntegrator(ms_run_data_set_csp)
{
  // Essential that the m_processingFlow member is configured to have the right
  // pointer to the ms run data set.

  m_processingFlow.setMsRunDataSetCstSPtr(ms_run_data_set_csp);
}


MsRunDataSetTreeMassDataIntegratorToRt::MsRunDataSetTreeMassDataIntegratorToRt(
  MsRunDataSetCstSPtr ms_run_data_set_csp,
  const ProcessingFlow &processing_flow)
  : MassDataIntegrator(ms_run_data_set_csp, processing_flow)
{
  // Essential that the m_processingFlow member is configured to have the right
  // pointer to the ms run data set.
  if(ms_run_data_set_csp != m_processingFlow.getMsRunDataSetCstSPtr())
    qFatal("The pointers should be identical.");
}


MsRunDataSetTreeMassDataIntegratorToRt::MsRunDataSetTreeMassDataIntegratorToRt(
  const MsRunDataSetTreeMassDataIntegratorToRt &other)
  : MassDataIntegrator(other)
{
  // Essential that the m_processingFlow member is configured to have the right
  // pointer to the ms run data set.
  if(other.mcsp_msRunDataSet != m_processingFlow.getMsRunDataSetCstSPtr())
    qFatal("The pointers should be identical.");
}


MsRunDataSetTreeMassDataIntegratorToRt::
  ~MsRunDataSetTreeMassDataIntegratorToRt()
{
  // qDebug() << "Destroying integrator:" << this;
}


ProcessingFlow
MsRunDataSetTreeMassDataIntegratorToRt::getProcessingFlow() const
{
  return m_processingFlow;
}


const MsRunDataSetStats &
MsRunDataSetTreeMassDataIntegratorToRt::getMsRunDataSetStats() const
{
  return m_msRunDataSetStats;
}


void
MsRunDataSetTreeMassDataIntegratorToRt::appendProcessingStep(
  ProcessingStep *processing_step_p)
{
  if(processing_step_p == nullptr)
    qFatal("The pointer cannot be nullptr.");

  m_processingFlow.push_back(processing_step_p);
}


void
MsRunDataSetTreeMassDataIntegratorToRt::
  seedInitialTicChromatogramAndMsRunDataSetStatistics()
{
  std::chrono::system_clock::time_point chrono_start_time =
    std::chrono::system_clock::now();

  // The number of nodes we want is that of the root nodes, not of all the
  // nodes, that is, we want the MS1 nodes. Indeed, the division of the nodes
  // into the various threads can only be performed with the root nodes.
  std::size_t node_count =
    mcsp_msRunDataSet->getMsRunDataSetTreeCstSPtr()->getRootNodes().size();

  // If there are no data, nothing to do.
  if(!node_count)
    {
      qDebug() << "The ms run data set is empty, nothing to do.";

      emit cancelOperationSignal();

      return;
    }

  // qDebug() << "root nodes count:" << node_count;

  // Document that we only care of the MS1 data, because we are computing an
  // initial TIC chromatogram.
  MsFragmentationSpec ms_frag_spec;
  ms_frag_spec.setMsLevel(1);

  // We need to craft a new processing step, push it back to the processing
  // flow and then call the proper integration function.

  ProcessingStep *step_p = new ProcessingStep(ms_frag_spec);

  double range_start = mcsp_msRunDataSet->getMsRunDataSetTreeCstSPtr()
                         ->getRootNodes()
                         .front()
                         ->getQualifiedMassSpectrum()
                         ->getRtInMinutes();
  double range_end = mcsp_msRunDataSet->getMsRunDataSetTreeCstSPtr()
                       ->getRootNodes()
                       .back()
                       ->getQualifiedMassSpectrum()
                       ->getRtInMinutes();

  // qDebug() << "Right from the ms run data set, get rt range start:"
  //<< range_start << "and rt range end:" << range_end;

  pappso::SelectionPolygon selection_polygon;
  selection_polygon.set1D(range_start, range_end);

  step_p->setSelectionPolygon(selection_polygon);

  // qDebug() << "Created 1D selection polygon:"
  //<< step_p->getSelectionPolygon().toString();

  // qDebug() << "Is1D:" << step_p->getSelectionPolygon().is1D();

  step_p->setSrcProcessingType(pappso::Axis::x, "FILE_RT");
  step_p->setDataKind(pappso::Axis::x, pappso::DataKind::rt);

  step_p->setSrcProcessingType(pappso::Axis::y, "NOT_SET");
  step_p->setDataKind(pappso::Axis::y, pappso::DataKind::unset);

  // Now document the destination processing type:
  step_p->setDestProcessingType("RT");

  // At this point we have document all the necessary to perform the
  // integration.

  m_processingFlow.push_back(step_p);

  // We need to clear the map trace!
  m_mapTrace.clear();

  // Create as many multi-visitors as there are available threads.

  // emit setProgressBarMaxValueSignal(node_count);

  // In the pair below, first is the ideal number of threads and second is the
  // number of nodes per thread.
  std::pair<std::size_t, std::size_t> best_parallel_params =
    bestParallelIntegrationParams(node_count);

  // qDebug() << "ideal_thread_count:" << best_parallel_params.first
  //<< "nodes_per_thread:" << best_parallel_params.second;

  using Iterator = std::vector<pappso::MsRunDataSetTreeNode *>::const_iterator;

  std::vector<MultiTreeNodeCombinerVisitorSPtr> visitors;
  std::vector<std::pair<Iterator, Iterator>> iterators =
    calculateMsRunDataSetTreeNodeIteratorPairs(
      node_count, best_parallel_params.first, best_parallel_params.second);

  for(std::size_t iter = 0; iter < iterators.size(); ++iter)
    {
      // qDebug() << "thread index:" << iter;

      MultiTreeNodeCombinerVisitorSPtr multi_visitor_sp =
        std::make_shared<MultiTreeNodeCombinerVisitor>(mcsp_msRunDataSet,
                                                       m_processingFlow);

      // Now configure the visitor to hold as many visitors as we need. We want
      // one visitor for initial TIC chrom calculation and one for the initial
      // ms run data set statistics.

      // Start with first visitor that will compute the TIC chromatogram
      TicChromTreeNodeCombinerVisitorSPtr tic_visitor_sp =
        std::make_shared<TicChromTreeNodeCombinerVisitor>(mcsp_msRunDataSet,
                                                          m_processingFlow);

      multi_visitor_sp->m_visitors.push_back(tic_visitor_sp);

      // And now the other visitor that will compute some statistics for the ms
      // run data set.

      // Set aside a stats holder instance.

      MsRunDataSetStats ms_run_data_set_stats;

      MsRunStatisticsTreeNodeVisitorSPtr stats_visitor_sp =
        std::make_shared<MsRunStatisticsTreeNodeVisitor>(
          mcsp_msRunDataSet, m_processingFlow, ms_run_data_set_stats);

      multi_visitor_sp->m_visitors.push_back(stats_visitor_sp);

      // We want to be able to intercept any cancellation of the operation.

      connect(this,
              &MsRunDataSetTreeMassDataIntegratorToRt::cancelOperationSignal,
              [multi_visitor_sp]() { multi_visitor_sp->cancelOperation(); });

      // The visitor gets the number of nodes to process from the data set tree
      // node. This signal tells the final user of the signal to set the max
      // value of the progress bar to number_of_nodes_to_process.

      connect(multi_visitor_sp.get(),
              &MultiTreeNodeCombinerVisitor::setProgressBarMaxValueSignal,
              [this](std::size_t number_of_nodes_to_process) {
                emit setProgressBarMaxValueSignal(number_of_nodes_to_process);
              });

      // The visitor emits the signal to tell that the currently iterated node
      // has number_of_nodes_to_process children to process. Because the visitor
      // might operate in a thread and that there might be other threads
      // handling other nodes, we do not consider that the
      // number_of_nodes_to_process value is the total number of nodes to
      // process but only the number of nodes that the visitor is handling. We
      // thus update the max progress bar value by number_of_nodes_to_process.
      // In a typical setting, the user of the signal is the monitoring object,
      // be it a non-gui object or the TaskMonitorCompositeWidget.
      connect(
        multi_visitor_sp.get(),
        &TicChromTreeNodeCombinerVisitor::incrementProgressBarMaxValueSignal,
        [this](std::size_t number_of_nodes_to_process) {
          emit incrementProgressBarMaxValueSignal(number_of_nodes_to_process);
        });

      connect(
        multi_visitor_sp.get(),
        &TicChromTreeNodeCombinerVisitor::setProgressBarCurrentValueSignal,
        [this](std::size_t number_of_processed_nodes) {
          emit setProgressBarCurrentValueSignal(number_of_processed_nodes);
        });

      connect(multi_visitor_sp.get(),
              &TicChromTreeNodeCombinerVisitor::
                incrementProgressBarCurrentValueAndSetStatusTextSignal,
              [this](std::size_t increment, QString text) {
                emit incrementProgressBarCurrentValueAndSetStatusTextSignal(
                  increment, text);
              });

      connect(multi_visitor_sp.get(),
              &TicChromTreeNodeCombinerVisitor::setStatusTextSignal,
              [this](QString text) { emit setStatusTextSignal(text); });

      connect(
        multi_visitor_sp.get(),
        &TicChromTreeNodeCombinerVisitor::setStatusTextAndCurrentValueSignal,
        [this](QString text, std::size_t value) {
          emit setStatusTextAndCurrentValueSignal(text, value);
        });

      visitors.push_back(multi_visitor_sp);
    }

  // Now perform a parallel iteration in the various visitors and ask them to
  // work on the matching ms run data set iterators pair.
   omp_set_num_threads(visitors.size());
  #pragma omp parallel for ordered
  for(std::size_t iter = 0; iter < visitors.size(); ++iter)
    {
      auto multi_visitor_sp = visitors.at(iter);

      // qDebug() << "Multi-visitor:" << multi_visitor_sp.get()
      //<< "at index:" << iter
      //<< "Executing from thread:" << QThread::currentThreadId();

      mcsp_msRunDataSet->msp_msRunDataSetTree->accept(
        *(multi_visitor_sp.get()),
        iterators.at(iter).first,
        iterators.at(iter).second);

      //qDebug() << "Finished running the visitor at index:" << iter
               //<< "and the m_maxMz for the data stats is:"
               //<< dynamic_cast<const MsRunStatisticsTreeNodeVisitor *>(
                    //multi_visitor_sp->m_visitors.back().get())
                    //->getMsRunDataSetStats()
                    //.m_maxMz;
    }
  // End of
  // #pragma omp parallel for

  // In the loop above, the m_ticChromMapTrace object (map <double, double>,
  // that is, (rt, tic)) has been filled with the various TIC values for the
  // different retention times.
  //
  // At this point we need to combine all the visitor-contained map traces
  // into a single trace. We make the combination into the member MapTrace
  // object. We choose the Trace combiner because we do not have to do any
  // binning. On the contrary we want absolutely all the RT values encountered
  // throughout all of the spectra.

  // qDebug() << "End of the iteration in the visitors";

  // If the task was cancelled, the monitor widget was locked. We need to
  // unlock it.
  emit unlockTaskMonitorCompositeWidgetSignal();
  emit setupProgressBarSignal(0, visitors.size() - 1);

  // We might be here because the user cancelled the operation in the for loop
  // above (visiting all the visitors). In this case m_isOperationCancelled is
  // true. We want to set it back to false, so that the following loop is gone
  // through. The user can ask that the operation be cancelled once more. But we
  // want that at least the performed work be used to show the trace.

  m_isOperationCancelled = false;

  pappso::TracePlusCombiner trace_plus_combiner;
  MsRunDataSetStats ms_run_data_set_stats_merge;

  for(std::size_t iter = 0; iter < visitors.size(); ++iter)
    {
      if(m_isOperationCancelled)
        break;

      auto &&multi_visitor_sp = visitors.at(iter);

      // We know that the first visitor is the visitor for the TIC chromatogram.
      // So, handle this one first.

      TicChromTreeNodeCombinerVisitorSPtr tic_visitor_sp =
        std::dynamic_pointer_cast<TicChromTreeNodeCombinerVisitor>(
          multi_visitor_sp->m_visitors.front());

      const pappso::MapTrace &map_trace = tic_visitor_sp->getTicChromMapTrace();

      // Use the trace plus combiner instance to combine the iterated trace into
      // the member map trace that the integrator users will query after that
      // that calculation is finished.
      trace_plus_combiner.combine(m_mapTrace, map_trace.toTrace());

      // Now we know that the second visitor is for the statistics. Handle this
      // now.

      MsRunStatisticsTreeNodeVisitorSPtr stats_visitor_sp =
        std::dynamic_pointer_cast<MsRunStatisticsTreeNodeVisitor>(
          multi_visitor_sp->m_visitors.back());

      // qDebug() << "In the consolidating loop, iter:" << iter
      //<< "with a map trace of this size : " << map_trace.size();

      emit setStatusTextSignal(
        QString("Consolidating TIC chromatograms and statistics from thread %1")
          .arg(iter + 1));

      emit setProgressBarCurrentValueSignal(iter + 1);

      //qDebug() << "Going to merge a visitor's MsRunDataSetStats and the "
                  //"m_maxMz for the data stats is:"
               //<< dynamic_cast<const MsRunStatisticsTreeNodeVisitor *>(
                    //multi_visitor_sp->m_visitors.back().get())
                    //->getMsRunDataSetStats()
                    //.m_maxMz;

      //qDebug() << "We are merging one visitor's ms run data set stats:"
               //<< stats_visitor_sp.get()->getMsRunDataSetStats().toString();

      //qDebug()
        //<< "Before the merge, the ms_run_data_set_stats_merge has m_maxMz:"
        //<< ms_run_data_set_stats_merge.m_maxMz;

      ms_run_data_set_stats_merge.merge(
        stats_visitor_sp.get()->getMsRunDataSetStats());

      //qDebug()
        //<< "After the merge, the ms_run_data_set_stats_merge has m_maxMz:"
        //<< ms_run_data_set_stats_merge.m_maxMz;
    }

  // At this point we finally have the statistical data merged into a single
  // instance. We now need to consolidate.

  //qDebug() << "Final consolidation of the statistics.";

  ms_run_data_set_stats_merge.consolidate();

  // At this point we can set the statistics to the ms run data set. We need to
  // un-const_cast the shared pointer (this is one of the rarest situations
  // where we need to do this).

  // qDebug() << "Setting the calculated statistics to the ms run data set.";

  std::const_pointer_cast<MsRunDataSet>(mcsp_msRunDataSet)
    ->setMsRunDataSetStats(ms_run_data_set_stats_merge);

  // qDebug().noquote() << mcsp_msRunDataSet->getMsRunDataSetStats().toString();

  std::chrono::system_clock::time_point chrono_end_time =
    std::chrono::system_clock::now();

  QString chrono_string = pappso::Utils::chronoIntervalDebugString(
    "Integration to initial TIC chromatogram and calculation of the ms run "
    "data set statistics took:",
    chrono_start_time,
    chrono_end_time);

  emit logTextToConsoleSignal(chrono_string);
  // qDebug().noquote() << chrono_string;

  emit finishedIntegratingToInitialTicChromatogramSignal(this);
}


bool
MsRunDataSetTreeMassDataIntegratorToRt::integrateToRt()
{
  // qDebug();

  std::chrono::system_clock::time_point chrono_start_time =
    std::chrono::system_clock::now();

  // We need to clear the map trace!
  m_mapTrace.clear();

  // We need a processing flow to work, and, in particular, a processing step
  // from which to find the specifics of the calculation. The processing flow
  // must have been set by the caller either at construction time or later
  // before using the integrator, that is, calling this function.

  if(!m_processingFlow.size())
    qFatal("The processing flow cannot be empty. Program aborted.");

  // Get the most recent step that holds all the specifics of this integration.
  const ProcessingStep *processing_step_p = m_processingFlow.mostRecentStep();

  if(!processing_step_p->srcMatches("ANY_RT"))
    qFatal(
      "There should be one source ProcessingType = ANY_RT. Program aborted.");

  // Try to limit the range of MS run data set tree nodes to be iterated through
  // by looking from what node to what other node we need to go to ensure that
  // our integration encompasses the right RT range.

  std::vector<pappso::MsRunDataSetTreeNode *> root_nodes =
    mcsp_msRunDataSet->getMsRunDataSetTreeCstSPtr()->getRootNodes();

  std::pair<double, double> range_pair;

  bool integration_rt = m_processingFlow.innermostRange("ANY_RT", range_pair);

  using Iterator = std::vector<pappso::MsRunDataSetTreeNode *>::const_iterator;
  using Pair     = std::pair<Iterator, Iterator>;

  Pair pair;

  Iterator begin_iterator = root_nodes.begin();
  Iterator end_iterator   = root_nodes.end();

  std::size_t node_count = 0;

  if(integration_rt)
    {
      pair = mcsp_msRunDataSet->treeNodeIteratorRangeForRtRange(
        range_pair.first, range_pair.second);

      begin_iterator = pair.first;
      end_iterator   = pair.second;

      node_count = std::distance(begin_iterator, end_iterator);

      // qDebug() << "node_count:" << node_count;
    }
  // else
  // qDebug() << "Not integration_rt";

  if(begin_iterator == root_nodes.end())
    {
      qDebug() << "There is nothing to integrate.";
      return false;
    }

  // Now that we know the non-0 count of nodes to be processed:
  emit setProgressBarMaxValueSignal(node_count);

  // At this point, allocate a visitor that is specific for the calculation of
  // the TIC chromatogram.

  // But we want to parallelize the computation.

  // In the pair below, first is the ideal number of threads and second is the
  // number of nodes per thread.
  std::pair<std::size_t, std::size_t> best_parallel_params =
    bestParallelIntegrationParams(node_count);

  // qDebug() << "ideal_thread_count:" << best_parallel_params.first
  //<< "nodes_per_thread:" << best_parallel_params.second;

  using Iterator = std::vector<pappso::MsRunDataSetTreeNode *>::const_iterator;

  std::vector<TicChromTreeNodeCombinerVisitorSPtr> visitors;
  std::vector<std::pair<Iterator, Iterator>> iterators =
    calculateMsRunDataSetTreeNodeIteratorPairs(begin_iterator,
                                               end_iterator,
                                               best_parallel_params.first,
                                               best_parallel_params.second);

  for(std::size_t iter = 0; iter < iterators.size(); ++iter)
    {
      // qDebug() << "thread index:" << iter;

      TicChromTreeNodeCombinerVisitorSPtr visitor_sp =
        std::make_shared<TicChromTreeNodeCombinerVisitor>(mcsp_msRunDataSet,
                                                          m_processingFlow);

      // We want to be able to intercept any cancellation of the operation.

      connect(this,
              &MsRunDataSetTreeMassDataIntegratorToRt::cancelOperationSignal,
              [visitor_sp]() { visitor_sp->cancelOperation(); });
      // visitor_sp.get(),
      //&TicChromTreeNodeCombinerVisitor::cancelOperation,
      // Qt::QueuedConnection);

      // The visitor gets the number of nodes to process from the data set tree
      // node. This signal tells the final user of the signal to set the max
      // value of the progress bar to number_of_nodes_to_process.

      connect(visitor_sp.get(),
              &TicChromTreeNodeCombinerVisitor::setProgressBarMaxValueSignal,
              [this](std::size_t number_of_nodes_to_process) {
                emit setProgressBarMaxValueSignal(number_of_nodes_to_process);
              });

      // The visitor emits the signal to tell that the currently iterated node
      // has number_of_nodes_to_process children to process. Because the visitor
      // might operate in a thread and that there might be other threads
      // handling other nodes, we do not consider that the
      // number_of_nodes_to_process value is the total number of nodes to
      // process but only the number of nodes that the visitor is handling. We
      // thus update the max progress bar value by number_of_nodes_to_process.
      // In a typical setting, the user of the signal is the monitoring object,
      // be it a non-gui object or the TaskMonitorCompositeWidget.
      connect(
        visitor_sp.get(),
        &TicChromTreeNodeCombinerVisitor::incrementProgressBarMaxValueSignal,
        [this](std::size_t number_of_nodes_to_process) {
          emit incrementProgressBarMaxValueSignal(number_of_nodes_to_process);
        });

      connect(
        visitor_sp.get(),
        &TicChromTreeNodeCombinerVisitor::setProgressBarCurrentValueSignal,
        [this](std::size_t number_of_processed_nodes) {
          emit setProgressBarCurrentValueSignal(number_of_processed_nodes);
        });

      connect(visitor_sp.get(),
              &TicChromTreeNodeCombinerVisitor::
                incrementProgressBarCurrentValueAndSetStatusTextSignal,
              [this](std::size_t increment, QString text) {
                emit incrementProgressBarCurrentValueAndSetStatusTextSignal(
                  increment, text);
              });

      connect(visitor_sp.get(),
              &TicChromTreeNodeCombinerVisitor::setStatusTextSignal,
              [this](QString text) { emit setStatusTextSignal(text); });

      connect(
        visitor_sp.get(),
        &TicChromTreeNodeCombinerVisitor::setStatusTextAndCurrentValueSignal,
        [this](QString text, std::size_t value) {
          emit setStatusTextAndCurrentValueSignal(text, value);
        });

      visitors.push_back(visitor_sp);
    }

  // Now perform a parallel iteration in the various visitors and ask them to
  // work on the matching ms run data set iterators pair.
  omp_set_num_threads(visitors.size());
#pragma omp parallel for ordered
  for(std::size_t iter = 0; iter < visitors.size(); ++iter)
    {
      auto visitor_sp = visitors.at(iter);

      // qDebug() << "Visitor:" << visitor_sp.get() << "at index:" << iter
      //<< "Executing from thread:" << QThread::currentThreadId();

      mcsp_msRunDataSet->msp_msRunDataSetTree->accept(
        *(visitor_sp.get()),
        iterators.at(iter).first,
        iterators.at(iter).second);
    }
  // End of
  // #pragma omp parallel for

  // In the loop above, the m_ticChromMapTrace object (map <double, double>,
  // that is, (rt, tic)) has been filled with the various TIC values for the
  // different retention times.
  //
  // At this point we need to combine all the visitor-contained map traces
  // into a single trace. We make the combination into the member MapTrace
  // object.

  // qDebug() << "End of the iteration in the visitors";

  pappso::TracePlusCombiner trace_plus_combiner;

  // If the task was cancelled, the monitor widget was locked. We need to
  // unlock it.
  emit unlockTaskMonitorCompositeWidgetSignal();
  emit setupProgressBarSignal(0, visitors.size() - 1);

  // We might be here because the user cancelled the operation in the for loop
  // above (visiting all the visitors). In this case m_isOperationCancelled is
  // true. We want to set it back to false, so that the following loop is gone
  // through. The user can ask that the operation be cancelled once more. But we
  // want that at least the performed work be used to show the trace.

  m_isOperationCancelled = false;

  for(std::size_t iter = 0; iter < visitors.size(); ++iter)
    {
      if(m_isOperationCancelled)
        break;

      auto &&visitor_sp = visitors.at(iter);

      const pappso::MapTrace &map_trace = visitor_sp->getTicChromMapTrace();

      // qDebug() << "In the consolidating loop, iter:" << iter
      //<< "with a map trace of this size : " << map_trace.size();

      emit setStatusTextSignal(
        QString("Consolidating TIC chromatograms from thread %1")
          .arg(iter + 1));

      emit setProgressBarCurrentValueSignal(iter + 1);

      trace_plus_combiner.combine(m_mapTrace, map_trace.toTrace());
    }

  std::chrono::system_clock::time_point chrono_end_time =
    std::chrono::system_clock::now();

  QString chrono_string = pappso::Utils::chronoIntervalDebugString(
    "Integration to TIC/XIC chromatogram took:",
    chrono_start_time,
    chrono_end_time);

  emit logTextToConsoleSignal(chrono_string);
  // qDebug().noquote() << chrono_string;

  return true;
}


} // namespace minexpert

} // namespace msxps
