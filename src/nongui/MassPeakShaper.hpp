/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// StdLib includes


/////////////////////// Qt includes


/////////////////////// pappsomspp includes
#include <pappsomspp/trace/datapoint.h>
#include <pappsomspp/trace/trace.h>


/////////////////////// Local includes
#include "MassPeakShaperConfig.hpp"


namespace msxps
{

namespace minexpert
{

// The MassPeakShaper class contains all required data to compute a gaussian or
// lorentzian shape corresponding to a given centroid (x,y) with z=m/z and
// y=relative intensity. The data points that make the shape are stored as a
// Trace.

class MassPeakShaper
{
  public:
  MassPeakShaper();
  MassPeakShaper(double mz, double i, const MassPeakShaperConfig &config);
  MassPeakShaper(const pappso::DataPoint &peakCentroid,
                 const MassPeakShaperConfig &config);
  MassPeakShaper(const MassPeakShaper &);
  virtual ~MassPeakShaper();

  void setPeakCentroid(const pappso::DataPoint &data_point);
  const pappso::DataPoint &getPeakCentroid() const;

  void setConfig(const MassPeakShaperConfig &config);
  const MassPeakShaperConfig &getConfig() const;

  const pappso::Trace &getTrace() const;

  bool computeRelativeIntensity(double sum_probabilities);
  void setRelativeIntensity(double rel_int);
  double getRelativeIntensity() const;

  int computePeakShape();
  int computeGaussianPeakShape();
  int computeLorentzianPeakShape();

  double intensityAt(double mz, pappso::PrecisionPtr precision_p, bool &ok);

  QString traceAsText();
  bool traceToFile(const QString &file_name);

  private:
  pappso::DataPoint m_peakCentroid;
  MassPeakShaperConfig m_config;
  double m_relativeIntensity = std::numeric_limits<double>::infinity();
  pappso::Trace m_trace;

  void clearTrace();
};

} // namespace minexpert

} // namespace msxps
