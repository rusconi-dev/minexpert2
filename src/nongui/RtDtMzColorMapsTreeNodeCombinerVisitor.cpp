/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2019 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QDebug>
#include <QThread>


/////////////////////// pappsomspp includes
#include "pappsomspp/processing/combiners/tracepluscombiner.h"


/////////////////////// Local includes
#include "RtDtMzColorMapsTreeNodeCombinerVisitor.hpp"


namespace msxps
{
namespace minexpert
{


RtDtMzColorMapsTreeNodeCombinerVisitor::RtDtMzColorMapsTreeNodeCombinerVisitor(
  MsRunDataSetCstSPtr ms_run_data_set_csp,
  const ProcessingFlow &processing_flow,
  pappso::DataKind data_kind)
  : BaseMsRunDataSetTreeNodeVisitor(ms_run_data_set_csp, processing_flow),
    m_dataKind(data_kind)
{
  //  qDebug() << "Processing flow has" << m_processingFlow.size() << "steps";

  if(data_kind != pappso::DataKind::rt && data_kind != pappso::DataKind::dt)
    qFatal("Programming error.");

  // When creating color map plots, the integrations to mass spectra are
  // performed using 0 decimal places, otherwise the colormaps would be
  // enormous.
  m_combiner.setDecimalPlaces(0);
}


RtDtMzColorMapsTreeNodeCombinerVisitor::RtDtMzColorMapsTreeNodeCombinerVisitor(
  const RtDtMzColorMapsTreeNodeCombinerVisitor &other)
  : BaseMsRunDataSetTreeNodeVisitor(other),
    m_dataKind(other.m_dataKind),
    m_combiner(other.m_combiner),
    m_doubleMapTraceMap(other.m_doubleMapTraceMap)
{
}


RtDtMzColorMapsTreeNodeCombinerVisitor &
RtDtMzColorMapsTreeNodeCombinerVisitor::operator=(
  const RtDtMzColorMapsTreeNodeCombinerVisitor &other)
{
  if(this == &other)
    return *this;

  BaseMsRunDataSetTreeNodeVisitor::operator=(other);

  m_dataKind          = other.m_dataKind;
  m_doubleMapTraceMap = other.m_doubleMapTraceMap;

  m_combiner = other.m_combiner;

  return *this;
}


RtDtMzColorMapsTreeNodeCombinerVisitor::
  ~RtDtMzColorMapsTreeNodeCombinerVisitor()
{
}


void
RtDtMzColorMapsTreeNodeCombinerVisitor::setProcessingFlow(
  const ProcessingFlow &processing_flow)
{
  m_processingFlow = processing_flow;
}


const std::map<double, pappso::MapTrace> &
RtDtMzColorMapsTreeNodeCombinerVisitor::getDoubleMapTraceMap() const
{
  return m_doubleMapTraceMap;
}


pappso::DataKind
RtDtMzColorMapsTreeNodeCombinerVisitor::getDataKind() const
{
  return m_dataKind;
}


bool
RtDtMzColorMapsTreeNodeCombinerVisitor::visit(
  const pappso::MsRunDataSetTreeNode &node)
{
  // qDebug() << "Visiting node:" << node.toString();

  // Sanity check
  if(m_dataKind != pappso::DataKind::rt && m_dataKind != pappso::DataKind::dt)
    qFatal("Programming error.");

  // qDebug() << "Visiting node for mass data kind:" << (int)m_dataKind;

  if(!m_isProgressFeedbackSilenced)
    {
      // Whatever the status of this node (to be or not processed) let the user
      // know that we have gone through one more.

      // The "%c" format refers to the current value in the task monitor widget
      // that will craft the new text according to the current value after
      // having incremented it in the call below. This is necessary because this
      // visitor might be in a thread and the  we need to account for all the
      // thread when giving feedback to the user.

      emit incrementProgressBarCurrentValueAndSetStatusTextSignal(
        1, "Processed %c nodes");
    }

  // This visit() function handles the complex data acquired during IM-MS
  // experiments. In this kind of experiment, a single scan start time (that is
  // the retention time value) can be associated with many mass spectra, because
  // each mobility mass spectrum (on Synapt instruments, 200 mobility spectra
  // are acquired) is acquired for that rt value.

  // This function can be used to handle rt/mass spectra or dt/mass spectra
  // association depending on the way *this object was constructed.

  // Immediately get a pointer to  the qualified mass spectrum that is enshrined
  // in the node.

  // qDebug() << "Visiting node:" << &node << "text format:" << node.toString()
  //<< "with quaified mass spectrum:"
  //<< node.getQualifiedMassSpectrum()->toString();

  pappso::QualifiedMassSpectrumCstSPtr qualified_mass_spectrum_csp =
    node.getQualifiedMassSpectrum();

  // A processing step might have a valid fragmentation specification. We need
  // to test that immediately before delving into the steps that make this flow.

  if(m_processingFlow.getDefaultMsFragmentationSpec().getMsLevel())
    {
      // qDebug() << "Node:" << &node
      //<< "The default ms fragmentation spec has ms level:"
      //<< m_processingFlow.getDefaultMsFragmentationSpec()
      //.getGreatestMsLevel();

      if(qualified_mass_spectrum_csp->getMsLevel() >=
         m_processingFlow.getDefaultMsFragmentationSpec().getMsLevel())
        {
          // qDebug() << "Node:" << &node << "The default frag spec ms level:"
          //<< m_processingFlow.getDefaultMsFragmentationSpec()
          //.getGreatestMsLevel()
          //<< "does NOT match the spectrum ms level:"
          //<< qualified_mass_spectrum_csp->getMsLevel();

          // The default ms fragmentation step's ms level in the processing flow
          // does not match the ms level of the current mass spectrum. Return
          // false immediately.

          return false;
        }
      else
        {
          // qDebug() << "Node:" << &node << "The default frag spec ms level:"
          //<< m_processingFlow.getDefaultMsFragmentationSpec()
          //.getGreatestMsLevel()
          //<< "matches YES the spectrum ms level:"
          //<< qualified_mass_spectrum_csp->getMsLevel();
        }
    }
  else
    {
      // qDebug() << "The processing flow's default ms frag spec is not valid.";
    }

  // At this point we need to go deep in the processing flow's steps to check if
  // the node matches the whole set of the steps' specs.

  for(auto &&step : m_processingFlow)
    {
      if(checkProcessingStep(*step, node))
        {
          // qDebug() << "The node matches the iterated step.";
        }
      else
        {
          // qDebug() << "The node does not match the iterated step.";

          return false;
        }
    }

  if(qualified_mass_spectrum_csp == nullptr)
    qFatal("Cannot be nullptr");
  if(qualified_mass_spectrum_csp.get() == nullptr)
    qFatal("Cannot be nullptr");

  if(qualified_mass_spectrum_csp->getMassSpectrumSPtr() == nullptr)
    {
      // qDebug() << "Need to access the mass data right from the file.";

      std::size_t mass_spectrum_index =
        mcsp_msRunDataSet->getMsRunDataSetTreeCstSPtr()->massSpectrumIndex(
          &node);

      pappso::MsRunReaderCstSPtr ms_run_reader_csp =
        mcsp_msRunDataSet->getMsRunReaderCstSPtr();

      qualified_mass_spectrum_csp =
        std::make_shared<pappso::QualifiedMassSpectrum>(
          ms_run_reader_csp->qualifiedMassSpectrum(mass_spectrum_index, true));
    }

  if(qualified_mass_spectrum_csp == nullptr)
    {
      qFatal("Failed to read the mass spectral data from the file.");
    }

  // if(!qualified_mass_spectrum_csp->size())
  // qDebug() << "The mass spectrum is empty.";

  // qDebug() << "The qualified mass spectrum:"
  //<< qualified_mass_spectrum_csp->toString()
  //<< "rt in minutes:" << qualified_mass_spectrum_csp->getRtInMinutes();

  // At this point, we need to cope with the data. What we want is to combine
  // into a single mass spectrum all the MS1 mass spectra that have been
  // acquired for any given retention time (on Synapt machines, that would be
  // 200 mobility spectra for each given rt value, year 2018).

  double rt_or_dt_key; // that is, either rt or dt.

  if(m_dataKind == pappso::DataKind::rt)
    {
      rt_or_dt_key = qualified_mass_spectrum_csp->getRtInMinutes();
    }
  else if(m_dataKind == pappso::DataKind::dt)
    {
      rt_or_dt_key = qualified_mass_spectrum_csp->getDtInMilliSeconds();
    }
  else
    qFatal("Programming error.");

  // qDebug() << "The value dt|rt:" << rt_or_dt_key;

  // When doing IM-MS experiments, the drift time of the ions introduces a new
  // dimension in the mass data when compared with conventional mass
  // spectrometry. In this function we try to cope with this extra dimension.

  std::map<double, pappso::MapTrace>::iterator double_map_trace_map_iterator;
  double_map_trace_map_iterator = m_doubleMapTraceMap.find(rt_or_dt_key);

  // Tells if the key was already found in the map.
  if(double_map_trace_map_iterator != m_doubleMapTraceMap.end())
    {

      // qDebug() << "A node for a mass spectrum having that same rt_or_dt_key "
      //"was found already:"
      //<< rt_or_dt_key;

      // The map already contained a pair that had the key value ==
      // rt_or_dt_key. All we have to do is combine the new mass spectrum
      // pointed to by the map value into the MapTrace that is pointed to by the
      // map key.

      // Combine the new mass spectrum right into the found MapTrace.

      // qDebug() << "Before combination, map trace map size: "
      //<< double_map_trace_map_iterator->second.size();

      m_combiner.combine(
        double_map_trace_map_iterator->second,
        *(qualified_mass_spectrum_csp->getMassSpectrumCstSPtr()));

      // qDebug() << "After combination, map trace map size: "
      //<< double_map_trace_map_iterator->second.size();
    }
  else
    {

      // No other mass spectrum was encountered by the rt_or_dt_key key. So we
      // need to do all the process here.

      // qDebug() << "No node for a mass spectrum having that same rt_or_dt_key
      // " "was found already:"
      //<< rt_or_dt_key;

      // Instantiate a new MapTrace in which we'll combine this and all the
      // future mass spectra having been acquired at this same dt value.

      pappso::MapTrace map_trace;

      m_combiner.combine(
        map_trace, *(qualified_mass_spectrum_csp->getMassSpectrumCstSPtr()));

      // Finally, store in the map, the map_trace along with its rt_or_dt_key.

      m_doubleMapTraceMap[rt_or_dt_key] = map_trace;

      // qDebug() << "After combination, map trace map size: " <<
      // map_trace.size();
    }

  return true;
}


bool
RtDtMzColorMapsTreeNodeCombinerVisitor::checkProcessingStep(
  const ProcessingStep &step, const pappso::MsRunDataSetTreeNode &node)
{
  //  qDebug();

  // A processing step is a collection of mapped items:
  // std::map<ProcessingType, ProcessingSpec> m_processingTypeSpecMap;
  //
  // For each item, we need to check if the node matches the spec.

  for(auto &&pair : step.getProcessingTypeSpecMap())
    {
      if(checkProcessingSpec(pair, node))
        {
          //          qDebug() << "The node matches the iterated spec.";
        }
      else
        {
          //          qDebug() << "The node does not match the iterated
          //          spec.";

          return false;
        }
    }

  return true;
}


bool
RtDtMzColorMapsTreeNodeCombinerVisitor::checkProcessingSpec(
  const std::pair<ProcessingType, ProcessingSpec *> &type_spec_pair,
  const pappso::MsRunDataSetTreeNode &node)
{
  //  qDebug();

  // This is the elemental component of a ProcessingFlow, so here we actually
  // look into the node.

  const ProcessingSpec *spec_p = type_spec_pair.second;

  pappso::QualifiedMassSpectrumCstSPtr qualified_mass_spectrum_csp =
    node.getQualifiedMassSpectrum();

  const MsFragmentationSpec fragmentation_spec =
    spec_p->getMsFragmentationSpec();

  if(fragmentation_spec.getMsLevel())
    {
      // qDebug() << "The fragmentation spec *is* valid.";

      // For each member of the MsFragmentationSpec structure, check if it is to
      // be used for the check.


      // Logically, if the currently handled mass spectrum has a ms level
      // that is greater than the fragmentation spec ms level, then that
      // means that it should be visited: in the process of making data
      // mining, one goes from the lowest ms level to higher ms levels, so
      // we need to keep the mass spectrum. At some point down the
      // processing datetime, a process spec will limit the mass spectra
      // that are actually validated.

      if(qualified_mass_spectrum_csp->getMsLevel() >=
         fragmentation_spec.getMsLevel())
        {
          // qDebug() << "Node:" << &node
          //<< "The ms level matches, with fragspec level:"
          //<< fragmentation_spec.msLevelsToString()
          //<< "and the node spectrum level:"
          //<< qualified_mass_spectrum_csp->getMsLevel();
        }
      else
        {
          // qDebug() << "Node:" << &node
          //<< "The ms level does not match, with fragspec level:"
          //<< fragmentation_spec.msLevelsToString()
          //<< "and the node spectrum level:"
          //<< qualified_mass_spectrum_csp->getMsLevel();

          return false;
        }

      if(fragmentation_spec.precursorMzValuesCount())
        {
          const std::vector<pappso::PrecursorIonData>
            &precursor_ion_data_vector =
              qualified_mass_spectrum_csp->getPrecursorIonData();

          if(!fragmentation_spec.containsMzPrecursors(
               precursor_ion_data_vector))
            {
              return false;
            }
        }

      if(fragmentation_spec.precursorSpectrumIndicesCount())
        {
          if(!fragmentation_spec.containsSpectrumPrecursorIndex(
               qualified_mass_spectrum_csp->getPrecursorSpectrumIndex()))
            {
              // qDebug() << "The precursor spectrum index does not match.";

              return false;
            }
        }
    }
  else
    {
      // Else, fragmentation is not a criterion for filtering data. So go on.
      // qDebug() << "The fragmentation spec is *not* valid.";
    }

  // qDebug() << "Going on with the spec check.";

  ProcessingType type = type_spec_pair.first;

  if(type.bitMatches("RT_TO_ANY"))
    {
      //      qDebug();
      if(!checkProcessingTypeByRt(type_spec_pair, node))
        return false;
    }
  else if(type.bitMatches("MZ_TO_ANY"))
    {
      //      qDebug();
      if(!checkProcessingTypeByMz(type_spec_pair, node))
        return false;
    }
  else if(type.bitMatches("DT_TO_ANY"))
    {
      //      qDebug();
      if(!checkProcessingTypeByDt(type_spec_pair, node))
        return false;
    }

  // At this point we seem to understand that the node matched!

  return true;
}


bool
RtDtMzColorMapsTreeNodeCombinerVisitor::checkProcessingTypeByRt(
  const std::pair<ProcessingType, ProcessingSpec *> &type_spec_pair,
  const pappso::MsRunDataSetTreeNode &node)
{
  //  qDebug();

  pappso::QualifiedMassSpectrumCstSPtr qualified_mass_spectrum_csp =
    node.getQualifiedMassSpectrum();

  const ProcessingSpec *spec_p = type_spec_pair.second;

  double rt = qualified_mass_spectrum_csp->getRtInMinutes();

  if(rt >= spec_p->getStart() && rt <= spec_p->getEnd())
    {
      //      qDebug() << "Returning true for Rt.";
      return true;
    }

  return false;
}


bool
RtDtMzColorMapsTreeNodeCombinerVisitor::checkProcessingTypeByMz(
  const std::pair<ProcessingType, ProcessingSpec *> &type_spec_pair,
  [[maybe_unused]] const pappso::MsRunDataSetTreeNode &node)
{
  qDebug();

  // Are there specs about the m/z range to be accounted for in the
  // computation?

  const ProcessingSpec *spec_p = type_spec_pair.second;

  if(spec_p->hasValidOneDimensionRange())
    {
      qDebug() << "Needed a m/z range filtering:" << spec_p->toString();

      // The point is that if there are more than one spec in the the flow's
      // steps that limit the m/z range, we want to be sure to perform the
      // combination using the innermost range. So ask the ProcessingFlow to
      // actually compute that innermost m/z range.

      // Make a local copy of the type_spec_pair
      std::pair<ProcessingType, ProcessingSpec *> local_type_spec_pair(
        type_spec_pair);

      if(m_processingFlow.innermostMzRange(local_type_spec_pair))
        {
          m_combiner.setFilterResampleKeepXRange(
            pappso::FilterResampleKeepXRange(
              local_type_spec_pair.second->getStart(),
              local_type_spec_pair.second->getEnd()));
        }
      else
        qFatal(
          "Cannot be that there is no innermost m/z range if there is a valid "
          "m/z range in a spec.");
    }

  return true;
}


bool
RtDtMzColorMapsTreeNodeCombinerVisitor::checkProcessingTypeByDt(
  const std::pair<ProcessingType, ProcessingSpec *> &type_spec_pair,
  const pappso::MsRunDataSetTreeNode &node)
{
  //  qDebug();

  pappso::QualifiedMassSpectrumCstSPtr qualified_mass_spectrum_csp =
    node.getQualifiedMassSpectrum();

  const ProcessingSpec *spec_p = type_spec_pair.second;

  double dt = qualified_mass_spectrum_csp->getDtInMilliSeconds();

  if(dt >= spec_p->getStart() && dt <= spec_p->getEnd())
    return true;

  return false;
}


} // namespace minexpert

} // namespace msxps
