/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// StdLib includes


/////////////////////// Qt includes


/////////////////////// pappsomspp includes
#include <pappsomspp/utils.h>
#include <pappsomspp/massspectrum/massspectrum.h>
#include <pappsomspp/massspectrum/qualifiedmassspectrum.h>


/////////////////////// Local includes
#include "globals.hpp"


namespace msxps
{

namespace minexpert
{

class MsRunDataSetStats;

typedef std::shared_ptr<MsRunDataSetStats> MsRunDataSetStatsSPtr;


class MsRunDataSetStats
{

  friend class MsRunDataSet;

  public:
  int m_spectrumCount = 0;

  double m_smallestSpectrumSize = std::numeric_limits<double>::max();
  double m_greatestSpectrumSize = 0;
  double m_spectrumSizeAvg      = 0;
  double m_spectrumSizeStdDev   = 0;

  // Smallest m/z value found in *all* the spectra (that is the minimum m/z
  // value encountered in the whole spectral data set).
  double m_minMz = std::numeric_limits<double>::max();

  // First mz value encountered in any given single mass spectrum
  double m_firstMzAvg    = 0;
  double m_firstMzStdDev = 0;

  // Biggest m/z value found in *all* the spectra (that is the maximum m/z
  // value encountered in the whole spectral data set).
  double m_maxMz = 0;

  // Last mz value encountered in any given single mass spectrum
  double m_lastMzAvg    = 0;
  double m_lastMzStdDev = 0;

  double m_smallestMzShift = std::numeric_limits<double>::max();
  double m_greatestMzShift = 0;
  double m_mzShiftAvg      = 0;
  double m_mzShiftStdDev   = 0;

  double m_nonZeroSmallestStep = std::numeric_limits<double>::max();
  double m_smallestStep        = std::numeric_limits<double>::max();
  double m_smallestStepMedian  = 0;
  double m_smallestStepAvg     = 0;
  double m_smallestStepStdDev  = 0;

  double m_greatestStep       = 0;
  double m_greatestStepAvg    = 0;
  double m_greatestStepStdDev = 0;

  // All the lists used to craft the statistics.
  std::vector<double> m_spectrumSizes;

  std::vector<double> m_firstMzs;
  std::vector<double> m_lastMzs;

  std::vector<double> m_mzShifts;

  std::vector<double> m_smallestSteps;
  std::vector<double> m_greatestSteps;
  std::vector<double> m_avgSteps;
  std::vector<double> m_stdDevSteps;


  // Helper references to mass spec data.
  MsRunDataSetStats *mp_msRunDataSet;
  std::vector<pappso::QualifiedMassSpectrumCstSPtr> *mp_massSpectra;

  // Construction / Destruction

  MsRunDataSetStats();
  MsRunDataSetStats(const MsRunDataSetStats &other);
  virtual ~MsRunDataSetStats();

  MsRunDataSetStats &operator=(const MsRunDataSetStats &other);

  void reset();

  int spectrumBins(const pappso::MassSpectrum &mass_spectrum,
                   std::vector<double> &bins);

  void incrementStatistics(const pappso::MassSpectrum &mass_spectrum);
  void incrementStatistics(const pappso::MassSpectrumCstSPtr mass_spectrum_csp);

  void merge(const MsRunDataSetStats &other);
  void consolidate();

  bool isValid();

  QString toString() const;
};


} // namespace minexpert
} // namespace msxps
