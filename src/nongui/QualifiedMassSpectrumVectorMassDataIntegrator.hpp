/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// StdLib includes
#include <vector>


/////////////////////// Qt includes


/////////////////////// pappsomspp includes
#include <pappsomspp/massspectrum/qualifiedmassspectrum.h>
#include <pappsomspp/processing/combiners/massspectrumpluscombiner.h>


/////////////////////// Local includes
#include "globals.hpp"
#include "MsRunDataSet.hpp"
#include "ProcessingFlow.hpp"


namespace msxps
{
namespace minexpert
{

class QualifiedMassSpectrumVectorMassDataIntegrator;

typedef std::shared_ptr<QualifiedMassSpectrumVectorMassDataIntegrator>
  QualifiedMassSpectrumVectorMassDataIntegratorSPtr;

typedef std::vector<pappso::QualifiedMassSpectrumCstSPtr>
  QualifiedMassSpectraVector;

typedef std::shared_ptr<QualifiedMassSpectraVector>
  QualifiedMassSpectraVectorSPtr;

typedef std::shared_ptr<const QualifiedMassSpectraVector>
  QualifiedMassSpectraVectorCstSPtr;

class QualifiedMassSpectrumVectorMassDataIntegrator : public QObject
{
  Q_OBJECT

  public:
  QualifiedMassSpectrumVectorMassDataIntegrator();

  QualifiedMassSpectrumVectorMassDataIntegrator(
    MsRunDataSetCstSPtr ms_run_data_set_csp,
    const ProcessingFlow &processing_flow,
    QualifiedMassSpectraVectorSPtr &qualified_mass_spectra_to_integrate_sp);

  QualifiedMassSpectrumVectorMassDataIntegrator(
    const QualifiedMassSpectrumVectorMassDataIntegrator &other);

  virtual ~QualifiedMassSpectrumVectorMassDataIntegrator();

  MsRunDataSetCstSPtr getMsRunDataSet() const;

  void setProcessingFlow(const ProcessingFlow &processing_flow);
  const ProcessingFlow &getProcessingFlow() const;

  virtual pappso::QualifiedMassSpectrumCstSPtr checkQualifiedMassSpectrum(
    pappso::QualifiedMassSpectrumCstSPtr &qualified_mass_spectrum_csp);

  const pappso::MapTrace &getMapTrace() const;

  void setMaxThreadUseCount(std::size_t count);
  std::size_t getMaxThreadUseCount();

  public slots:

  void cancelOperation();


  signals:
  void cancelOperationSignal();

  void setTaskDescriptionTextSignal(QString text);

  void setupProgressBarSignal(std::size_t min_value, std::size_t max_value);
  void setProgressBarMinValueSignal(std::size_t value);
  void setProgressBarMaxValueSignal(std::size_t value);
  void incrementProgressBarMaxValueSignal(std::size_t increment);
  void setProgressBarCurrentValueSignal(std::size_t number_of_processed_nodes);
  void incrementProgressBarCurrentValueSignal(std::size_t increment);

  void setStatusTextSignal(QString text);
  void appendStatusTextSignal(QString text);
  void setStatusTextAndCurrentValueSignal(QString text, int value);

  void
  incrementProgressBarCurrentValueAndSetStatusTextSignal(std::size_t increment,
                                                         const QString &text);

  void logTextToConsoleSignal(QString text);
  void logColoredTextToConsoleSignal(QString text, QColor color);

  void lockTaskMonitorCompositeWidgetSignal();
  void unlockTaskMonitorCompositeWidgetSignal();

  protected:
  MsRunDataSetCstSPtr mcsp_msRunDataSet = nullptr;

  ProcessingFlow m_processingFlow;

  std::size_t m_maxThreadUseCount = 0;

  std::vector<ProcessingStep> m_innermostSteps2D;

  std::vector<pappso::SelectionPolygonSpec> m_selectionPolygonSpecs;

  std::pair<double, double> m_dtRange =
    std::pair<double, double>(std::numeric_limits<double>::quiet_NaN(),
                              std::numeric_limits<double>::quiet_NaN());
  std::pair<double, double> m_mzRange =
    std::pair<double, double>(std::numeric_limits<double>::quiet_NaN(),
                              std::numeric_limits<double>::quiet_NaN());
  std::pair<double, double> m_rtRange =
    std::pair<double, double>(std::numeric_limits<double>::quiet_NaN(),
                              std::numeric_limits<double>::quiet_NaN());

  const QualifiedMassSpectraVectorSPtr
    mcsp_qualifiedMassSpectraToIntegrateVector;

  pappso::MapTrace m_mapTrace;

  bool m_isOperationCancelled = false;

  std::size_t bestThreadCount();

  // In the pair below, first is the ideal number of threads and second is the
  // number of nodes per thread.
  std::pair<std::size_t, std::size_t>
  bestParallelIntegrationParams(std::size_t node_count);

  virtual void setInnermostRanges();

  virtual std::size_t fillInSelectionPolygonSpecs(
    std::vector<pappso::SelectionPolygonSpec> &selection_polygon_specs);

  virtual bool checkMsLevel(
    pappso::QualifiedMassSpectrumCstSPtr &qualified_mass_spectrum_csp);

  virtual bool checkMsFragmentation(
    const ProcessingStep &processing_step,
    pappso::QualifiedMassSpectrumCstSPtr &qualified_mass_spectrum_csp);

  virtual bool checkMsFragmentation(
    pappso::QualifiedMassSpectrumCstSPtr &qualified_mass_spectrum_csp);

  virtual bool checkRtRange(
    pappso::QualifiedMassSpectrumCstSPtr &qualified_mass_spectrum_csp);

  virtual bool checkDtRange(
    pappso::QualifiedMassSpectrumCstSPtr &qualified_mass_spectrum_csp);

  protected:
  // We almost systematically need these m/z range values when making visits:
  // one criterium might be that the m/z value of data points (the x member of
  // DataPoint) be contained within the limit m/z range.
  //
  // Note that we set them to min() and max() because we'll use std::min() and
  // std::max() against these values.

  using Iterator =
    std::vector<pappso::QualifiedMassSpectrumCstSPtr>::const_iterator;

  std::vector<std::pair<Iterator, Iterator>>
  calculateIteratorPairs(std::size_t thread_count, int nodes_per_thread);

  std::vector<std::pair<Iterator, Iterator>>
  calculateIteratorPairs(Iterator begin_iterator,
                         Iterator end_iterator,
                         std::size_t thread_count,
                         int nodes_per_thread);

  bool checkInnermostDtRt2DSteps(
    pappso::QualifiedMassSpectrumCstSPtr &qualified_mass_spectrum_csp);

  private:
};


} // namespace minexpert

} // namespace msxps


Q_DECLARE_METATYPE(
  msxps::minexpert::QualifiedMassSpectrumVectorMassDataIntegrator)
extern int qualifiedMassSpectrumVectorMassDataIntegratorMetaTypeId;

Q_DECLARE_METATYPE(
  msxps::minexpert::QualifiedMassSpectrumVectorMassDataIntegratorSPtr)
extern int qualifiedMassSpectrumVectorMassDataIntegratorSPtrMetaTypeId;

