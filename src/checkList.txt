Make sure that when converting double values to string, the format is correct:

QString("%1").arg(driftTime, 0, 'f', 10)

We do want the precision that is indicated by the '6' above.


