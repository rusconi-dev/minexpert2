/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Std includes
#include <iostream>
#include <iomanip>
#include <string>
#include <algorithm>
#include <limits>
#include <ctime>
#include <chrono>


/////////////////////// Qt includes
#include <QApplication>
#include <QtGlobal>
#include <QStringList>
#include <QVector>
#include <QDebug>
#include <QCommandLineParser>
#include <QTableView>
#include <QClipboard>
#include <QLocale>


/////////////////////// pappso includes

/////////////////////// Local includes
#include "config.h"
#include "gui/Application.hpp"
#include "gui/ProgramWindow.hpp"


namespace msxps
{
namespace minexpert
{

void printGreetings();
void printVersion();
void printConfig(const QString & = QString());

void
printGreetings()
{
  QString greetings = QObject::tr("mineXpert, version %1\n\n").arg(VERSION);

  greetings += "Type 'minexpert --help' for help\n\n";

  greetings +=
    "mineXpert is Copyright 2016-2019 \n"
    "by Filippo Rusconi.\n\n"
    "mineXpert comes with ABSOLUTELY NO WARRANTY.\n"
    "mineXpert is free software, "
    "covered by the GNU General\n"
    "Public License Version 3 or later, "
    "and you are welcome to change it\n"
    "and/or distribute copies of it under "
    "certain conditions.\n"
    "Check the file COPYING in the distribution "
    "and/or the\n"
    "'Help/About(Ctrl+H)' menu item of the program.\n"
    "\nHappy mineXpert'ing!\n\n";

  std::cout << greetings.toStdString();
}


void
printVersion()
{
  QString version = QObject::tr(
                      "mineXpert, version %1 -- "
                      "Compiled against Qt, version %2\n")
                      .arg(VERSION)
                      .arg(QT_VERSION_STR);

  std::cout << version.toStdString();
}


void
printConfig(const QString &execName)
{
  QString config = QObject::tr(
                     "mineXpert: "
                     "Compiled with the following configuration:\n"
                     "EXECUTABLE BINARY FILE: = %1\n"
                     "MSXPERTSUITE_BIN_DIR = %2\n"
                     "MSXPERTSUITE_DATA_DIR = %3\n"
                     "MSXPERTSUITE_DOC_DIR = %5\n")
                     .arg(execName)
                     .arg(BIN_DIR)
                     .arg(DOC_DIR);

  std::cout << config.toStdString();
}

} // namespace minexpert
} // namespace msxps


int
main(int argc, char **argv)
{

  Q_INIT_RESOURCE(minexpert2);

  // Qt stuff starts here.
  msxps::minexpert::Application application(argc, argv, "minexpert2");

  QCommandLineParser parser;
  parser.setApplicationDescription("mineXpert2");
  parser.addHelpOption();
  parser.addVersionOption();

  // Process the actual command line arguments given by the user
  parser.process(application);
  QString file_name;

  msxps::minexpert::ProgramWindow *main_window_p =
    new msxps::minexpert::ProgramWindow(nullptr, "mineXpert2");
  main_window_p->show();

  const QStringList args = parser.positionalArguments();

  if(!args.isEmpty())
    {
      for(auto &&file_name : args)
        {
          // Check if the argument is a directory or a file.

          QFileInfo file_info(file_name);

          if(!file_info.exists() || file_info.isDir())
            main_window_p->openMassSpectrometryFileDlg(file_name, false);
          else
            main_window_p->openMassSpectrometryFile(file_name, false);
        }
    }
  // else
  //{
  // main_window_p->openMassSpectrometryFileDlg(file_name, false);
  //}

  // Now set the default locale to american english.
  QLocale::setDefault(QLocale(QLocale::English, QLocale::UnitedStates));

  return application.exec();
}
// End of
// main(int argc, char **argv)
