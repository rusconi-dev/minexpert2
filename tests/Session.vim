let SessionLoad = 1
if &cp | set nocp | endif
let s:cpo_save=&cpo
set cpo&vim
imap <Nul> <C-Space>
inoremap <expr> <Up> pumvisible() ? "\" : "\<Up>"
inoremap <expr> <S-Tab> pumvisible() ? "\" : "\<S-Tab>"
inoremap <expr> <Down> pumvisible() ? "\" : "\<Down>"
inoremap <silent> <C-ScrollWheelDown> <C-ScrollWheelDown>:call color_coded#moved()<Insert>
inoremap <silent> <C-ScrollWheelUp> <C-ScrollWheelUp>:call color_coded#moved()<Insert>
inoremap <silent> <S-ScrollWheelDown> <S-ScrollWheelDown>:call color_coded#moved()<Insert>
inoremap <silent> <S-ScrollWheelUp> <S-ScrollWheelUp>:call color_coded#moved()<Insert>
inoremap <silent> <ScrollWheelDown> <ScrollWheelDown>:call color_coded#moved()<Insert>
inoremap <silent> <ScrollWheelUp> <ScrollWheelUp>:call color_coded#moved()<Insert>
imap <C-X><C-B> <Plug>(HTMLEntityComplete)
imap <C-X><C-Z> <Plug>(UnicodeComplete)
imap <C-X><C-G> <Plug>(DigraphComplete)
inoremap <silent> <C-Tab> =UltiSnips#ListSnippets()
inoremap <silent> <C-L> =UltiSnips#ExpandSnippet()
imap <M-P> <Plug>yankstack_substitute_newer_paste
imap <M-p> <Plug>yankstack_substitute_older_paste
inoremap <silent> <SNR>67_yrrecord =YRRecord3()
inoremap <silent> <Plug>NERDCommenterInsert  <BS>:call NERDComment('i', 'insert')
imap <C-J> <Plug>IMAP_JumpForward
inoremap <silent> <Plug>IMAP_JumpBack =IMAP_Jumpfunc('b', 0)
inoremap <silent> <Plug>IMAP_JumpForward =IMAP_Jumpfunc('', 0)
snoremap <silent>  "_c
vmap <NL> <Plug>IMAP_JumpForward
nmap <NL> <Plug>IMAP_JumpForward
xnoremap <silent>  :call UltiSnips#SaveLastVisualSelection()gvs
snoremap <silent>  :call UltiSnips#ExpandSnippet()
nnoremap <silent>  :YRReplace '1', p
map  <Plug>(ctrlp)
vnoremap  "hy:%s/h//gc<Left><Left><Left>
nmap d :cs find d =expand("<cword>")	
nmap i :cs find i ^=expand("<cfile>")$
nmap f :cs find f =expand("<cfile>")	
nmap e :cs find e =expand("<cword>")	
nmap t :cs find t =expand("<cword>")	
nmap c :cs find c =expand("<cword>")	
nmap g :cs find g =expand("<cword>")	
nmap s :cs find s =expand("<cword>")	
omap <silent> % <Plug>(MatchitOperationForward)
xmap <silent> % <Plug>(MatchitVisualForward)
nmap <silent> % <Plug>(MatchitNormalForward)
xmap ,e <Plug>CamelCaseMotion_e
xmap ,b <Plug>CamelCaseMotion_b
xmap ,w <Plug>CamelCaseMotion_w
omap ,e <Plug>CamelCaseMotion_e
omap ,b <Plug>CamelCaseMotion_b
omap ,w <Plug>CamelCaseMotion_w
nmap ,e <Plug>CamelCaseMotion_e
nmap ,b <Plug>CamelCaseMotion_b
nmap ,w <Plug>CamelCaseMotion_w
vnoremap // y/"
nnoremap =op <Nop>
vnoremap ?? y?"
nmap @ :YRMapsMacro
imap Ð <Plug>yankstack_substitute_newer_paste
imap ð <Plug>yankstack_substitute_older_paste
imap œihn :IHN
imap œis :IHS:A
imap œih :IHS
inoremap œ<C-U> gUiwi
inoremap œ gUiwi
inoremap œfrups filippo.rusconi@universite-paris-saclay.fr
inoremap œups universite-paris-saclay.fr
inoremap œers Sincerely,Filippo Rusconi
inoremap œrs Sincèrement,Filippo Rusconi
inoremap œfs Sincèrement,Filippo
inoremap œrab À bientôt,Filippo Rusconi
inoremap œfab À bientôt,Filippo
nmap [xx <Plug>unimpaired_line_xml_encode
xmap [x <Plug>unimpaired_xml_encode
nmap [x <Plug>unimpaired_xml_encode
nmap [uu <Plug>unimpaired_line_url_encode
xmap [u <Plug>unimpaired_url_encode
nmap [u <Plug>unimpaired_url_encode
nmap [yy <Plug>unimpaired_line_string_encode
xmap [y <Plug>unimpaired_string_encode
nmap [y <Plug>unimpaired_string_encode
nmap [P <Plug>unimpairedPutAbove
nmap [p <Plug>unimpairedPutAbove
xmap [e <Plug>unimpairedMoveSelectionUp
nmap [e <Plug>unimpairedMoveUp
nmap [  <Plug>unimpairedBlankUp
omap [n <Plug>unimpairedContextPrevious
xmap [n <Plug>unimpairedContextPrevious
nmap [n <Plug>unimpairedContextPrevious
nmap [f <Plug>unimpairedDirectoryPrevious
nmap [<C-T> <Plug>unimpairedTPPrevious
nmap [ <Plug>unimpairedTPPrevious
nmap [T <Plug>unimpairedTFirst
nmap [t <Plug>unimpairedTPrevious
nmap [<C-Q> <Plug>unimpairedQPFile
nmap [ <Plug>unimpairedQPFile
nmap [Q <Plug>unimpairedQFirst
nmap [q <Plug>unimpairedQPrevious
nmap [<C-L> <Plug>unimpairedLPFile
nmap [ <Plug>unimpairedLPFile
nmap [L <Plug>unimpairedLFirst
nmap [l <Plug>unimpairedLPrevious
nmap [B <Plug>unimpairedBFirst
nmap [b <Plug>unimpairedBPrevious
nmap [A <Plug>unimpairedAFirst
nmap [a <Plug>unimpairedAPrevious
omap <silent> [% <Plug>(MatchitOperationMultiBackward)
xmap <silent> [% <Plug>(MatchitVisualMultiBackward)
nmap <silent> [% <Plug>(MatchitNormalMultiBackward)
nmap ]xx <Plug>unimpaired_line_xml_decode
xmap ]x <Plug>unimpaired_xml_decode
nmap ]x <Plug>unimpaired_xml_decode
nmap ]uu <Plug>unimpaired_line_url_decode
xmap ]u <Plug>unimpaired_url_decode
nmap ]u <Plug>unimpaired_url_decode
nmap ]yy <Plug>unimpaired_line_string_decode
xmap ]y <Plug>unimpaired_string_decode
nmap ]y <Plug>unimpaired_string_decode
nmap ]P <Plug>unimpairedPutBelow
nmap ]p <Plug>unimpairedPutBelow
xmap ]e <Plug>unimpairedMoveSelectionDown
nmap ]e <Plug>unimpairedMoveDown
nmap ]  <Plug>unimpairedBlankDown
omap ]n <Plug>unimpairedContextNext
xmap ]n <Plug>unimpairedContextNext
nmap ]n <Plug>unimpairedContextNext
nmap ]f <Plug>unimpairedDirectoryNext
nmap ]<C-T> <Plug>unimpairedTPNext
nmap ] <Plug>unimpairedTPNext
nmap ]T <Plug>unimpairedTLast
nmap ]t <Plug>unimpairedTNext
nmap ]<C-Q> <Plug>unimpairedQNFile
nmap ] <Plug>unimpairedQNFile
nmap ]Q <Plug>unimpairedQLast
nmap ]q <Plug>unimpairedQNext
nmap ]<C-L> <Plug>unimpairedLNFile
nmap ] <Plug>unimpairedLNFile
nmap ]L <Plug>unimpairedLLast
nmap ]l <Plug>unimpairedLNext
nmap ]B <Plug>unimpairedBLast
nmap ]b <Plug>unimpairedBNext
nmap ]A <Plug>unimpairedALast
nmap ]a <Plug>unimpairedANext
omap <silent> ]% <Plug>(MatchitOperationMultiForward)
xmap <silent> ]% <Plug>(MatchitVisualMultiForward)
nmap <silent> ]% <Plug>(MatchitNormalMultiForward)
xmap a% <Plug>(MatchitVisualTextObject)
nmap cr <Plug>(abolish-coerce-word)
vmap gx <Plug>NetrwBrowseXVis
nmap gx <Plug>NetrwBrowseX
omap <silent> g% <Plug>(MatchitOperationBackward)
xmap <silent> g% <Plug>(MatchitVisualBackward)
nmap <silent> g% <Plug>(MatchitNormalBackward)
nnoremap gV `[v`]
xmap i,e <Plug>CamelCaseMotion_ie
xmap i,b <Plug>CamelCaseMotion_ib
xmap i,w <Plug>CamelCaseMotion_iw
omap i,e <Plug>CamelCaseMotion_ie
omap i,b <Plug>CamelCaseMotion_ib
omap i,w <Plug>CamelCaseMotion_iw
onoremap ts :execute "normal! G?^-- $\rkzz"
nnoremap <SNR>123_: :=v:count ? v:count : ''
vnoremap <silent> <Plug>NetrwBrowseXVis :call netrw#BrowseXVis()
nnoremap <silent> <Plug>NetrwBrowseX :call netrw#BrowseX(netrw#GX(),netrw#CheckIfRemote(netrw#GX()))
nnoremap <silent> <C-ScrollWheelDown> <C-ScrollWheelDown>:call color_coded#moved()
nnoremap <silent> <C-ScrollWheelUp> <C-ScrollWheelUp>:call color_coded#moved()
nnoremap <silent> <S-ScrollWheelDown> <S-ScrollWheelDown>:call color_coded#moved()
nnoremap <silent> <S-ScrollWheelUp> <S-ScrollWheelUp>:call color_coded#moved()
nnoremap <silent> <ScrollWheelDown> <ScrollWheelDown>:call color_coded#moved()
nnoremap <silent> <ScrollWheelUp> <ScrollWheelUp>:call color_coded#moved()
vmap <F4> <Plug>(MakeDigraph)
nmap <F4> <Plug>(MakeDigraph)
vnoremap <silent> <Plug>(calendar) :Calendar
nnoremap <silent> <Plug>(calendar) :Calendar
onoremap <Plug>(operator-clang-format) g@
vnoremap <C-R> "hy:%s/h//gc<Left><Left><Left>
snoremap <silent> <C-H> "_c
snoremap <silent> <Del> "_c
snoremap <silent> <BS> "_c
snoremap <silent> <C-Tab> :call UltiSnips#ListSnippets()
xnoremap <silent> <C-L> :call UltiSnips#SaveLastVisualSelection()gvs
snoremap <silent> <C-L> :call UltiSnips#ExpandSnippet()
nnoremap <silent> <Plug>(ctrlp) :CtrlP
vnoremap <silent> <Plug>CamelCaseMotion_ie :call camelcasemotion#InnerMotion('e',v:count1)
vnoremap <silent> <Plug>CamelCaseMotion_ib :call camelcasemotion#InnerMotion('b',v:count1)
vnoremap <silent> <Plug>CamelCaseMotion_iw :call camelcasemotion#InnerMotion('w',v:count1)
onoremap <silent> <Plug>CamelCaseMotion_ie :call camelcasemotion#InnerMotion('e',v:count1)
onoremap <silent> <Plug>CamelCaseMotion_ib :call camelcasemotion#InnerMotion('b',v:count1)
onoremap <silent> <Plug>CamelCaseMotion_iw :call camelcasemotion#InnerMotion('w',v:count1)
vnoremap <silent> <Plug>CamelCaseMotion_e :call camelcasemotion#Motion('e',v:count1,'v')
vnoremap <silent> <Plug>CamelCaseMotion_b :call camelcasemotion#Motion('b',v:count1,'v')
vnoremap <silent> <Plug>CamelCaseMotion_w :call camelcasemotion#Motion('w',v:count1,'v')
onoremap <silent> <Plug>CamelCaseMotion_e :call camelcasemotion#Motion('e',v:count1,'o')
onoremap <silent> <Plug>CamelCaseMotion_b :call camelcasemotion#Motion('b',v:count1,'o')
onoremap <silent> <Plug>CamelCaseMotion_w :call camelcasemotion#Motion('w',v:count1,'o')
nnoremap <silent> <Plug>CamelCaseMotion_e :call camelcasemotion#Motion('e',v:count1,'n')
nnoremap <silent> <Plug>CamelCaseMotion_b :call camelcasemotion#Motion('b',v:count1,'n')
nnoremap <silent> <Plug>CamelCaseMotion_w :call camelcasemotion#Motion('w',v:count1,'n')
nnoremap <silent> <Plug>unimpairedTPNext :exe "p".(v:count ? v:count : "")."tnext"
nnoremap <silent> <Plug>unimpairedTPPrevious :exe "p".(v:count ? v:count : "")."tprevious"
nnoremap <silent> <Plug>unimpairedTLast :exe "".(v:count ? v:count : "")."tlast"
nnoremap <silent> <Plug>unimpairedTFirst :exe "".(v:count ? v:count : "")."tfirst"
nnoremap <silent> <Plug>unimpairedTNext :exe "".(v:count ? v:count : "")."tnext"
nnoremap <silent> <Plug>unimpairedTPrevious :exe "".(v:count ? v:count : "")."tprevious"
nnoremap <silent> <Plug>unimpairedQNFile :exe "".(v:count ? v:count : "")."cnfile"zv
nnoremap <silent> <Plug>unimpairedQPFile :exe "".(v:count ? v:count : "")."cpfile"zv
nnoremap <silent> <Plug>unimpairedQLast :exe "".(v:count ? v:count : "")."clast"zv
nnoremap <silent> <Plug>unimpairedQFirst :exe "".(v:count ? v:count : "")."cfirst"zv
nnoremap <silent> <Plug>unimpairedQNext :exe "".(v:count ? v:count : "")."cnext"zv
nnoremap <silent> <Plug>unimpairedQPrevious :exe "".(v:count ? v:count : "")."cprevious"zv
nnoremap <silent> <Plug>unimpairedLNFile :exe "".(v:count ? v:count : "")."lnfile"zv
nnoremap <silent> <Plug>unimpairedLPFile :exe "".(v:count ? v:count : "")."lpfile"zv
nnoremap <silent> <Plug>unimpairedLLast :exe "".(v:count ? v:count : "")."llast"zv
nnoremap <silent> <Plug>unimpairedLFirst :exe "".(v:count ? v:count : "")."lfirst"zv
nnoremap <silent> <Plug>unimpairedLNext :exe "".(v:count ? v:count : "")."lnext"zv
nnoremap <silent> <Plug>unimpairedLPrevious :exe "".(v:count ? v:count : "")."lprevious"zv
nnoremap <silent> <Plug>unimpairedBLast :exe "".(v:count ? v:count : "")."blast"
nnoremap <silent> <Plug>unimpairedBFirst :exe "".(v:count ? v:count : "")."bfirst"
nnoremap <silent> <Plug>unimpairedBNext :exe "".(v:count ? v:count : "")."bnext"
nnoremap <silent> <Plug>unimpairedBPrevious :exe "".(v:count ? v:count : "")."bprevious"
nnoremap <silent> <Plug>unimpairedALast :exe "".(v:count ? v:count : "")."last"
nnoremap <silent> <Plug>unimpairedAFirst :exe "".(v:count ? v:count : "")."first"
nnoremap <silent> <Plug>unimpairedANext :exe "".(v:count ? v:count : "")."next"
nnoremap <silent> <Plug>unimpairedAPrevious :exe "".(v:count ? v:count : "")."previous"
xmap <M-P> <Plug>yankstack_substitute_newer_paste
nmap <M-P> <Plug>yankstack_substitute_newer_paste
xmap <M-p> <Plug>yankstack_substitute_older_paste
nmap <M-p> <Plug>yankstack_substitute_older_paste
nnoremap <silent> <C-N> :YRReplace '1', p
map <C-P> <Plug>(ctrlp)
nnoremap <silent> <SNR>67_yrrecord :call YRRecord3()
xnoremap <silent> <Plug>NERDCommenterUncomment :call NERDComment("x", "Uncomment")
nnoremap <silent> <Plug>NERDCommenterUncomment :call NERDComment("n", "Uncomment")
xnoremap <silent> <Plug>NERDCommenterAlignBoth :call NERDComment("x", "AlignBoth")
nnoremap <silent> <Plug>NERDCommenterAlignBoth :call NERDComment("n", "AlignBoth")
xnoremap <silent> <Plug>NERDCommenterAlignLeft :call NERDComment("x", "AlignLeft")
nnoremap <silent> <Plug>NERDCommenterAlignLeft :call NERDComment("n", "AlignLeft")
nnoremap <silent> <Plug>NERDCommenterAppend :call NERDComment("n", "Append")
xnoremap <silent> <Plug>NERDCommenterYank :call NERDComment("x", "Yank")
nnoremap <silent> <Plug>NERDCommenterYank :call NERDComment("n", "Yank")
xnoremap <silent> <Plug>NERDCommenterSexy :call NERDComment("x", "Sexy")
nnoremap <silent> <Plug>NERDCommenterSexy :call NERDComment("n", "Sexy")
xnoremap <silent> <Plug>NERDCommenterInvert :call NERDComment("x", "Invert")
nnoremap <silent> <Plug>NERDCommenterInvert :call NERDComment("n", "Invert")
nnoremap <silent> <Plug>NERDCommenterToEOL :call NERDComment("n", "ToEOL")
xnoremap <silent> <Plug>NERDCommenterNested :call NERDComment("x", "Nested")
nnoremap <silent> <Plug>NERDCommenterNested :call NERDComment("n", "Nested")
xnoremap <silent> <Plug>NERDCommenterMinimal :call NERDComment("x", "Minimal")
nnoremap <silent> <Plug>NERDCommenterMinimal :call NERDComment("n", "Minimal")
xnoremap <silent> <Plug>NERDCommenterToggle :call NERDComment("x", "Toggle")
nnoremap <silent> <Plug>NERDCommenterToggle :call NERDComment("n", "Toggle")
xnoremap <silent> <Plug>NERDCommenterComment :call NERDComment("x", "Comment")
nnoremap <silent> <Plug>NERDCommenterComment :call NERDComment("n", "Comment")
xmap <silent> <Plug>(MatchitVisualTextObject) <Plug>(MatchitVisualMultiBackward)o<Plug>(MatchitVisualMultiForward)
onoremap <silent> <Plug>(MatchitOperationMultiForward) :call matchit#MultiMatch("W",  "o")
onoremap <silent> <Plug>(MatchitOperationMultiBackward) :call matchit#MultiMatch("bW", "o")
xnoremap <silent> <Plug>(MatchitVisualMultiForward) :call matchit#MultiMatch("W",  "n")m'gv``
xnoremap <silent> <Plug>(MatchitVisualMultiBackward) :call matchit#MultiMatch("bW", "n")m'gv``
nnoremap <silent> <Plug>(MatchitNormalMultiForward) :call matchit#MultiMatch("W",  "n")
nnoremap <silent> <Plug>(MatchitNormalMultiBackward) :call matchit#MultiMatch("bW", "n")
onoremap <silent> <Plug>(MatchitOperationBackward) :call matchit#Match_wrapper('',0,'o')
onoremap <silent> <Plug>(MatchitOperationForward) :call matchit#Match_wrapper('',1,'o')
xnoremap <silent> <Plug>(MatchitVisualBackward) :call matchit#Match_wrapper('',0,'v')m'gv``
xnoremap <silent> <Plug>(MatchitVisualForward) :call matchit#Match_wrapper('',1,'v')m'gv``
nnoremap <silent> <Plug>(MatchitNormalBackward) :call matchit#Match_wrapper('',0,'n')
nnoremap <silent> <Plug>(MatchitNormalForward) :call matchit#Match_wrapper('',1,'n')
vmap <C-J> <Plug>IMAP_JumpForward
nmap <C-J> <Plug>IMAP_JumpForward
vnoremap <silent> <Plug>IMAP_JumpBack `<i=IMAP_Jumpfunc('b', 0)
vnoremap <silent> <Plug>IMAP_JumpForward i=IMAP_Jumpfunc('', 0)
vnoremap <silent> <Plug>IMAP_DeleteAndJumpBack "_<Del>i=IMAP_Jumpfunc('b', 0)
vnoremap <silent> <Plug>IMAP_DeleteAndJumpForward "_<Del>i=IMAP_Jumpfunc('', 0)
nnoremap <silent> <Plug>IMAP_JumpBack i=IMAP_Jumpfunc('b', 0)
nnoremap <silent> <Plug>IMAP_JumpForward i=IMAP_Jumpfunc('', 0)
nnoremap <F5> :Make
nmap <C-@><C-@>d :vert scs find d =expand("<cword>")
nmap <Nul><Nul>d :vert scs find d =expand("<cword>")
nmap <C-@><C-@>i :vert scs find i ^=expand("<cfile>")$	
nmap <Nul><Nul>i :vert scs find i ^=expand("<cfile>")$	
nmap <C-@><C-@>f :vert scs find f =expand("<cfile>")	
nmap <Nul><Nul>f :vert scs find f =expand("<cfile>")	
nmap <C-@><C-@>e :vert scs find e =expand("<cword>")
nmap <Nul><Nul>e :vert scs find e =expand("<cword>")
nmap <C-@><C-@>t :vert scs find t =expand("<cword>")
nmap <Nul><Nul>t :vert scs find t =expand("<cword>")
nmap <C-@><C-@>c :vert scs find c =expand("<cword>")
nmap <Nul><Nul>c :vert scs find c =expand("<cword>")
nmap <C-@><C-@>g :vert scs find g =expand("<cword>")
nmap <Nul><Nul>g :vert scs find g =expand("<cword>")
nmap <C-@><C-@>s :vert scs find s =expand("<cword>")
nmap <Nul><Nul>s :vert scs find s =expand("<cword>")
nmap <C-@>d :scs find d =expand("<cword>")	
nmap <Nul>d :scs find d =expand("<cword>")	
nmap <C-@>i :scs find i ^=expand("<cfile>")$	
nmap <Nul>i :scs find i ^=expand("<cfile>")$	
nmap <C-@>f :scs find f =expand("<cfile>")	
nmap <Nul>f :scs find f =expand("<cfile>")	
nmap <C-@>e :scs find e =expand("<cword>")	
nmap <Nul>e :scs find e =expand("<cword>")	
nmap <C-@>t :scs find t =expand("<cword>")	
nmap <Nul>t :scs find t =expand("<cword>")	
nmap <C-@>c :scs find c =expand("<cword>")	
nmap <Nul>c :scs find c =expand("<cword>")	
nmap <C-@>g :scs find g =expand("<cword>")	
nmap <Nul>g :scs find g =expand("<cword>")	
nmap <C-@>s :scs find s =expand("<cword>")	
nmap <Nul>s :scs find s =expand("<cword>")	
nmap <C-Bslash>d :cs find d =expand("<cword>")	
nmap <C-Bslash>i :cs find i ^=expand("<cfile>")$
nmap <C-Bslash>f :cs find f =expand("<cfile>")	
nmap <C-Bslash>e :cs find e =expand("<cword>")	
nmap <C-Bslash>t :cs find t =expand("<cword>")	
nmap <C-Bslash>c :cs find c =expand("<cword>")	
nmap <C-Bslash>g :cs find g =expand("<cword>")	
nmap <C-Bslash>s :cs find s =expand("<cword>")	
inoremap <expr> 	 pumvisible() ? "\" : "\	"
imap <NL> <Plug>IMAP_JumpForward
inoremap <silent>  =UltiSnips#ExpandSnippet()
imap  <Plug>(HTMLEntityComplete)
imap  <Plug>(UnicodeComplete)
imap  <Plug>(DigraphComplete)
inoremap  <Nop>
inoremap <expr> [200~ XTermPasteBegin()
xmap Ð <Plug>yankstack_substitute_newer_paste
nmap Ð <Plug>yankstack_substitute_newer_paste
xmap ð <Plug>yankstack_substitute_older_paste
nmap ð <Plug>yankstack_substitute_older_paste
nnoremap Â gwip
nmap œun <Plug>(UnicodeSwapCompleteName)
nmap œca <Plug>NERDCommenterAltDelims
xmap œcu <Plug>NERDCommenterUncomment
nmap œcu <Plug>NERDCommenterUncomment
xmap œcb <Plug>NERDCommenterAlignBoth
nmap œcb <Plug>NERDCommenterAlignBoth
xmap œcl <Plug>NERDCommenterAlignLeft
nmap œcl <Plug>NERDCommenterAlignLeft
nmap œcA <Plug>NERDCommenterAppend
xmap œcy <Plug>NERDCommenterYank
nmap œcy <Plug>NERDCommenterYank
xmap œcs <Plug>NERDCommenterSexy
xmap œci <Plug>NERDCommenterInvert
nmap œci <Plug>NERDCommenterInvert
nmap œc$ <Plug>NERDCommenterToEOL
xmap œcn <Plug>NERDCommenterNested
nmap œcn <Plug>NERDCommenterNested
xmap œcm <Plug>NERDCommenterMinimal
nmap œcm <Plug>NERDCommenterMinimal
xmap œc  <Plug>NERDCommenterToggle
nmap œc  <Plug>NERDCommenterToggle
xmap œcc <Plug>NERDCommenterComment
nmap œcc <Plug>NERDCommenterComment
nmap œihn :IHN
nmap œih :IHS
nnoremap œmtn :call MoveToNextTab()
nnoremap œmtp :call MoveToPrevTab()
nnoremap œm :wa|:Start! make all
nnoremap œT :TlistToggle
nnoremap œlt :exe "tabn ".g:lasttab
nnoremap œtp :tabprev
nnoremap œtn :tabnext
nnoremap œbp :bp
nnoremap œbn :bn
nnoremap œp 
nnoremap œr 
nnoremap œl 
nnoremap œu 
nnoremap œd <NL>
nnoremap œcs ^i*  
nnoremap œyycm :let b:ycm_largefile=0
nnoremap œnycm :let b:ycm_largefile=1
nnoremap œypar ::YcmCompleter GetParent
nnoremap œydec ::YcmCompleter GoToDeclaration
nnoremap œydef ::YcmCompleter GoToDefinition
nnoremap œyinc ::YcmCompleter GoToInclude
nnoremap œrcs :source $MYVIMRC
nnoremap œrce :split $MYVIMRC
nnoremap œyrt :YRShow
nmap œ<Down> <Plug>yankstack_substitute_newer_paste
nmap œ<Up> <Plug>yankstack_substitute_older_paste
nnoremap œntt :NERDTreeToggle
nnoremap œmut :MundoToggle
nnoremap œrms magg:.,$s/\s\{2,20\}/ /'a
nnoremap œrws magg:%s/[[:space:]]\{1,4\}$//'a
nnoremap œ  :nohlsearch
nnoremap œ<C-U> gUiw
nnoremap œ gUiw
nnoremap œtt :read !today-time
nnoremap œt :read !today
nnoremap œfc :foldclose
nnoremap œfo :foldopen
nnoremap œis mkH=L'k
nnoremap œif mkHmlgg=G'lzt'k
nnoremap œbx :BufExplorer
nmap œcft :ClangFormatAutoToggle
nnoremap œfrups ifilippo.rusconi@universite-paris-saclay.fr
nnoremap œups iuniversite-paris-saclay.fr
nnoremap œers iSincerely,Filippo Rusconi
nnoremap œrs iSincèrement,Filippo Rusconi
nnoremap œfs iSincèrement,Filippo
nnoremap œrab iÀ bientôt,Filippo Rusconi
nnoremap œfab iÀ bientôt,Filippo
inoremap kj 
iabbr csg Abide by the coding style guide
iabbr ppd pappso::pappso_double
iabbr pd pappso_double
iabbr jsdoc /*/js/*/
iabbr ldo@@ lopippo@debian.org
iabbr lrlpn@@ listes.rusconi@laposte.net
iabbr frups@@ filippo.rusconi@u-psud.fr
iabbr frlpn@@ filippo.rusconi@laposte.net
let &cpo=s:cpo_save
unlet s:cpo_save
set autowrite
set background=dark
set backspace=indent,eol,start
set completefunc=youcompleteme#CompleteFunc
set completeopt=preview,menuone
set cpoptions=aAceFsB
set cscopetag
set expandtab
set fileencodings=ucs-bom,utf-8,default,latin1
set foldlevelstart=3
set grepprg=grep\ -nH\ $*
set helplang=en
set history=2000
set ignorecase
set incsearch
set iskeyword=@,48-57,_,192-255,:
set laststatus=2
set pastetoggle=<Esc>[201~
set printoptions=paper:letter
set ruler
set runtimepath=~/.vim,~/.vim/bundle/Vundle.vim,~/.vim/bundle/a.vim,~/.vim/bundle/nerdtree,~/.vim/bundle/nerdcommenter,~/.vim/bundle/vim-easytags,~/.vim/bundle/vim-misc,~/.vim/bundle/vim-mundo,~/.vim/bundle/YankRing.vim,~/.vim/bundle/vim-yankstack,~/.vim/bundle/vim-dispatch,~/.vim/bundle/vim-unimpaired,~/.vim/bundle/hiPairs,~/.vim/bundle/CamelCaseMotion,~/.vim/bundle/SmartCase,~/.vim/bundle/bbye,~/.vim/bundle/tabular,~/.vim/bundle/ack.vim,~/.vim/bundle/YouCompleteMe,~/.vim/bundle/vim-abolish,~/.vim/bundle/vim-fugitive,~/.vim/bundle/ctrlp.vim,~/.vim/bundle/vim-ctrlp-switcher,~/.vim/bundle/YCM-Generator,~/.vim/bundle/ultisnips,~/.vim/bundle/vim-autoformat,~/.vim/bundle/vim-operator-user,~/.vim/bundle/vim-clang-format,~/.vim/bundle/calendar.vim,~/.vim/bundle/vim-docbk,~/.vim/bundle/vim-snippets,~/.vim/bundle/xmledit,~/.vim/bundle/xterm-color-table.vim,~/.vim/bundle/vim-toml,~/.vim/bundle/editorconfig-vim,~/.vim/bundle/unicode.vim,~/.vim/bundle/vim-meson,~/.vim/bundle/color_coded,~/.vim/bundle/vim-markdown,~/.vim/bundle/vim-xml-runtime,~/.vim/bundle/papercolor-theme,~/.vim/pack/themes/start/dracula,/var/lib/vim/addons,/etc/vim,/usr/share/vim/vimfiles,/usr/share/vim/vim82,/usr/share/vim/vim82/pack/dist/opt/matchit,~/.vim/pack/themes/start/dracula/after,/usr/share/vim/vimfiles/after,/etc/vim/after,/var/lib/vim/addons/after,~/.vim/after,~/.vim/bundle/Vundle.vim,~/.vim/bundle/Vundle.vim/after,~/.vim/bundle/a.vim/after,~/.vim/bundle/nerdtree/after,~/.vim/bundle/nerdcommenter/after,~/.vim/bundle/vim-easytags/after,~/.vim/bundle/vim-misc/after,~/.vim/bundle/vim-mundo/after,~/.vim/bundle/YankRing.vim/after,~/.vim/bundle/vim-yankstack/after,~/.vim/bundle/vim-dispatch/after,~/.vim/bundle/vim-unimpaired/after,~/.vim/bundle/hiPairs/after,~/.vim/bundle/CamelCaseMotion/after,~/.vim/bundle/SmartCase/after,~/.vim/bundle/bbye/after,~/.vim/bundle/tabular/after,~/.vim/bundle/ack.vim/after,~/.vim/bundle/YouCompleteMe/after,~/.vim/bundle/vim-abolish/after,~/.vim/bundle/vim-fugitive/after,~/.vim/bundle/ctrlp.vim/after,~/.vim/bundle/vim-ctrlp-switcher/after,~/.vim/bundle/YCM-Generator/after,~/.vim/bundle/ultisnips/after,~/.vim/bundle/vim-autoformat/after,~/.vim/bundle/vim-operator-user/after,~/.vim/bundle/vim-clang-format/after,~/.vim/bundle/calendar.vim/after,~/.vim/bundle/vim-docbk/after,~/.vim/bundle/vim-snippets/after,~/.vim/bundle/xmledit/after,~/.vim/bundle/xterm-color-table.vim/after,~/.vim/bundle/vim-toml/after,~/.vim/bundle/editorconfig-vim/after,~/.vim/bundle/unicode.vim/after,~/.vim/bundle/vim-meson/after,~/.vim/bundle/color_coded/after,~/.vim/bundle/vim-markdown/after,~/.vim/bundle/vim-xml-runtime/after,~/.vim/bundle/papercolor-theme/after
set shiftwidth=2
set shortmess=filnxtToOSc
set showcmd
set showmatch
set smartcase
set softtabstop=2
set statusline=%#identifier#[%f]%*%#warningmsg#%{&ff!='unix'?'['.&ff.']':''}%*%#warningmsg#%{(&fenc!='utf-8'&&&fenc!='')?'['.&fenc.']':''}%*%h%y%#identifier#%r%*%#warningmsg#%m%*%{fugitive#statusline()}%#error#%{&paste?'[paste]':''}%*--buf:%n--tab:%{tabpagenr()}%#TabNum#/%{tabpagenr('$')}--[%4l@%2c]/%4L
set suffixes=.bak,~,.swp,.o,.info,.aux,.log,.dvi,.bbl,.blg,.brf,.cb,.ind,.idx,.ilg,.inx,.out,.toc
set switchbuf=usetab,split
set tabline=%!MyTabLine()
set tabstop=2
set tags=./tags,./TAGS,tags,TAGS,~/.vimtags
set textwidth=80
set wildignore=*.o,*.obj,*~
set wildmenu
set wildmode=list:longest
let s:so_save = &so | let s:siso_save = &siso | set so=0 siso=0
let v:this_session=expand("<sfile>:p")
silent only
silent tabonly
cd ~/devel/minexpert2/development/src/tests
if expand('%') == '' && !&modified && line('$') <= 1 && getline(1) == ''
  let s:wipebuf = bufnr('%')
endif
set shortmess=aoO
argglobal
%argdel
$argadd CMakeLists.txt
edit ../CMakeLists.txt
set splitbelow splitright
wincmd _ | wincmd |
vsplit
1wincmd h
wincmd w
set nosplitbelow
set nosplitright
wincmd t
set winminheight=0
set winheight=1
set winminwidth=0
set winwidth=1
exe 'vert 1resize ' . ((&columns * 117 + 117) / 234)
exe 'vert 2resize ' . ((&columns * 116 + 117) / 234)
argglobal
setlocal keymap=
setlocal noarabic
setlocal noautoindent
setlocal backupcopy=
setlocal balloonexpr=
setlocal nobinary
setlocal nobreakindent
setlocal breakindentopt=
setlocal bufhidden=
setlocal buflisted
setlocal buftype=
setlocal nocindent
setlocal cinkeys=0{,0},0),0],:,0#,!^F,o,O,e
setlocal cinoptions=
setlocal cinwords=if,else,while,do,for,switch
setlocal colorcolumn=
setlocal comments=s1:/*,mb:*,ex:*/,://,b:#,:%,:XCOMM,n:>,fb:-
setlocal commentstring=#\ %s
setlocal complete=.,w,b,u,t,i
setlocal concealcursor=
setlocal conceallevel=0
setlocal completefunc=youcompleteme#CompleteFunc
setlocal nocopyindent
setlocal cryptmethod=
setlocal nocursorbind
setlocal nocursorcolumn
setlocal nocursorline
setlocal cursorlineopt=both
setlocal define=
setlocal dictionary=
setlocal nodiff
setlocal equalprg=
setlocal errorformat=
setlocal expandtab
if &filetype != 'cmake'
setlocal filetype=cmake
endif
setlocal fixendofline
setlocal foldcolumn=0
setlocal foldenable
setlocal foldexpr=0
setlocal foldignore=#
setlocal foldlevel=3
setlocal foldmarker={{{,}}}
set foldmethod=indent
setlocal foldmethod=indent
setlocal foldminlines=1
set foldnestmax=3
setlocal foldnestmax=3
setlocal foldtext=foldtext()
setlocal formatexpr=
setlocal formatoptions=tcq
setlocal formatlistpat=^\\s*\\d\\+[\\]:.)}\\t\ ]\\s*
setlocal formatprg=
setlocal grepprg=
setlocal iminsert=0
setlocal imsearch=-1
setlocal include=
setlocal includeexpr=
setlocal indentexpr=CMakeGetIndent(v:lnum)
setlocal indentkeys=0{,0},0),0],:,0#,!^F,o,O,e,=ENDIF(,ENDFOREACH(,ENDMACRO(,ELSE(,ELSEIF(,ENDWHILE(
setlocal noinfercase
setlocal iskeyword=@,48-57,_,192-255,:
setlocal keywordprg=
setlocal nolinebreak
setlocal nolisp
setlocal lispwords=
setlocal nolist
setlocal makeencoding=
setlocal makeprg=
setlocal matchpairs=(:),{:},[:]
setlocal modeline
setlocal modifiable
setlocal nrformats=bin,octal,hex
set number
setlocal number
set numberwidth=3
setlocal numberwidth=3
setlocal omnifunc=
setlocal path=
setlocal nopreserveindent
setlocal nopreviewwindow
setlocal quoteescape=\\
setlocal noreadonly
setlocal norelativenumber
setlocal norightleft
setlocal rightleftcmd=search
setlocal noscrollbind
setlocal scrolloff=-1
setlocal shiftwidth=2
setlocal noshortname
setlocal showbreak=
setlocal sidescrolloff=-1
setlocal signcolumn=auto
setlocal nosmartindent
setlocal softtabstop=2
setlocal nospell
setlocal spellcapcheck=[.?!]\\_[\\])'\"\	\ ]\\+
setlocal spellfile=
setlocal spelllang=en
setlocal statusline=
setlocal suffixesadd=
setlocal swapfile
setlocal synmaxcol=3000
if &syntax != 'cmake'
setlocal syntax=cmake
endif
setlocal tabstop=2
setlocal tagcase=
setlocal tagfunc=
setlocal tags=
setlocal termwinkey=
setlocal termwinscroll=10000
setlocal termwinsize=
setlocal textwidth=80
setlocal thesaurus=
setlocal noundofile
setlocal undolevels=-123456
setlocal varsofttabstop=
setlocal vartabstop=
setlocal wincolor=
setlocal nowinfixheight
setlocal nowinfixwidth
setlocal wrap
setlocal wrapmargin=0
let s:l = 282 - ((54 * winheight(0) + 31) / 62)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
282
normal! 0
wincmd w
argglobal
if bufexists("CMakeLists.txt") | buffer CMakeLists.txt | else | edit CMakeLists.txt | endif
setlocal keymap=
setlocal noarabic
setlocal noautoindent
setlocal backupcopy=
setlocal balloonexpr=
setlocal nobinary
setlocal nobreakindent
setlocal breakindentopt=
setlocal bufhidden=
setlocal buflisted
setlocal buftype=
setlocal nocindent
setlocal cinkeys=0{,0},0),0],:,0#,!^F,o,O,e
setlocal cinoptions=
setlocal cinwords=if,else,while,do,for,switch
setlocal colorcolumn=
setlocal comments=s1:/*,mb:*,ex:*/,://,b:#,:%,:XCOMM,n:>,fb:-
setlocal commentstring=#\ %s
setlocal complete=.,w,b,u,t,i
setlocal concealcursor=
setlocal conceallevel=0
setlocal completefunc=youcompleteme#CompleteFunc
setlocal nocopyindent
setlocal cryptmethod=
setlocal nocursorbind
setlocal nocursorcolumn
setlocal nocursorline
setlocal cursorlineopt=both
setlocal define=
setlocal dictionary=
setlocal nodiff
setlocal equalprg=
setlocal errorformat=
setlocal expandtab
if &filetype != 'cmake'
setlocal filetype=cmake
endif
setlocal fixendofline
setlocal foldcolumn=0
setlocal foldenable
setlocal foldexpr=0
setlocal foldignore=#
setlocal foldlevel=3
setlocal foldmarker={{{,}}}
set foldmethod=indent
setlocal foldmethod=indent
setlocal foldminlines=1
set foldnestmax=3
setlocal foldnestmax=3
setlocal foldtext=foldtext()
setlocal formatexpr=
setlocal formatoptions=tcq
setlocal formatlistpat=^\\s*\\d\\+[\\]:.)}\\t\ ]\\s*
setlocal formatprg=
setlocal grepprg=
setlocal iminsert=0
setlocal imsearch=-1
setlocal include=
setlocal includeexpr=
setlocal indentexpr=CMakeGetIndent(v:lnum)
setlocal indentkeys=0{,0},0),0],:,0#,!^F,o,O,e,=ENDIF(,ENDFOREACH(,ENDMACRO(,ELSE(,ELSEIF(,ENDWHILE(
setlocal noinfercase
setlocal iskeyword=@,48-57,_,192-255,:
setlocal keywordprg=
setlocal nolinebreak
setlocal nolisp
setlocal lispwords=
setlocal nolist
setlocal makeencoding=
setlocal makeprg=
setlocal matchpairs=(:),{:},[:]
setlocal modeline
setlocal modifiable
setlocal nrformats=bin,octal,hex
set number
setlocal number
set numberwidth=3
setlocal numberwidth=3
setlocal omnifunc=DebCompleteBugs
setlocal path=
setlocal nopreserveindent
setlocal nopreviewwindow
setlocal quoteescape=\\
setlocal noreadonly
setlocal norelativenumber
setlocal norightleft
setlocal rightleftcmd=search
setlocal noscrollbind
setlocal scrolloff=-1
setlocal shiftwidth=2
setlocal noshortname
setlocal showbreak=
setlocal sidescrolloff=-1
setlocal signcolumn=auto
setlocal nosmartindent
setlocal softtabstop=2
setlocal nospell
setlocal spellcapcheck=[.?!]\\_[\\])'\"\	\ ]\\+
setlocal spellfile=
setlocal spelllang=en
setlocal statusline=
setlocal suffixesadd=
setlocal swapfile
setlocal synmaxcol=3000
if &syntax != 'cmake'
setlocal syntax=cmake
endif
setlocal tabstop=2
setlocal tagcase=
setlocal tagfunc=
setlocal tags=
setlocal termwinkey=
setlocal termwinscroll=10000
setlocal termwinsize=
setlocal textwidth=80
setlocal thesaurus=
setlocal noundofile
setlocal undolevels=-123456
setlocal varsofttabstop=
setlocal vartabstop=
setlocal wincolor=
setlocal nowinfixheight
setlocal nowinfixwidth
setlocal wrap
setlocal wrapmargin=0
3
normal! zo
let s:l = 36 - ((35 * winheight(0) + 31) / 62)
if s:l < 1 | let s:l = 1 | endif
exe s:l
normal! zt
36
normal! 03|
wincmd w
2wincmd w
exe 'vert 1resize ' . ((&columns * 117 + 117) / 234)
exe 'vert 2resize ' . ((&columns * 116 + 117) / 234)
tabnext 1
badd +66 CMakeLists.txt
badd +0 ../CMakeLists.txt
if exists('s:wipebuf') && len(win_findbuf(s:wipebuf)) == 0
  silent exe 'bwipe ' . s:wipebuf
endif
unlet! s:wipebuf
set winheight=1 winwidth=20 shortmess=filnxtToOSc
set winminheight=1 winminwidth=1
let s:sx = expand("<sfile>:p:r")."x.vim"
if filereadable(s:sx)
  exe "source " . fnameescape(s:sx)
endif
let &so = s:so_save | let &siso = s:siso_save
doautoall SessionLoadPost
unlet SessionLoad
" vim: set ft=vim :
