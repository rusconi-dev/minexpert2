#include <catch2/catch.hpp>

#include <QDebug>

#include <pappsomspp/processing/combiners/mzintegrationparams.h>
#include <pappsomspp/utils.h>


using namespace pappso;
using namespace msxps::minexpert;


TEST_CASE("Constructing MzIntegrationParams objects.",
          "[MzIntegrationParams][constructors]")
{

  SECTION("Construct a MzIntegrationParams", "[MzIntegrationParams]")
  {
    MzIntegrationParams mz_integration_params1;
    PrecisionPtr precision_p = pappso::PrecisionFactory::getPpmInstance(20);

    REQUIRE(mz_integration_params1.getBinningType() == BinningType::NONE);

    REQUIRE(precision_p == mz_integration_params1.getPrecision());

    double smallestMz            = 1800.373856;
    double greatestMz            = 2600.956480;
    BinningType binningType      = BinningType::ARBITRARY;
    int decimalPlaces            = 3;
    bool applyMzShift            = false;
    double mzShift               = 0;
    bool removeZeroValDataPoints = true;


    MzIntegrationParams mz_integration_params2(smallestMz,
                                               greatestMz,
                                               binningType,
                                               decimalPlaces,
                                               precision_p,
                                               applyMzShift,
                                               mzShift,
                                               removeZeroValDataPoints);

    REQUIRE(mz_integration_params2.getSmallestMz() == smallestMz);
    REQUIRE(mz_integration_params2.getGreatestMz() == greatestMz);
    REQUIRE(mz_integration_params2.getBinningType() == binningType);
    REQUIRE(mz_integration_params2.getDecimalPlaces() == decimalPlaces);
    REQUIRE(mz_integration_params2.getPrecision() == precision_p);
    REQUIRE(mz_integration_params2.isApplyMzShift() == applyMzShift);
    REQUIRE(mz_integration_params2.getMzShift() == mzShift);
    REQUIRE(mz_integration_params2.isRemoveZeroValDataPoints() ==
            removeZeroValDataPoints);
  }


  SECTION(
    "Construct a MzIntegrationParams from another "
    "MzIntegrationParams",
    "[MzIntegrationParams]")
  {

    double smallestMz            = 1800.373856;
    double greatestMz            = 2600.956480;
    BinningType binningType      = BinningType::ARBITRARY;
    int decimalPlaces            = 3;
    PrecisionPtr precision_p     = pappso::PrecisionFactory::getPpmInstance(20);
    bool applyMzShift            = false;
    double mzShift               = 0;
    bool removeZeroValDataPoints = true;

    MzIntegrationParams mz_integration_params1(smallestMz,
                                               greatestMz,
                                               binningType,
                                               decimalPlaces,
                                               precision_p,
                                               applyMzShift,
                                               mzShift,
                                               removeZeroValDataPoints);

    MzIntegrationParams mz_integration_params2(mz_integration_params1);

    REQUIRE(mz_integration_params2.getSmallestMz() == smallestMz);
    REQUIRE(mz_integration_params2.getGreatestMz() == greatestMz);
    REQUIRE(mz_integration_params2.getBinningType() == binningType);
    REQUIRE(mz_integration_params2.getDecimalPlaces() == decimalPlaces);
    REQUIRE(mz_integration_params2.getPrecision() == precision_p);
    REQUIRE(mz_integration_params2.isApplyMzShift() == applyMzShift);
    REQUIRE(mz_integration_params2.getMzShift() == mzShift);
    REQUIRE(mz_integration_params2.isRemoveZeroValDataPoints() ==
            removeZeroValDataPoints);
  }

  SECTION("Construct a MzIntegrationParams with the Savitzky-Golay params",
          "[MzIntegrationParams]")
  {
    SavGolParams savgol_params1;

    REQUIRE(savgol_params1.nL == 15);
    REQUIRE(savgol_params1.nR == 15);
    REQUIRE(savgol_params1.m == 4);
    REQUIRE(savgol_params1.lD == 0);
    REQUIRE(savgol_params1.convolveWithNr == false);

    MzIntegrationParams mz_integration_params(savgol_params1);

    REQUIRE(mz_integration_params.getBinningType() == BinningType::NONE);
    REQUIRE(mz_integration_params.getPrecision() ==
            PrecisionFactory::getPpmInstance(20));

    SavGolParams savgol_params2 = mz_integration_params.getSavGolParams();

    REQUIRE(savgol_params2.nL == 15);
    REQUIRE(savgol_params2.nR == 15);
    REQUIRE(savgol_params2.m == 4);
    REQUIRE(savgol_params2.lD == 0);
    REQUIRE(savgol_params2.convolveWithNr == false);
  }
}


TEST_CASE("Create bins with Da-based bin size", "[MzIntegrationParams]")
{
  double smallestMz        = 1800.373856;
  double greatestMz        = 1800.373856;
  BinningType binningType  = BinningType::ARBITRARY;
  int decimalPlaces        = 3;
  PrecisionPtr precision_p = pappso::PrecisionFactory::getDaltonInstance(0.05);
  bool applyMzShift        = false;
  double mzShift           = 0;
  bool removeZeroValDataPoints = true;

  MzIntegrationParams mz_integration_params(smallestMz,
                                            greatestMz,
                                            binningType,
                                            decimalPlaces,
                                            precision_p,
                                            applyMzShift,
                                            mzShift,
                                            removeZeroValDataPoints);

  SECTION("Create bins in ARBITRARY mode", "[MzIntegrationParams]")
  {

    std::vector<double> bins = mz_integration_params.createBins();

    // Do not forget that we round to 3 decimal places:
    REQUIRE(bins.front() == 1800.374);

    // We create two bins: the first mz value (1800.374) and a new one that is
    // that mz value incremented by the delta (that is the size of the bin that
    // is created using an increment of 0.05).

    REQUIRE(bins.back() == 1800.374 + precision_p->delta(1800.374));
  }
}


TEST_CASE("Create bins with ppm-based bin sizes", "[MzIntegrationParams]")
{
  double smallestMz            = 1800.373856;
  double greatestMz            = 1800.373856;
  BinningType binningType      = BinningType::ARBITRARY;
  int decimalPlaces            = 3;
  PrecisionPtr precision_p     = pappso::PrecisionFactory::getPpmInstance(20);
  bool applyMzShift            = false;
  double mzShift               = 0;
  bool removeZeroValDataPoints = true;

  MzIntegrationParams mz_integration_params(smallestMz,
                                            greatestMz,
                                            binningType,
                                            decimalPlaces,
                                            precision_p,
                                            applyMzShift,
                                            mzShift,
                                            removeZeroValDataPoints);

  SECTION("Create bins in ARBITRARY mode", "[MzIntegrationParams]")
  {

    std::vector<double> bins = mz_integration_params.createBins();

    // Do not forget that we round to 3 decimal places:
    REQUIRE(bins.front() == 1800.374);

    // Check that the precision delta is computed correctly:
    double precision_delta = 1800.374 * 20 / 1000000;
    REQUIRE(pappso::Utils::almostEqual(
      precision_p->delta(1800.374), precision_delta, /*decimal_places*/ 15));

    double rounded_precision_delta =
      ceil((precision_delta * pow(10, decimalPlaces)) - 0.49) /
      pow(10, decimalPlaces);

    // We create two bins: the first mz value (1800.374) and a new one that is
    // that mz value incremented by the delta (that is the size of the bin that
    // is created using relative proportion of 20 ppm).

    REQUIRE(bins.back() == 1800.374 + rounded_precision_delta);
  }
}


TEST_CASE("Create bins with res-based bin sizes", "[MzIntegrationParams]")
{
  double smallestMz            = 1800.373856;
  double greatestMz            = 1800.373856;
  BinningType binningType      = BinningType::ARBITRARY;
  int decimalPlaces            = 3;
  PrecisionPtr precision_p     = pappso::PrecisionFactory::getResInstance(20000);
  bool applyMzShift            = false;
  double mzShift               = 0;
  bool removeZeroValDataPoints = true;

  MzIntegrationParams mz_integration_params(smallestMz,
                                            greatestMz,
                                            binningType,
                                            decimalPlaces,
                                            precision_p,
                                            applyMzShift,
                                            mzShift,
                                            removeZeroValDataPoints);

  SECTION("Create bins in ARBITRARY mode", "[MzIntegrationParams]")
  {

    std::vector<double> bins = mz_integration_params.createBins();

    // Do not forget that we round to 3 decimal places:
    REQUIRE(bins.front() == 1800.374);

    // Check that the precision delta is computed correctly:
    double precision_delta = 1800.374 / 20000;
    REQUIRE(pappso::Utils::almostEqual(
      precision_p->delta(1800.374), precision_delta, /*decimal_places*/ 15));

    double rounded_precision_delta =
      ceil((precision_delta * pow(10, decimalPlaces)) - 0.49) /
      pow(10, decimalPlaces);

    // We create two bins: the first mz value (1800.374) and a new one that is
    // that mz value incremented by the delta (that is the size of the bin that
    // is created using relative proportion of 20 ppm).

    REQUIRE(bins.back() == 1800.374 + rounded_precision_delta);
  }
}


